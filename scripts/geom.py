import nibabel as nib
import numpy as np
from skimage import measure
from stl import mesh
import os

jp = os.path.join


def label2stl(input_label, output_path, masks, voxel_size=(0.5, 0.5, 0.5)):
    """This function creates STL files from masks using marching cubes.

    Parameters:
    input_path (str): path of the masks files
    output_path (str): path to save the STL files
    masks (dict): mask labels and filenames
    spacing (tuple): if provided must be equal to the voxel size of the
                       input labelmap

    Returns:
    ----------
    """

    if not isinstance(masks, dict):

        print('The 3rd input must be a dictionary\n')
        print('So that the algorithm can identify the labels\n')
        return

    labels = list(masks.keys())

    for l in range(len(labels)):

        m = nib.load(input_label)

        m_d = m.get_data()
        m_d = np.where(m_d == labels[l], m_d, 0)

        vert, faces, normals, val = \
            measure.marching_cubes_lewiner(m_d, level=None, spacing=voxel_size)

        # swap faces ids to be in the proper orientation

        faces[:, [0, 2]] = faces[:, [2, 0]]

        surface = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))

        for i, f in enumerate(faces):
            for j in range(3):
                surface.vectors[i][j] = vert[f[j], :]

        if not os.path.exists(output_path):
            os.makedirs(output_path)

        surface.save(jp(output_path, masks[labels[l]][:-7] + '.stl'))

# NOT USED FUNCTION


def masks2stl(input_path, output_path, masks, voxel_size=(0.5, 0.5, 0.5)):
    """This function creates STL files from masks using marching cubes.

    Parameters:
    input_path (str): path of the masks files
    output_path (str): path to save the STL files
    masks (list-str): list of mask filenames. If this parameter is a dictionary
                     it is automatically converted to list.
    spacing (tuple): if provided must be equal to the voxel size of the
                       input labelmap.

    Returns:
    ----------
    """

    if masks is dict:

        temp = []

        for k, v in masks.keys():
            temp.append(masks[k])
        masks = temp

    for i in range(len(masks)):

        m = nib.load(jp(input_path, masks[i]))

        m_d = m.get_data()

        vert, faces, normals, val = \
            measure.marching_cubes_lewiner(m_d, level=None,
                                           spacing=voxel_size)

        # swap faces ids to be in the proper orientation

        faces[:, [0, 2]] = faces[:, [2, 0]]

        surface = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))

        for i, f in enumerate(faces):
            for j in range(3):
                surface.vectors[i][j] = vert[f[j], :]

        if not os.path.exists(output_path):
            os.makedirs(output_path)

        surface.save(jp(output_path, masks[i][:-7] + '.stl'))
