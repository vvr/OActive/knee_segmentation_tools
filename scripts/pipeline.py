"""
Description: Registration, segmentation and extraction of .stl from the 
             MRI target sequence.

Requirements:
    Python 3.x
    Libraries: nilearn, numpy, nibabel, scikit-image, numpy-stl, scipy
    Programs: image registration (DRAMMS, flirt), segmentation (PICSL)
    Geometry processing: meshlab

Author: Filip Nikolopoulos
Contributors: Dimitar Stanev, Eva Zacharaki
"""
##
# libraries
import os
from os.path import join as jp
from os.path import dirname as dn
import time
import numpy as np
from resample import resample2affine as resample
from labelmaps import mask2labelmap as lab
from dramms import affine, deform, combine, inv_field, transf_label
from dramms import resample as res_dramms
from geom import label2stl as extract_stl
from morph_oper import morph
from picsl import segmentation
from mesh_filt import mesh_filt

##
# target MRI

# specify new MRI
target_mri = '04068603198R'

##
# pre-define path variables

data = os.path.abspath('../data/')

# temp_vol: the MRI used to define the atlas space for the registration
temp_vol = jp(data, 'oks001', 'registration', 'resampled.nii.gz')

# original_mri: the target MRI we want to segment before we apply any procedure
original_mri = jp(data, target_mri, target_mri + '.nii.gz')

# resampled_mri: the target MRI with new voxel size
resampled_mri = jp(data, target_mri, 'registration', 'resampled.nii.gz')

# affine_mri: the target MRI with affine registration to the atlas space
affine_mri = jp(data, target_mri, 'registration', 'affine.nii.gz')

# deform_mri: the target MRI with deformable registration to the atlas space
deform_mri = jp(data, target_mri, 'registration', 'deform_mri.nii.gz')

# out_seg_picsl: the segmentation of the target MRI to the atlas space
out_seg_picsl = jp(data, target_mri, 'segmentation', 'labelmap.nii.gz')

# inv_seg_picsl: the segmentation of the target MRI to the image space
inv_seg_picsl = jp(data, target_mri, 'resampled_space', 'labelmap.nii.gz')

# filtered_seg: the segmentation of the target MRI after filtering
filtered_seg = jp(data, target_mri, 'morphological_operation', 'labelmap.nii.gz')

# rotated_seg: the segmentation of the target MRI rotated to match the needs of
#              hexahedral meshing since it considers a different orientation in
#              this folder are found the extracted stl files from the
#              segmentation using the marching cubes method
rotated_seg = jp(data, target_mri, 'marching_cubes', 'labelmap.nii.gz')

# stl_post_meshlab: folder containing the stl files after they are processed
#                   with meshlab
stl_post_meshlab = jp(data, target_mri, 'stl_postprocessing')

# dn: function that from the absolute name of a file resolves the absolute path
#     of the folder that contains that file.
#
# The files affine.mat, def_transf.nii.gz by default are found in the same
# folder with deform_mri.
affine_mat = jp(dn(affine_mri), 'affine.mat')
def_transf = jp(dn(deform_mri), 'def_transf.nii.gz')
total_transf = jp(dn(deform_mri), 'total_transf.nii.gz')
inv_tot_transf = jp(dn(deform_mri), 'inv_total_transf.nii.gz')

# segmentation masks
masks = {
    1: 'femur.nii.gz',
    2: 'tibia.nii.gz',
    3: 'femoral_cartilage.nii.gz',
    4: 'lateral_tibial_cartilage.nii.gz',
    5: 'medial_tibial_cartilage.nii.gz',
    6: 'lateral_meniscus.nii.gz',
    7: 'medial_meniscus.nii.gz',
    8: 'acl.nii.gz',
    9: 'pcl.nii.gz',
    10: 'patella.nii.gz',
    11: 'fibula.nii.gz',
    12: 'patella_cartilage.nii.gz',
    13: 'lcl.nii.gz',
    14: 'patella_tendon.nii.gz',
    15: 'quadriceps_tendon.nii.gz'
}

##
# functionality

if 1:  # Resample target image

    # Resample the target image to match voxel size and dimensions of the
    # template image. Must be performed with DRAMMS.
    res_dramms(original_mri, resampled_mri, temp_vol, [0.5, 0.5, 0.5])

if 1:  # affine target image
    
    # If the target image was not resampled replace resampled_mri with
    # original_mri.
    affine(resampled_mri, affine_mri, temp_vol)

if 1:  # Deform target image

    # Deform the affine mri so that it much the template image as close as
    # possible.
    deform(affine_mri, deform_mri, temp_vol)

if 1:  # Combine transforms target image

    # This block calculates the total transformation field. However, it is not
    # needed since we will invert only the deformable transformation.
    combine(affine_mat, def_transf, total_transf, resampled_mri, affine_mri)

if 1:  # Invert target image transform

    # The right way is to invert the whole transform. However, because the
    # deformable image space and the initial image space have different
    # image dimensions and possibly spacing this results in erroneous
    # inversions. So we invert only the deformable transformation and
    # the results lie in the affine image space.
    inv_field(total_transf, inv_tot_transf)

if 1:  # Segment target image

    # This blocks reads the warped (registered, transformed) atlas MRIs and
    # labelmaps and resolves the segmentation of the target image.
    warped_mri = np.array([
        jp(data, 'oks001', 'registration', 'resampled.nii.gz'),
        jp(data, 'oks002', 'registration', 'deform_mri.nii.gz'),
        jp(data, 'oks003', 'registration', 'deform_mri.nii.gz'),
        jp(data, 'oks006', 'registration', 'deform_mri.nii.gz'),
        jp(data, 'oks007', 'registration', 'deform_mri.nii.gz'),
        jp(data, 'oks008', 'registration', 'deform_mri.nii.gz'),
        jp(data, 'oks009', 'registration', 'deform_mri.nii.gz')])
    warped_label = np.array([
        jp(data, 'oks001', 'segmentation', 'labelmap.nii.gz'),
        jp(data, 'oks002', 'segmentation', 'def_labelmap.nii.gz'),
        jp(data, 'oks003', 'segmentation', 'def_labelmap.nii.gz'),
        jp(data, 'oks006', 'segmentation', 'def_labelmap.nii.gz'),
        jp(data, 'oks007', 'segmentation', 'def_labelmap.nii.gz'),
        jp(data, 'oks008', 'segmentation', 'def_labelmap.nii.gz'),
        jp(data, 'oks009', 'segmentation', 'def_labelmap.nii.gz')])
    start = time.time()
    segmentation(warped_mri, out_seg_picsl, deform_mri, warped_label)
    end = time.time()
    hours, rem = divmod(end-start, 3600)
    minutes, seconds = divmod(rem, 60)
    print('Time elapsed:')
    print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))

if 1:  # Invert labelmap

    # This block applies the inverse deformable transformation to the output
    # segmentation of the previous block.
    transf_label(out_seg_picsl, inv_tot_transf, inv_seg_picsl, resampled_mri)

if 1:  # Morphological operations segmentation
    
    # This block applies filters to enhance the segmentation. The first
    # line takes as input the absolute path of the segmentation we want to
    # filter. The second input is the absolute path to the folder where the
    # results of filtering are saved. The results are the individual masks
    # that is a dictionary of label value and file names,
    # masks = {label_val: 'mask_name.nii.gz',...}.
    # The second line of code combines the separate masks into a single
    # labelmap based on the label values of the masks dictionary. The order
    # is important in case of overlapping regions. The preceding masks will
    # overlap the later masks.
    morph(inv_seg_picsl, dn(filtered_seg), masks)
    lab(filtered_seg, masks)

if 1:
    # Marching Cubes to extract STL
    # The labelmap is also rotated so the extracted geometries have the same
    # orientation in the Hex Mesh algorithm. For the rotation we used
    # a new affine matrix that is diagonal.
    resample(filtered_seg, rotated_seg,
             np.array([[0.5, 0, 0], [0, 0.5, 0], [0, 0, 0.5]]), True)
    extract_stl(rotated_seg, dn(rotated_seg), masks)

if 0:  # Geometry filtering with Meshlab
    raise NotImplementedError('Do not use yet!')
    input_path = dn(rotated_seg)

    for i in ['femur.stl', 'tibia.stl', 'patella.stl', 'fibula.stl',
              'patella_cartilage.stl']:
        mesh_filt(jp(input_path, i), jp(
            stl_post_meshlab, i), 'femur_tibia.mlx')

    for i in ['lateral_tibial_cartilage.stl',
              'medial_tibial_cartilage.stl',
              'lateral_meniscus.stl',
              'medial_meniscus.stl']:
        mesh_filt(jp(input_path, i), jp(stl_post_meshlab, i),
                  'tibia_cart_men.mlx')

    mesh_filt(jp(input_path, 'femoral_cartilage.stl'),
              jp(stl_post_meshlab, 'femoral_cartilage.stl'),
              'femoral_cartilage.mlx')

##
