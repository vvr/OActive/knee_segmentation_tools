"""
Description: Scripts for creating the atlases.

Requirements:
    Python 3.x
    Libraries: nilearn, numpy, nibabel, scikit-image, numpy-stl, scipy
    Programs: image registration (DRAMMS, flirt), segmentation (PICSL)
    Geometry processing: meshlab

Author: Filip Nikolopoulos
Contributors: Dimitar Stanev, Eva Zacharaki
"""
##
# libraries
import os
import copy
from os.path import join as jp
from os.path import dirname as dn
import numpy as np
from resample import resample2affine as resample
from dramms import affine, deform, combine, transf_label

##
# settings

# data folder
data = os.path.abspath('../data/')

# temp_vol: the MRI used to define the atlas space for the registration
template = 'oks001'
temp_vol = jp(data, template, 'registration', 'resampled.nii.gz')

# training set
data_set = ['oks001', 'oks002', 'oks003', 'oks006', 'oks007', 'oks008', 'oks009']

# output folders (all except the template)
atlases = copy.copy(data_set)
atlases.remove(template)

##
# functionality

if 1:  # Resample Atlases

    # The resample atlases code block changes the voxel size of the atlas images
    # to 0.5 or to what the user decides. To do so, we specify a new affine
    # matrix that incorporates the new voxel size.  If the user decides not to
    # change the voxel size (resample the atlases) there is no need to execute
    # the following block. For our case the block has already been executed. The
    # training set contains the initial not-resampled (Cartilage_image,
    # labelmap) atlases that are in the folders oks001, oks002 ...
    new_affine = np.array([[0, 0, 0.5], [-0.5, 0, 0], [0, -0.5, 0]])
    for atlas in data_set:
        # First resample the MRI
        resample(jp(data, atlas, 'Cartilage_imaging.nii.gz'),
                 jp(data, atlas, 'registration', 'resampled.nii.gz'),
                 new_affine)
        # Then resample the labelmaps
        resample(jp(data, atlas, 'labelmap.nii.gz'),
                 jp(data, atlas, 'segmentation', 'labelmap.nii.gz'),
                 new_affine, label=True)

if 1:  # Affine atlases

    # The affine atlases block performs a affine transform to align the atlases
    # to the temp_vol which is the reference atlas image (or reference volume).
    for atlas in atlases:
        affine(jp(data, atlas, 'registration', 'resampled.nii.gz'),
               jp(data, atlas, 'registration', 'affine.nii.gz'), temp_vol)

if 1:  # Deform atlases

    # The Deform atlases block performs a deformable transform to align the
    # affine atlases to the temp_vol with higher accuracy.
    for atlas in atlases:
        deform(jp(data, atlas, 'registration', 'affine.nii.gz'),
               jp(data, atlas, 'registration', 'deform_mri.nii.gz'),
               temp_vol)

if 1:  # Combine affine and deformable transform into a single file

    # This block combines the affine and deformable transformations derived
    # from the previous two blocks into a single file so that a final total
    # transform can be applied to the atlas labelmaps. The previous two blocks
    # transformed only the MRI not the labelmaps.
    for atlas in atlases:
        affine_file = jp(data, atlas, 'registration', 'affine.mat')
        def_file = jp(data, atlas, 'registration', 'def_transf.nii.gz')
        out_file = jp(data, atlas, 'registration', 'total_transf.nii.gz')
        original_mri = jp(data, atlas, 'registration', 'resampled.nii.gz')
        affine_mri = jp(data, atlas, 'registration', 'affine.nii.gz')
        combine(affine_file, def_file, out_file, original_mri, affine_mri)

if 1:  # Apply total transform to atlas' labelmaps
    for atlas in atlases:
        input_label = jp(data, atlas, 'segmentation', 'labelmap.nii.gz')
        input_field = jp(data, atlas, 'registration', 'total_transf.nii.gz')
        output_label = jp(data, atlas, 'segmentation', 'def_labelmap.nii.gz')
        transf_label(input_label, input_field, output_label, temp_vol)

##
