"""
Description:

TODO

Requirements:
    Python 3.x
    Libraries: nilearn, numpy, nibabel, scikit-image, numpy-stl, scipy
    Programs: image registration (DRAMMS, flirt), segmentation (PICSL)
    Geometry processing: meshlab

Author: Filip Nikolopoulos
Contributors: Dimitar Stanev (stanev@ece.upatras.gr)
"""
import os
from os.path import join as jp
from os.path import dirname as dn
import time
import numpy as np
# from nilearn.image import load_img, crop_img
from resample import resample2affine as resample
from labelmaps import mask2labelmap as lab
from dramms import affine, deform, combine, inv_field, transf_label
from dramms import resample as res_dramms
from geom import label2stl as extract_stl
from morph_oper import morph
from picsl import segmentation
from mesh_filt import mesh_filt

# specify new MRI
unsegmented_mri = '09020300415R'

# parameters
registration_switch = True
segmentation_switch = True

abs_path = os.path.abspath('../data/')
# temp_vol: the MRI used to define the atlas space for the registration
temp_vol = jp(abs_path, 'resampled_volumes', 'oks001',
              'Cartilage_imaging.nii.gz')

# original_mri: the target MRI we want to segment before we apply any procedure
original_mri = jp(abs_path, unsegmented_mri + '.nii.gz')

# resampled_mri: the target MRI with new voxel size
resampled_mri = jp(abs_path, 'resampled_volumes', unsegmented_mri,
                   'resampled.nii.gz')

# affine_mri: the target MRI with affine registration to the atlas space
affine_mri = jp(abs_path, 'reg_dramms', unsegmented_mri, 'affine.nii.gz')

# deform_mri: the target MRI with deformable registration to the atlas space
deform_mri = jp(abs_path, 'reg_dramms', unsegmented_mri, 'deform_mri.nii.gz')

# out_seg_picsl: the segmentation of the target MRI to the atlas space
out_seg_picsl = jp(abs_path, 'seg_dramms', unsegmented_mri, 'Step1',
                   'labelmap.nii.gz')

# inv_seg_picsl: the segmentation of the target MRI to the image space
inv_seg_picsl = jp(abs_path, 'seg_dramms', unsegmented_mri, 'Step2',
                   'labelmap.nii.gz')

# filtered_seg: the segmentation of the target MRI after filtering
filtered_seg = jp(abs_path, 'seg_dramms', unsegmented_mri,
                  'Step3', 'labelmap.nii.gz')

# rotated_seg: the segmentation of the target MRI rotated to match the needs of
#              hexahedral meshing since it considers a different orientation in
#              this folder are found the extracted stl files from the
#              segmentation using the marching cubes method
rotated_seg = jp(abs_path, 'seg_dramms', unsegmented_mri, 'Step4',
                 'labelmap.nii.gz')

# stl_post_meshlab: folder containing the stl files after they are processed
#                   with meshlab
stl_post_meshlab = jp(abs_path, 'seg_dramms', unsegmented_mri, 'Step5')

# dn: function that from the absolute name of a file resolves the absolute path
#     of the folder that contains that file
#
# The files affine.mat, def_transf.nii.gz by default are found in the same
# folder with deform_mri.
affine_mat = jp(dn(affine_mri), 'affine.mat')
def_transf = jp(dn(deform_mri), 'def_transf.nii.gz')
total_transf = jp(dn(deform_mri), 'total_transf.nii.gz')
inv_tot_transf = jp(dn(deform_mri), 'inv_total_transf.nii.gz')

# The user decides which line of code to execute by changing 0 to 1 in the if
# statements.

#################################
#            ATLASES
#################################

if 0:  # Resample Atlases
    # The Resample Atlases code block changes the voxel size of the atlas
    # images to 0.5 or to what the user decides. To do so, we specify a new
    # affine matrix that incorporates the new voxel size.

    # If the user decides to do not change the voxel size (resample the
    # atlases) there is no need to execute the following block. For our case
    # the block has already been executed.
    # The input_path contains the initial not-resampled atlases that are in the
    # folders oks001, oks002 ...
    input_path = jp(abs_path, 'cropped_volumes')
    # The output_path will contain the folders oks001, oks002... that contain
    # the atlases with the new voxel size
    output_path = jp(abs_path, 'resampled_volumes')
    old_mri_folders = ['oks001', 'oks002', 'oks003', 'oks006', 'oks007',
                       'oks008', 'oks009']
    new_affine = np.array([[0, 0, 0.5], [-0.5, 0, 0], [0, -0.5, 0]])

    for i in range(len(old_mri_folders)):
        # First resample the MRI
        resample(jp(input_path, old_mri_folders[i],
                    'Cartilage_imaging.nii.gz'),
                 jp(output_path, old_mri_folders[i],
                    'Cartilage_imaging.nii.gz'),
                 new_affine)
        # Then resample the labelmaps
        resample(jp(input_path, old_mri_folders[i],
                    'labelmap.nii.gz'),
                 jp(output_path, old_mri_folders[i],
                    'labelmap.nii.gz'),
                 new_affine, label=True)

folders = ['oks002', 'oks003', 'oks003', 'oks006', 'oks007', 'oks008',
           'oks009']

if 0:  # affine atlases

    # The affine atlases block performs a affine transform to align the atlases
    # to the temp_vol which is the reference atlas image (or reference volume).

    # This block has already been executed so there is no need to run it again.

    for i in range(len(folders)):
        affine(jp(abs_path, 'resampled_volumes', folders[i],
                  'Cartilage_imaging.nii.gz'),
               jp(abs_path, 'reg_dramms', folders[i],
                  'affine.nii.gz'), temp_vol)

if 0:  # Deform atlases

    # The Deform atlases block performs a deformable transform to align the
    # affine atlases to the temp_vol with higher accuracy.

    # This block has already been executed so there is no need to run it again.

    for i in range(len(folders)):
        deform(jp(abs_path, 'reg_dramms', folders[i],
                  'affine.nii.gz'),
               jp(abs_path, 'reg_dramms', folders[i], 'deform_mri.nii.gz'),
               temp_vol)

if 0:  # Combine affine and deformable transform into a single file

    # This block combines the affine and deformable transformations derived
    # from the previous two blocks into a single file so that a final total
    # transform can be applied to the atlas labelmaps. The previous two blocks
    # transformed only the MRI not the labelmaps.

    # This block has already been executed so there is no need to run it again.

    for i in range(len(folders)):
        affine_file = jp(abs_path, 'reg_dramms', folders[i], 'affine.mat')
        def_file = jp(abs_path, 'reg_dramms', folders[i], 'def_transf.nii.gz')
        out_file = jp(abs_path, 'reg_dramms',
                      folders[i], 'total_transf.nii.gz')
        original_mri = jp(abs_path, 'resampled_volumes', folders[i],
                          'Cartilage_imaging.nii.gz')
        affine_mri = jp(abs_path, 'reg_dramms', folders[i],
                        'affine.nii.gz')
        combine(affine_file, def_file, out_file, original_mri, affine_mri)

if 0:  # Apply total transform to atlas labelmaps
    for i in range(len(folders)):
        input_label = jp(abs_path, 'resampled_volumes', folders[i],
                         'labelmap.nii.gz')
        input_field = jp(abs_path, 'reg_dramms', folders[i],
                         'total_transf.nii.gz')
        output_label = jp(abs_path, 'reg_dramms', folders[i],
                          'def_labelmap.nii.gz')

        transf_label(input_label, input_field, output_label, temp_vol)

#################################
#          TARGET IMAGE
#################################

# Resample target image
if registration_switch:
    # Must resample the target image to match voxel size and dimensions of the
    # template image. Must be performed with DRAMMS.
    #  res_dramms(original_mri, resampled_mri, temp_vol, [0.5, 0.5, 0.5])

    # Affine target image. If the target image was not resampled replace
    # resampled_mri with original_mri.
    affine(resampled_mri, affine_mri, temp_vol, 90)
    # affine(original_mri, affine_mri, temp_vol)

    # Deform target image
    deform(affine_mri, deform_mri, temp_vol)

    # Combine transforms target image. This block calculates the total
    # transformation field. However it is not needed since we will invert only
    # the deformable transformation.
    combine(affine_mat, def_transf, total_transf, resampled_mri, affine_mri)

    # Invert target image transform.  The right way is to invert the whole
    # transform. However, because the deformable image space and the initial
    # image space have different image dimensions and possibly spacing this
    # results in erroneous inversions. So we invert only the deformable
    # transformation and the results lie in the affine image space.
    inv_field(total_transf, inv_tot_transf)
    # inv_field(def_transf, inv_tot_transf)

if segmentation_switch:
    # Segment target image (Step 1). This blocks reads the warped (registered,
    # transformed) atlas MRIs and labelmaps and resolves the segmentation of the
    # target image.
    warped_mri = np.array([jp(abs_path, 'resampled_volumes', 'oks001',
                              'Cartilage_imaging.nii.gz'),
                           jp(abs_path, 'reg_dramms', 'oks002',
                              'deform_mri.nii.gz'),
                           jp(abs_path, 'reg_dramms', 'oks003',
                              'deform_mri.nii.gz'),
                           jp(abs_path, 'reg_dramms', 'oks006',
                              'deform_mri.nii.gz'),
                           jp(abs_path, 'reg_dramms', 'oks007',
                              'deform_mri.nii.gz'),
                           jp(abs_path, 'reg_dramms', 'oks008',
                              'deform_mri.nii.gz'),
                           jp(abs_path, 'reg_dramms', 'oks009',
                              'deform_mri.nii.gz')])
    warped_label = np.array([jp(abs_path, 'resampled_volumes', 'oks001',
                                'labelmap.nii.gz'),
                             jp(abs_path, 'reg_dramms', 'oks002',
                                'def_labelmap.nii.gz'),
                             jp(abs_path, 'reg_dramms', 'oks003',
                                'def_labelmap.nii.gz'),
                             jp(abs_path, 'reg_dramms', 'oks006',
                                'def_labelmap.nii.gz'),
                             jp(abs_path, 'reg_dramms', 'oks007',
                                'def_labelmap.nii.gz'),
                             jp(abs_path, 'reg_dramms', 'oks008',
                                'def_labelmap.nii.gz'),
                             jp(abs_path, 'reg_dramms', 'oks009',
                                'def_labelmap.nii.gz')])
    start = time.time()
    segmentation(warped_mri, out_seg_picsl, deform_mri, warped_label)
    end = time.time()
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    print('Time elapsed:')
    print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))

    # Invert labelmap (Step 2). This block applies the inverse deformable
    # transformation to the output segmentation of the previous block.
    transf_label(out_seg_picsl, inv_tot_transf, inv_seg_picsl, resampled_mri)
    # transf_label(out_seg_picsl, inv_tot_transf, inv_seg_picsl, affine_mri)

    masks = {
        1: 'femur.nii.gz',
        2: 'tibia.nii.gz',
        3: 'femoral_cartilage.nii.gz',
        4: 'lateral_tibial_cartilage.nii.gz',
        5: 'medial_tibial_cartilage.nii.gz',
        6: 'lateral_meniscus.nii.gz',
        7: 'medial_meniscus.nii.gz',
        8: 'acl.nii.gz',
        9: 'pcl.nii.gz',
        10: 'patella.nii.gz',
        11: 'fibula.nii.gz',
        12: 'patella_cartilage.nii.gz',
        13: 'lcl.nii.gz',
        14: 'patella_tendon.nii.gz',
        15: 'quadriceps_tendon.nii.gz'
    }

    # Filter segmentation (Step 3). This block applies filters to enhance the
    # segmentation. The first line takes as input the absolute path of the
    # segmentation we want to filter. The second input is the absolute path to
    # the folder where the results of filtering are saved. The results are the
    # individual masks that is a dictionary of label value and file names, masks
    # = {label_val: 'mask_name.nii.gz',...}.  The second line of code combines
    # the separate masks into a single labelmap based on the label values of the
    # masks dictionary. The order is important in case of overlapping
    # regions. The preceding masks will overlap the later masks.
    morph(inv_seg_picsl, dn(filtered_seg), masks)
    lab(filtered_seg, masks)

    # Marching Cubes to extract STL (Step 4). The labelmap is also rotated so
    # the extracted geometries have the same orientation in the Hex Mesh
    # algorithm. For the rotation we used a new affine matrix that is diagonal.
    resample(filtered_seg, rotated_seg,
             np.array([[0.5, 0, 0], [0, 0.5, 0], [0, 0, 0.5]]), True)
    extract_stl(rotated_seg, dn(rotated_seg), masks)

# this might be deprecated
if 0:  # Geom filtering with Meshlab
    input_path = dn(rotated_seg)

    for i in ['femur.stl', 'tibia.stl', 'patella.stl', 'fibula.stl',
              'patella_cartilage.stl']:
        mesh_filt(jp(input_path, i), jp(
            stl_post_meshlab, i), 'femur_tibia.mlx')

    for i in ['lateral_tibial_cartilage.stl',
              'medial_tibial_cartilage.stl',
              'lateral_meniscus.stl',
              'medial_meniscus.stl']:
        mesh_filt(jp(input_path, i), jp(stl_post_meshlab, i),
                  'tibia_cart_men.mlx')

    mesh_filt(jp(input_path, 'femoral_cartilage.stl'),
              jp(stl_post_meshlab, 'femoral_cartilage.stl'),
              'femoral_cartilage.mlx')
