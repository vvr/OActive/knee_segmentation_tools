from nilearn.image import load_img, new_img_like, crop_img
import numpy as np
from numpy.linalg import inv
import os
from os.path import join as jp
from os.path import dirname as dn


def crop(input_image, template_image, name):
    """
    """

    # T is the template image or the cropped image
    T = load_img(template_image)

    # datT is the data matrix of the template/cropped image
    datT = T.get_data()
    # affT is the affine matrix of the template/cropped image
    affT = T.affine

        # M is the current image that we crop
    M = load_img(input_image)

    # datM is the data matrix of the current mask image
    datM = M.get_data()

    # affM is the affine matrix of the current mask image
    affM = M.affine

    # o are the coordinates of the origin of the cropped image,
    # when it is superimposed over the uncropped image.

    # To find o we multiply the origin affT[:, 3] of the cropped image
    # with the inverse of the affine matrix of the current mask
    # inv(affM).

    o = np.dot(inv(affM), affT[:, 3]).astype(int)

    # So in order to crop the current image we keep only the voxels that
    # start at o up to o + the cropped image dimensions

    cropM = datM[o[0]:T.shape[0]+o[0],
                    o[1]:T.shape[1]+o[1],
                    o[2]:T.shape[2]+o[2]]

    new_mask = new_img_like(T, cropM, copy_header=True)
    new_mask.to_filename(jp(dn(input_image), name))

    
'''
OLD_VERSION
def crop(input_path, output_path, folders, cropped_image, mri, masks):
    """
    This function takes a cropped image from a specific subject and crops
    the rest images (mri and masks) in the same folder.
    Inputs: @input_path = input path of the specimen folders
            @output_path = output path of the specimen folders
            @folders = dictionary of folders containing the specimen mri and
            masks. folders = {1:'oks001', ...}
            @cropped_img = image name that was cropped manually
            @mri = rest of mri names that we want to crop
            @masks = list of masks names that we want to crop.
    """

    # If masks parameter is a dictionary, convert it to a list

    if isinstance(masks, dict):
        temp = []
        for k in masks.keys():
            temp.append(masks[k])
        masks = temp

    for i in folders.keys():

        # T is the template image or the cropped image
        T = load_img(
            jp(output_path, folders[i], cropped_image))

        # datT is the data matrix of the template/cropped image
        datT = T.get_data()
        # affT is the affine matrix of the template/cropped image
        affT = T.affine

        for j in range(len(masks)):

            if not os.path.exists(jp(output_path, folders[i], masks[j])):
                os.makedirs(jp(output_path, folders[i], masks[j]))

            # M is the current mask image that we crop
            M = load_img(
                jp(input_path, folders[i], masks[j]))

            # datM is the data matrix of the current mask image
            datM = M.get_data()

            # affM is the affine matrix of the current mask image
            affM = M.affine

            # o are the coordinates of the origin of the cropped image,
            # when it is superimposed over the uncropped image.

            # To find o we multiply the origin affT[:, 3] of the cropped image
            # with the inverse of the affine matrix of the current mask
            # inv(affM).

            o = np.dot(inv(affM), affT[:, 3]).astype(int)

            # So in order to crop the current mask we keep only the voxels that
            # start at o up to o + the cropped image dimensions

            cropM = datM[o[0]:T.shape[0]+o[0],
                         o[1]:T.shape[1]+o[1],
                         o[2]:T.shape[2]+o[2]]

            new_mask = new_img_like(T, cropM, copy_header=True)
            new_mask.to_filename(
                jp(output_path, folders[i], masks[j]))

        for j in range(len(mri)):

            M = load_img(
                jp(input_path, folders[i], mri[j]))

            datM = M.get_data()
            affM = M.affine

            o = np.dot(inv(affM), affT[:, 3]).astype(int)

            cropM = datM[o[0]:T.shape[0]+o[0],
                         o[1]:T.shape[1]+o[1],
                         o[2]:T.shape[2]+o[2]]

            new_mri = new_img_like(T, cropM, copy_header=True)
            new_mri.to_filename(
                jp(output_path, folders[i], mri[j]))
'''
