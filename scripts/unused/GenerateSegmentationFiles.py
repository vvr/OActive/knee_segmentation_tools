# Generates the required files needed for the segmenation program (picsl).

import collections as col
PathW = "$DATA_DIR/registration/warped_volumes/"
PathT = "$DATA_DIR/registration/target_volume/"
PathP = "$DATA_DIR/posterior_maps/target_posteriors/"
PathWP = "$DATA_DIR/posterior_maps/warped_posterior"
PathCL = "$DATA_DIR/corrective_learning/label"
PathOutSeg = "$DATA_DIR/output_segmentation/"

numb = col.deque([1, 2, 3, 6, 7, 9])
seg_name = ["Background.nii.gz",
            "Bone_femur.nii.gz",
            "Bone_tibia.nii.gz",
            "Cartilage_femoral.nii.gz",
            "Cartilage_lateral_tibial.nii.gz",
            "Cartilage_medial_tibial.nii.gz",
            "Menisci_lateral_meniscus.nii.gz",
            "Menisci_medial_meniscus.nii.gz"]

# Create Joint fusion file for target image

try:
    jf = open('../scripts_picsl/JointFusionTarget.sh', 'w')
    jf.write("#!/bin/bash\n\n")
    jf.write("DATA_DIR=$(cd ../data; pwd)\n")
    jf.write("export PATH=$(cd ./PICSL/build; pwd):PATH\n")
    jf.write("echo $PATH\n")
    jf.write("echo $DATA_DIR\n\n")

    jf.write("jointfusion 3 3 -g " + PathW + "WarpedCartilages1.nii.gz \\")
    jf.write("\n"+PathW + "WarpedGenerals1.nii.gz \\")
    jf.write("\n"+PathW + "WarpedSofts1.nii.gz \\")
    jf.write("\n"+PathW + "WarpedCartilages2.nii.gz \\")
    jf.write("\n"+PathW + "WarpedGenerals2.nii.gz \\")
    jf.write("\n"+PathW + "WarpedSofts2.nii.gz \\")
    jf.write("\n"+PathW + "WarpedCartilages3.nii.gz \\")
    jf.write("\n"+PathW + "WarpedGenerals3.nii.gz \\")
    jf.write("\n"+PathW + "WarpedSofts3.nii.gz \\")
    jf.write("\n"+PathW + "WarpedCartilages6.nii.gz \\")
    jf.write("\n"+PathW + "WarpedGenerals6.nii.gz \\")
    jf.write("\n"+PathW + "WarpedSofts6.nii.gz \\")
    jf.write("\n"+PathW + "WarpedCartilages7.nii.gz \\")
    jf.write("\n"+PathW + "WarpedGenerals7.nii.gz \\")
    jf.write("\n"+PathW + "WarpedSofts7.nii.gz \\")
    jf.write("\n"+PathW + "WarpedCartilages9.nii.gz \\")
    jf.write("\n"+PathW + "WarpedGenerals9.nii.gz \\")
    jf.write("\n"+PathW + "WarpedSofts9.nii.gz \\")

    jf.write("\n-l " + PathW + "WarpedLabelMaps1.nii.gz \\")
    jf.write("\n"+PathW + "WarpedLabelMaps2.nii.gz \\")
    jf.write("\n"+PathW + "WarpedLabelMaps3.nii.gz \\")
    jf.write("\n"+PathW + "WarpedLabelMaps6.nii.gz \\")
    jf.write("\n"+PathW + "WarpedLabelMaps7.nii.gz \\")
    jf.write("\n"+PathW + "WarpedLabelMaps9.nii.gz \\")

    jf.write("\n-m Joint[0.1,2] \\")
    jf.write("\n-rp 2x2x2 \\")
    jf.write("\n-rs 3x3x3 \\")

    jf.write("\n-tg " + PathT + "Cartilage_pad.nii.gz \\")
    jf.write("\n"+PathT + "GenPurpose_pad.nii.gz \\")
    jf.write("\n"+PathT + "Soft_merged_pad.nii.gz \\")
    jf.write("\n-p "+ PathP +"Segm_Joint_posterior%04d.nii.gz \\")
    jf.write("\n"+PathP + "SegJoint.nii.gz")
finally:
    jf.close()

# Create Joint fusion files for warped atlases

for i in range(6):
    try:
        jf = open('../scripts_picsl/JointFusionWarped'+str(numb[0])+'.sh', 'w')
        jf.write("#!/bin/bash\n\n")
        jf.write("DATA_DIR=$(cd ../data; pwd)\n")
        jf.write("export PATH=$(cd ./PICSL/build; pwd):PATH\n")
        jf.write("echo $PATH\n")
        jf.write("echo $DATA_DIR\n\n")

        jf.write("jointfusion 3 3 -g \\")
        for j in range(1, 6):
            jf.write("\n" + PathW + "WarpedCartilages" + str(numb[j]) + ".nii.gz \\")
            jf.write("\n" + PathW + "WarpedGenerals"+ str(numb[j]) + ".nii.gz \\")
            jf.write("\n" + PathW + "WarpedSofts" + str(numb[j]) + ".nii.gz \\")

        jf.write("\n-l \\")
        for j in range(1, 6):
            jf.write("\n"+PathW + "WarpedLabelMaps"+str(numb[j]) +".nii.gz \\")

        jf.write("\n-m Joint[0.1,2] \\")
        jf.write("\n-rp 2x2x2 \\")
        jf.write("\n-rs 3x3x3 \\")
        jf.write("\n-tg " + PathW + "WarpedCartilages" + str(numb[0]) + ".nii.gz \\")
        jf.write("\n"+PathW + "WarpedGenerals" + str(numb[0]) + ".nii.gz \\")
        jf.write("\n"+PathW + "WarpedSofts" + str(numb[0]) + ".nii.gz \\")
        jf.write("\n-p "+ PathWP + str(numb[0]) + "/Segm_Joint_posterior%04d.nii.gz \\")
        jf.write("\n"+PathWP + str(numb[0]) + "/SegJoint.nii.gz")
    finally:
        jf.close()
        numb.rotate(-1)

numb = col.deque([1, 2, 3, 6, 7, 9])

#Create Corrective learning files

for i in range(8):
    try:
        cl = open('../scripts_picsl/CorrectiveLearning'+str(i)+'.sh', 'w')
        cl.write("#!/bin/bash\n\n")
        cl.write("DATA_DIR=$(cd ../data; pwd)\n")
        cl.write("export PATH=$(cd ./PICSL/build; pwd):PATH\n")
        cl.write("echo $PATH\n")
        cl.write("echo $DATA_DIR\n\n")

        cl.write("bl 3 -ms \\")
        for j in range(6):
            cl.write("\n"+PathW+"WarpedLabelMaps"+str(numb[j])+".nii.gz \\")

        cl.write("\n-as \\")
        for j in range(6):
            cl.write("\n"+PathWP + str(numb[0])+"/SegJoint.nii.gz \\")

        cl.write("\n-tl "+str(i)+" \\")
        cl.write("\n-rd 1 \\")
        cl.write("\n-i 500 \\")
        cl.write("\n-rate 0.1 \\")
        cl.write("\n-rf 2x2x2 \\")
        cl.write("\n-c 4 \\")
        cl.write("\n-f \\")
        for j in range(6):
            cl.write("\n"+PathW+"WarpedCartilages" + str(numb[j]) + ".nii.gz \\")
            cl.write("\n"+PathW+"WarpedGenerals" + str(numb[j]) + ".nii.gz \\")
            cl.write("\n"+PathW+"WarpedSofts" + str(numb[j]) + ".nii.gz \\")
            cl.write("\n"+PathWP+str(numb[j]) + "/Segm_Joint_posterior000" + str(i) + ".nii.gz \\")

        cl.write("\n"+PathCL+str(i)+"/Joint_BL")
    finally:
        cl.close()

#Create segmenation correction files

for i in range(8):
    try:
        sc = open('../scripts_picsl/SegCorr'+str(i)+'.sh', 'w')
        sc.write("#!/bin/bash\n\n")
        sc.write("DATA_DIR=$(cd ../data; pwd)\n")
        sc.write("export PATH=$(cd ./PICSL/build; pwd):PATH\n")
        sc.write("echo $PATH\n")
        sc.write("echo $DATA_DIR\n\n")

        sc.write("sa "+PathP+"SegJoint.nii.gz \\")
        sc.write("\n"+PathCL+str(i)+"/Joint_BL \\")
        sc.write("\n"+PathOutSeg+seg_name[i]+" \\")
        sc.write("\n-f "+PathT+"Cartilage_pad.nii.gz \\")
        sc.write("\n"+PathT+"GenPurpose_pad.nii.gz \\")
        sc.write("\n"+PathT+"Soft_merged_pad.nii.gz \\")
        sc.write("\n"+PathP+"Segm_Joint_posterior\%04d.nii.gz \\")
    finally:
        sc.close()
