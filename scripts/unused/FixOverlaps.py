# This code fixes the overlapping of labels by taking into consideration the
# 1x1x1 neighborhood of the labelmap voxels of the warped labelmaps and keeping
# the most probable value

import nibabel as nib

Path = '../data/output_segmentation/'
Pathw = '../data/registration/warped_volumes/'

b0 = Path + 'Bone_femur.nii.gz'
b1 = Path + 'Bone_tibia.nii.gz'
b2 = Path + 'Cartilage_femoral.nii.gz'
b3 = Path + 'Cartilage_lateral_tibial.nii.gz'
b4 = Path + 'Cartilage_medial_tibial.nii.gz'
b5 = Path + 'Menisci_lateral_meniscus.nii.gz'
b6 = Path + 'Menisci_medial_meniscus.nii.gz'

w0 = Pathw + 'WarpedLabelMaps1.nii.gz'
w1 = Pathw + 'WarpedLabelMaps2.nii.gz'
w2 = Pathw + 'WarpedLabelMaps3.nii.gz'
w3 = Pathw + 'WarpedLabelMaps6.nii.gz'
w4 = Pathw + 'WarpedLabelMaps7.nii.gz'
w5 = Pathw + 'WarpedLabelMaps9.nii.gz'

warped = [nib.load(w0), nib.load(w1), nib.load(w2), nib.load(w3), nib.load(w4),
          nib.load(w5)]
mfile = [nib.load(b0), nib.load(b1), nib.load(b2), nib.load(b3), nib.load(b4),
         nib.load(b5), nib.load(b6)]
name = ['Bone_femur', 'Bone_tibia','Cartilage_femoral',
        'Cartilage_lateral_tibial', 'Cartilage_medial_tibial',
        'Menisci_lateral_meniscus','Menisci_medial_meniscus']

dataw = [warped[0].get_data(), warped[1].get_data(), warped[2].get_data(),
         warped[3].get_data(), warped[4].get_data(), warped[5].get_data()]
data = [mfile[0].get_data(), mfile[1].get_data(), mfile[2].get_data(),
        mfile[3].get_data(), mfile[4].get_data(), mfile[5].get_data(),
        mfile[6].get_data()]
dim = data[0].shape

def check(x ,v):
        summ = 0
        for i in range(-1, 2, 1):
                for j in range(-1, 2, 1):
                        for k in range(-1, 2, 1):
                                if (x[i][j][k] == v):
                                        summ += 1

        return summ

x = dim[0]
y = dim[1]
z = dim[2]

totalValues = lambda v : check(dataw[0], v)+check(dataw[1], v)+check(dataw[2], v) \
        + check(dataw[3], v)+check(dataw[4], v)+check(dataw[5], v)

for i in range(2, x - 1):
        for j in range(2, y - 1):
                for k in range(2 ,z - 1):

                        collisionSum = 0

                        collisionList = []

                        for ii in range(len(data)):
                                if (data[ii][i][j][k] != 0):
                                        collisionList.append(ii)
                                        collisionSum += 1

                        if (collisionSum > 1):
                                maxValue = totalValues(collisionList[0])
                                maxIt = 0
                                for jj in range(1, len(collisionList)):
                                        tempVal = totalValues(collisionList[jj])
                                        if (tempVal > maxValue):
                                                maxValue = tempVal
                                                maxIt = jj

                                for jj in range(len(collisionList)):
                                        labelValue = collisionList[jj]
                                        data[labelValue][i][j][k] = maxIt

                        del collisionList

for i in range(len(data)):
        tempImage = nib.Nifti1Image(data[i], mfile[i].affine, mfile[i].header,
                                    mfile[i].extra, mfile[i].file_map)
        nib.save(tempImage, (Path+name[i]+'_NoOverlap.nii.gz'))
