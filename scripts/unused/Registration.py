# This script performs registration between the target image and the Atles and
# save the results in the specified directory. The atlases are transformed so as
# to align with the target specimen.

# To execute this run the following command in Slicer3D
#    execfile("../scripts/Registration.py")

# Information about the parameters of the brains resample module can be found in the following sites:
# https://github.com/tokjun/RegistrationUtilities/blob/master/TransformLabel/TransformLabel.py
# https://github.com/nipy/nipype/blob/master/nipype/interfaces/semtools/registration/brainsresample.py

# Information about the parameters of the brains registration module can be found in the following site:
# https://github.com/nipy/nipype/blob/master/nipype/interfaces/semtools/registration/brainsfit.py

import numpy

grid = [[30,30,30]]
relaxationFactor = [0.5]
maxBSplineDisplacement = [0.5]

# First image must be the template (in this case s8)
Imag = ['s8','s1','s2','s3','s6','s7','s9']

# Path from which we load the images. Data must be organized accordingly (please
# see the organization followed)
Path1 = '../data/openknee/'

# Path to save the images must be absolute
AbsolutePath = '~/dev/personalized_knee_model/data/'

for i in range(1,len(Imag)):
    # Load fixed volume
    [success, fix] = slicer.util.loadVolume(Path1 + Imag[0] +
                                            '/MRI/Cartilage_pad.nii.gz', returnNode=True)
    fix.SetName(Imag[0])

    # Load moving volume, in each iteration it changes
    [success, mov] = slicer.util.loadVolume(Path1 + Imag[i] +
                                            '/MRI/Cartilage_pad.nii.gz', returnNode=True)
    mov.SetName(Imag[i])

    # Create a rigid scale transform node
    outputTransform = slicer.vtkMRMLLinearTransformNode()
    outputTransform.SetName('Rigid_Scale_Transformation')
    slicer.mrmlScene.AddNode(outputTransform)

    # Create a volume to save the results from rigid + scaling the moving volume
    outputVolume=slicer.vtkMRMLScalarVolumeNode()
    outputVolume.SetName('Rigid_Scale_Volume')
    slicer.mrmlScene.AddNode(outputVolume)

    # Create a BSpline transform node
    outputTransform2 = slicer.vtkMRMLBSplineTransformNode()
    outputTransform2.SetName('Spline_Transformation')
    slicer.mrmlScene.AddNode(outputTransform2)

    # Create a volume to save the results from BSpline transform
    outputVolume2=slicer.vtkMRMLScalarVolumeNode()
    outputVolume2.SetName('Spline_Volume')  # of the outputVolume
    slicer.mrmlScene.AddNode(outputVolume2)

    # Finally create a rigid transform node to reorient the final volume
    outputTransform3 = slicer.vtkMRMLBSplineTransformNode()
    # in case the BSpline transform changed any orientation or translation
    outputTransform3.SetName('Final_Rigid_Transformation')
    slicer.mrmlScene.AddNode(outputTransform3)

    # Create a final volume that contains the result of registration
    outputVolume3=slicer.vtkMRMLScalarVolumeNode()
    outputVolume3.SetName('WarpedCartilage'+str(Imag[i]))
    slicer.mrmlScene.AddNode(outputVolume3)

    # Apply Rigid_Affine_Scew transform
    parameters1 = {'fixedVolume' : fix, 'movingVolume' : mov, \
                   'outputVolume' : outputVolume, 'outputTransform' : outputTransform, \
                   'useScaleVersor3D' : True, 'interpolationMode' : 'BSpline', \
                   'initializeTransformMode' : 'useGeometryAlign','costMetric':'NC','splineGridSize' : grid[0]}

    # Execute General Registration (BRAINS)
    slicer.cli.runSync(slicer.modules.brainsfit, parameters=parameters1)


    # Apply BSpline transform
    parameters2 = {'fixedVolume' : fix, 'movingVolume' : outputVolume, \
                   'outputVolume' : outputVolume2, 'outputTransform' : outputTransform2, \
                   'useBSpline' : True, 'interpolationMode' : 'BSpline', 'costMetric':'NC',\
                   'initializeTransformMode' : 'Off','splineGridSize' : grid[0], \
                   'relaxationFactor':relaxationFactor[0],'maxBSplineDisplacement':maxBSplineDisplacement[0]}

    # Execute General Registration (BRAINS)
    slicer.cli.runSync(slicer.modules.brainsfit, parameters=parameters2)

    parameters3 = {'fixedVolume' : fix, 'movingVolume' : outputVolume2, \
                   'outputVolume' : outputVolume3, 'outputTransform' : outputTransform3, \
                   'useRigid' : True, 'interpolationMode' : 'BSpline', \
                   'initializeTransformMode' : 'Off','costMetric':'NC','splineGridSize' : grid[0]}

    # Execute General Registration (BRAINS)
    slicer.cli.runSync(slicer.modules.brainsfit, parameters=parameters3)


    # Here we concatenate the separated tranformations into one final transformation node
    outputTransform2.SetAndObserveTransformNodeID(outputTransform.GetID())
    outputTransform3.SetAndObserveTransformNodeID(outputTransform2.GetID())
    outputTransform3.HardenTransform()
    outputTransform3.SetName('Transformation'+str(Imag[i]))

    # Save transformations
    slicer.util.saveNode(outputTransform3,AbsolutePath
                         +'registration/transformation_files/' + outputTransform3.GetName() + '.h5')
    slicer.util.saveNode(outputVolume3,AbsolutePath
                         +'registration/warped_volumes/' + outputVolume3.GetName() + '.nii.gz')

    # Now we apply the transformation to the other image modalities since sofar
    # only the cartilage_pad is transformed
    [success, gp] = slicer.util.loadVolume(Path1 + Imag[i] +
                                           '/MRI/GenPurpose_pad.nii.gz', returnNode=True)
    gp.SetName('WarpedGeneral'+str(Imag[i]))

    [success, st] = slicer.util.loadVolume(Path1 + Imag[i] +
                                           '/MRI/Soft_merged_pad.nii.gz', returnNode=True)
    st.SetName('WarpedSoft'+str(Imag[i]))

    gp.SetAndObserveTransformNodeID(outputTransform3.GetID())
    gp.HardenTransform()

    st.SetAndObserveTransformNodeID(outputTransform3.GetID())
    st.HardenTransform()

    slicer.util.saveNode(gp,AbsolutePath+'registration/warped_volumes/' +
                         gp.GetName() + '.nii.gz')
    slicer.util.saveNode(st,AbsolutePath+'/registration/warped_volumes/' +
                         st.GetName() + '.nii.gz')

    # In the next we apply the transformations to the labelmaps files using the
    # brains resample module. We do not use the procedure above concerning the
    # other volume modalities since that will ruin the labelmap.  We use brains
    # resample since we want to use for the interpolation mode the Nearest
    # Neighbor feature.
    [success, labelmap] = slicer.util.loadVolume(Path1 + Imag[i] +
                                                 '/Segmentations/labelmap.nii.gz', returnNode=True)
    labelmap.SetName('labelmap')      # I use the name I want to save as a file

    outputVolume_resample=slicer.vtkMRMLScalarVolumeNode()  # BSpline Ouput Volumes Collection
    outputVolume_resample.SetName('WarpedLabelMap'+str(Imag[i]))
    slicer.mrmlScene.AddNode(outputVolume_resample)

    parameters_resample = {'inputVolume' : labelmap, 'referenceVolume' : fix, \
                           'outputVolume' : outputVolume_resample, 'pixelType' : 'float', \
                           'warpTransform' : outputTransform3, 'interpolationMode' : 'NearestNeighbor', \
                           'defaultValue':0.0,'numberOfThreads' : -1}

    slicer.cli.run(slicer.modules.brainsresample, None, parameters_resample,
                   True)
    slicer.util.saveNode(outputVolume_resample,AbsolutePath
                         +'registration/warped_volumes/' + outputVolume_resample.GetName() +
                         '.nii.gz')
    slicer.mrmlScene.Clear(0)

# Here we save the Target Image we want to segment with PICSL to the
# correspoding folder so that PICSL can find it.
[success, gp] = slicer.util.loadVolume(Path1 + Imag[0] +
                                       '/MRI/GenPurpose_pad.nii.gz', returnNode=True)
gp.SetName('GenPurpose_pad')
slicer.util.saveNode(gp,AbsolutePath +'registration/target_volume/' +
                     gp.GetName() + '.nii.gz')

[success, st] = slicer.util.loadVolume(Path1 + Imag[0] +
                                       '/MRI/Soft_merged_pad.nii.gz', returnNode=True)
st.SetName('Soft_merged_pad')
slicer.util.saveNode(st,AbsolutePath +'registration/target_volume/' +
                     st.GetName() + '.nii.gz')

[success, cart] = slicer.util.loadVolume(Path1 + Imag[0] +
                                         '/MRI/Cartilage_pad.nii.gz', returnNode=True)
cart.SetName('Cartilage_pad')
slicer.util.saveNode(cart,AbsolutePath+'registration/target_volume/' +
                     cart.GetName() + '.nii.gz')

slicer.mrmlScene.Clear(0)
