function [] = writemsh( path, physical, nodes, surf, points, hex )

fileID = fopen(path,'w');

fprintf(fileID, '$MeshFormat\n');
fprintf(fileID, '2.2 0 8\n');
fprintf(fileID, '$EndMeshFormat\n');
fprintf(fileID, '$PhysicalNames\n');

[~,m] = size(physical.names);

fprintf(fileID, strcat(num2str(m), '\n'));
for i = 1:m
    fprintf(fileID, '%d %d "%s"\n', physical.type(i),...
        physical.values(i), char(physical.names(i)));
end
fprintf(fileID, '$EndPhysicalNames\n');
fprintf(fileID, '$Nodes\n');

[m,~] = size(nodes);

fprintf(fileID, '%d\n',m);
for i=1:m
    fprintf(fileID, '%d %.4f %.4f %.4f\n', i, nodes(i,1),...
        nodes(i,2), nodes(i,3));
end
fprintf(fileID, '$EndNodes\n');
fprintf(fileID, '$Elements\n');

[m,~] = size(hex.ids);

if (isempty(surf) == 0 && isempty(points))
    [n,~] = size(surf.ids);
    fprintf(fileID, '%d\n', m + n);
    for i = 1:n
        fprintf(fileID, '%d 3 2 %d 1 %d %d %d %d\n', i , surf.physical_values(i),...
            surf.ids(i,1), surf.ids(i,2), surf.ids(i,3), surf.ids(i,4));
    end
elseif (isempty(surf) && isempty(points) == 0)
    [n,~] = size(points.ids);
    fprintf(fileID, '%d\n', m + n);
    for i = 1:n
        fprintf(fileID,'%d 15 2 %d %d %d\n', i,...
            points.physical_values(i), points.ids(i), points.ids(i));
    end
elseif (isempty(surf) == 0 && isempty(points) == 0)
    [ps,~] = size(points.ids);
    [ss,~] = size(surf.ids);
    n = ps + ss;
    fprintf(fileID, '%d\n', m + n);
    for i = 1:ps
        fprintf(fileID,'%d 15 2 %d %d %d\n', i,...
        points.physical_values(i), points.ids(i), points.ids(i));
    end
    for i = 1:ss
        fprintf(fileID, '%d 3 2 %d 1 %d %d %d %d\n', i + ps, surf.physical_values(i),...
        surf.ids(i,1), surf.ids(i,2), surf.ids(i,3), surf.ids(i,4));
    end
else
    n = 0;
    fprintf(fileID, '%d\n', m);
end

for i = 1:m
    if (ismember(3, physical.type))
        fprintf(fileID, '%d 5 2 %d 1 %d %d %d %d %d %d %d %d\n',...
        i + n, hex.layer(i),...
        hex.ids(i,1), hex.ids(i,2),...
        hex.ids(i,3), hex.ids(i,4),...
        hex.ids(i,5), hex.ids(i,6),...
        hex.ids(i,7), hex.ids(i,8));
    else
        fprintf(fileID, '%d 2 2 1 1 %d %d %d\n',...
        i + n, hex.ids(i,1), hex.ids(i,2), hex.ids(i,3));
    end
end

fprintf(fileID, '$EndElements');
fclose(fileID);
end