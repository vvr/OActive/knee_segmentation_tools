function [surf_outer, spring_nodes] = surfaceQuadsSprings(hex, exNodes, anterior, posterior)

% This function takes the external Surface Points, finds in which
% elements they belong and returns the outer faces of these elements
% which are Quads. Also, it returns the points where the springs are
% attached.

% We take the exNodes vector that contains a list of all external nodes
% and we check which of them compose an external face of the corresponing
% element it belongs to using the pattern presented bellow.
%        v
% 4----------3   
% |\     ^   |\     
% | \    |   | \     
% |  \   |   |  \    
% |   8------+---7      
% |   |  +-- |-- | -> u  
% 1---+---\--2   |       
%  \  |    \  \  |      
%   \ |     \  \ |      
%    \|      w  \|        
%     5----------6         

surf_outer = [];
[m,~] = size(hex);

% Anterior Surface from which
% we will find the nodes for the springs
antS = [];
% Posterior Surface from which
% we will find the nodes for the springs
postS = [];
% antS and postS are interchanged in the case of moving from
% the lateral meniscus to the medial meniscus.

for i = 1:m
    % We find the nodes that are external and compose the
    % surface [5 6 7 8]
    % From experiments all these faces compose one of the surfaces
    % upon which the springs are attached.
    if ismember(hex(i,5:8), exNodes)
        surf_outer=[surf_outer;hex(i,5:8)];
        antS = [antS;hex(i,5:8)];
    end
    % We find the nodes that are external and compose the
    % surface [1 2 3 4]
    % From experiments all these faces compose the other of the surfaces
    % upon which the springs are attached.
    if ismember(hex(i,1:4), exNodes)
        surf_outer=[surf_outer;fliplr(hex(i,1:4))];
        postS = [postS;fliplr(hex(i,1:4))];
    end
    % We find the nodes that are external and compose the
    % surface [2 6 7 3]
    temp = [hex(i,2) hex(i,6) hex(i,7) hex(i,3)];
    if ismember(temp, exNodes)
        surf_outer = [surf_outer;fliplr(temp)];
    end
    % We find the nodes that are external and compose the
    % surface [3 4 8 7]
    temp = [hex(i,3) hex(i,4) hex(i,8) hex(i,7)];
    if ismember(temp, exNodes)
        surf_outer = [surf_outer;temp];
    end
    % We find the nodes that are external and compose the
    % surface [1 2 6 5]
    temp = [hex(i,1) hex(i,2) hex(i,6) hex(i,5)];
    if ismember(temp, exNodes)
        surf_outer = [surf_outer;temp];
    end
    % We find the nodes that are external and compose the
    % surface [1 5 8 4]
    temp = [hex(i,1) hex(i,5) hex(i,8) hex(i,4)];
    if ismember(temp, exNodes)
        surf_outer = [surf_outer;temp];
    end  
end

ant = unique(antS(:,1:4));
[n,~] = size(ant);
spring_nodes_ant.ids = ant;
spring_nodes_ant.physical_values = anterior * ones(n,1);

% If we are in the case of the medial meniscus the above value
% is posterior*ones(n,1). This is accounted by the order we enter the
% parameters to this function.

post = unique(postS(:,1:4));
[n,~] = size(post);
spring_nodes_post.ids = post;
spring_nodes_post.physical_values = posterior * ones(n,1);

spring_nodes.ids = [spring_nodes_ant.ids;spring_nodes_post.ids];
spring_nodes.physical_values = ...
    [spring_nodes_ant.physical_values;spring_nodes_post.physical_values];
end

