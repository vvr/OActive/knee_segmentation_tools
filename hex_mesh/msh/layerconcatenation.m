function [hex] = layerconcatenation(cartilage)

% This function combines layers 1 & 2 in one layer (1)
% layers 3 & 4 in another layer (2)
% and layers 5 & 6 in a final layer (3).
% It returns a new hex structure in which each element
% belongs to a new layer created from the method above.

[m,~] = size(cartilage.hex);

id = [];
layer = [];
for i = 1:m
   if (cartilage.layers(i) == 1 || cartilage.layers(i) == 2)
       layer_value = 1;
   end
   if (cartilage.layers(i) == 3 || cartilage.layers(i) == 4)
       layer_value = 2;
   end
   if (cartilage.layers(i) == 5 || cartilage.layers(i) == 6)
       layer_value = 3;
   end
   layer = [layer;layer_value];
   id = [id;cartilage.hex(i,:)];
end

hex.ids = id;
hex.layer = layer;

end

