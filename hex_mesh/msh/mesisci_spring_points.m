function [t_ant_lat, t_post_lat, t_ant_med, t_post_med] = ...
    mesisci_spring_points(pathMenLat, pathMenMed, v_tib)
%% Points Lateral Meniscus
[~, p_ant_lat, p_post_lat] = readmshnodes(pathMenLat);
%% Points Medial Meniscus
[~, p_ant_med, p_post_med] = readmshnodes(pathMenMed);
%% Main method

% Centroid of anterior & posterior points
% Centroid anterior lateral meniscus spring nodes
CAL = [mean(p_ant_lat(:,1)) mean(p_ant_lat(:,2)) mean(p_ant_lat(:,3))];
% Centroid posterior lateral meniscus spring nodes
CPL = [mean(p_post_lat(:,1)) mean(p_post_lat(:,2)) mean(p_post_lat(:,3))];
% Centroid anterior medial meniscus spring nodes
CAM = [mean(p_ant_med(:,1)) mean(p_ant_med(:,2)) mean(p_ant_med(:,3))];
% Centroid posterior medial meniscus spring nodes
CPM = [mean(p_post_med(:,1)) mean(p_post_med(:,2)) mean(p_post_med(:,3))];

% Vector from posterior cetroid of the medial meniscus to the anterior
% centroid of the lateral meniscus.
% VALPM (VECTOR ANTERIOR LATERAL POSTERIOR MEDIAL
VALPM = CAL - CPM;
% Vector from posterior cetroid of the lateral meniscus to the anterior
% centroid of the medial meniscus.
% VAMPL (VECTOR ANTERIOR MEDIAL POSTERIOR LATERAL
VAMPL = CAM - CPL;

% The above two vectors form an X between the anterior and posterior horns
% of the two menisci.

% virtual points that will give the closest points to the tibia upon which
% the springs will be attached
v_ant_lat = CAL - 0.25 * VALPM;
v_post_lat = CPL + 0.25 * VAMPL; 
v_ant_med = CAM - 0.5 * VAMPL;
v_post_med = CPM + 0.5 * VALPM;

% find the points to the tibia
t_ant_lat = findmatch(v_tib, v_ant_lat);
t_post_lat = findmatch(v_tib, v_post_lat);
t_ant_med = findmatch(v_tib, v_ant_med);
t_post_med = findmatch(v_tib, v_post_med);
end

