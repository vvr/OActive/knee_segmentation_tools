function [] = writeObj(inputMesh,filename)

fid = fopen(filename,'w+t');

[nX,] = size(inputMesh.nodes);
Quads = Hex2Quads(inputMesh.hex);
[eX,] = size(Quads);


for i=1:1:nX
    fprintf(fid, 'v %.4f %.4f %.4f\n',inputMesh.nodes(i,1),inputMesh.nodes(i,2),...
        inputMesh.nodes(i,3));
end

for i=1:1:eX
    fprintf(fid, 'f %d %d %d %d\n',Quads(i,1),Quads(i,2),...
        Quads(i,3), Quads(i,4));
end
fclose(fid);
end

