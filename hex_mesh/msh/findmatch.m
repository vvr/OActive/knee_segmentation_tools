function [bone_node] = findmatch(bone_points, meniscus_point)

[m,~] = size(bone_points);
minValue = 50000;
bone_node = 1;

for i = 1:m
    
    temp_bone_point = bone_points(i,:);
    
    dist = norm(temp_bone_point - meniscus_point);   
    
    if dist < minValue
        minValue = dist;
        bone_node = i;
    end
    
end
end

