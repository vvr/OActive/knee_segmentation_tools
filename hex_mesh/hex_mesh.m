% define subject dir
subject_dir = '../data/29129401088R/';

% input and output dirs
input_dir = [subject_dir, 'marching_cubes/'];
output_dir = [subject_dir, 'hex_mesh/'];
mkdir(output_dir)

% add hex_mesh software functions
addpath('hex_mesh/');

% stl files
addpath(input_dir);
femoral_cartilage = 'femoral_cartilage.stl';
tibial_cartilage_lateral = 'lateral_tibial_cartilage.stl';
tibial_cartilage_medial = 'medial_tibial_cartilage.stl';
meniscus_lateral = 'lateral_meniscus.stl';
meniscus_medial = 'medial_meniscus.stl';
patellar_cartilage = 'patellar.stl';

% execute hexmesh
[femoral_cartilage_m, tibial_cartilage_lateral_m, tibial_cartilage_medial_m, ...
    meniscus_lateral_m, meniscus_medial_m ] = main( femoral_cartilage, ...
    tibial_cartilage_lateral, tibial_cartilage_medial, meniscus_lateral, ...
    meniscus_medial);

% save mesh results
save([output_dir, 'femoral_cartilage_m.mat'], 'femoral_cartilage_m');
save([output_dir, 'meniscus_lateral_m.mat'], 'meniscus_lateral_m');
save([output_dir, 'meniscus_medial_m.mat'], 'meniscus_medial_m');
save([output_dir, 'tibial_cartilage_medial_m.mat'], 'tibial_cartilage_medial_m');
save([output_dir, 'tibial_cartilage_lateral_m.mat'], 'tibial_cartilage_lateral_m');
% visualize(meniscus_medial_m);

saveFemoral([output_dir, 'femoral_cartilage_m.mat'], ...
            [output_dir, 'femoral_cartilage.msh']); 
saveLTibial([output_dir, 'tibial_cartilage_lateral_m.mat'], ...
            [output_dir, 'lateral_tibial_cartilage.msh']); 
saveMTibial([output_dir, 'tibial_cartilage_medial_m.mat'], ...
            [output_dir, 'medial_tibial_cartilage.msh']); 
saveLMeniscus([output_dir, 'meniscus_lateral_m.mat'], ...
              [output_dir, 'lateral_meniscus.msh']);
saveMMeniscus([output_dir, 'meniscus_medial_m.mat'], ...
              [output_dir,'medial_meniscus.msh']);
% saveFemur([input_dir, 'femur.stl'], [output_dir, 'femur.msh']);
% saveTibia([input_dir, 'tibia.stl'], [output_dir, 'tibia.msh'], ...
%           [output_dir, 'lateral_meniscus.msh'], ... 
%           [output_dir, 'medial_meniscus.msh']);
