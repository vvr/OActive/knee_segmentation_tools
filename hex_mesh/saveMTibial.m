function [] = saveMTibial(matfile_path,export_file_name)
addpath('msh/');

load(matfile_path, 'tibial_cartilage_medial_m');

% START Header
val_bone_connection = 4;
val_contact = 5;
physical_node = 0;
physical_surface = 2;
physical_volume = 3;

physical.names = {'bone_connection_nodes', 'contact_surface',...
    'bone_layer', 'inner_layer', 'outer_layer'};
physical.type = [physical_node physical_surface ...
    physical_volume physical_volume physical_volume];
physical.values = [val_bone_connection val_contact 1 2 3];
% END Header

[m,~] = size(tibial_cartilage_medial_m.hex);

elemLayer1 = [];
elemLayer2 = [];
elemLayer6 = [];

for i = 1:m
    if tibial_cartilage_medial_m.layers(i) == 1
        elemLayer1 = [elemLayer1; tibial_cartilage_medial_m.hex(i,:)];
    end
    if tibial_cartilage_medial_m.layers(i) == 2
        elemLayer2 = [elemLayer2; tibial_cartilage_medial_m.hex(i,:)];
    end
    if tibial_cartilage_medial_m.layers(i) == 6
        elemLayer6 = [elemLayer6; tibial_cartilage_medial_m.hex(i,:)];
    end
end

[s,~] = size(elemLayer6);
surfaces.ids = fliplr(elemLayer6(:,1:4));
surfaces.physical_values = val_contact * ones(s,1);

nodes1 = setdiff(elemLayer1, elemLayer2);
[s,~] = size(nodes1);
connection_nodes.ids = nodes1;
connection_nodes.physical_values = val_bone_connection * ones(s,1);

hex = layerconcatenation(tibial_cartilage_medial_m);

writemsh(export_file_name, physical, tibial_cartilage_medial_m.nodes, surfaces, connection_nodes, hex);
end

