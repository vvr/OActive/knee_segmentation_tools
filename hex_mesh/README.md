# Description

The following utilities are used to refine the .stl files obtained
from the automatic segmentation. These utilities were developed
B. Rodríguez-Vila et al. (see acknowledgment). We fixed some issues in
the initial code so that the tools are more robust to problems in the
raw geometry.

The user should run the `hex_mesh.m` script.

# Acknowledgment

Automated Hexahedral Meshing of Knee Cartilage Structures
B. Rodríguez-Vila and D.M. Pierce
Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
University of Connecticut, Storrs
June 05, 2017
Copyright (c) 2017, All Rights Reserved.
*CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED.
