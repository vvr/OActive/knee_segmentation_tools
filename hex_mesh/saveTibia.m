function [] = saveTibia(stl_path,export_file_name,lat_men_msh_path,med_men_msh_path)
addpath('msh/');

[v_t, f_t] = stlread(stl_path);


physical.names = {'anterior_lateral_node','posterior_lateral_node',...
    'anterior_medial_node','posterior_medial_node','surface'};
physical.type = [0 0 0 0 2];
physical.values = [2 3 4 5 1];

if 0
    [al, pl, am, pm] = mesisci_spring_points(lat_men_msh_path,med_men_msh_path,v_t);
else
    al = [];
    pl = [];
    am = [];
    pm = [];
end
spring_nodes.ids = [al;pl;am;pm;];
spring_nodes.physical_values = [2;3;4;5];
faces.ids = f_t;
writemsh(export_file_name,physical,v_t,[],spring_nodes,faces);
end

