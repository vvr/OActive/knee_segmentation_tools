function [ rays, points ] = RaysForMedial( PM,orig,RadialRes )
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Radial resolution: angles in a single plane
    %Creation of the angles of the rays
    theta = (0:360/RadialRes-1)*RadialRes;
    siz = size(theta,2);
    recta3D = [cosd(theta); sind(theta); zeros(1,siz)];
    %Rotation of the rays to be perpendicular to the main vector
    R = rotateAtoB([0,0,1]',[1,0,0]');
    r = R*recta3D;
%% Creation of the spherical extremes
    % Angles of the rays
    phi = (0:90/(0.5*RadialRes)-1)*(0.5*RadialRes);
    p = fliplr(phi);
    t = fliplr(theta);
    recta3D_right = [];
    for ii=1:size(phi,2)
        recta3D_right = [recta3D_right, [cosd(theta).*sind(p(ii)); sind(theta).*sind(p(ii)); ones(1,siz).*cosd(p(ii))]];
    end
    casquete_right = R*recta3D_right;
%% Longitudinal resolution
% Create an auxiliar point that helps to define the application points 
    Point = orig - 2*(PM - orig); 
    Point(1) = Point(1) - 15;
    points = repmat(Point',1,size(casquete_right,2))';
    rays = casquete_right';
end

