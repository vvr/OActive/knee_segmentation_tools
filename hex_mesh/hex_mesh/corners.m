function [hex, nodes2] = corners( hex,nodes, ind6nodes )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

nodes2 = nodes;
for ii = 1:size(ind6nodes,1)
    switch find(isnan(nodes(hex(:,ind6nodes(ii)),1)), 1,'first')
        case 1
            nodes2(hex(2,ind6nodes(ii)),:) = nodes(hex(2,ind6nodes(ii)),:)...
                +nodes(hex(4,ind6nodes(ii)),:)-...
                nodes(hex(3,ind6nodes(ii)),:);
            nodes2(hex(4,ind6nodes(ii)),:) = nodes2(hex(2,ind6nodes(ii)),:);
            nodes2(hex(6,ind6nodes(ii)),:) = nodes(hex(6,ind6nodes(ii)),:)...
                +nodes(hex(8,ind6nodes(ii)),:)-...
                nodes(hex(7,ind6nodes(ii)),:);
            nodes2(hex(8,ind6nodes(ii)),:) = nodes2(hex(6,ind6nodes(ii)),:);
        case 2
            nodes2(hex(1,ind6nodes(ii)),:) = nodes(hex(1,ind6nodes(ii)),:)...
                +nodes(hex(3,ind6nodes(ii)),:)-...
                nodes(hex(4,ind6nodes(ii)),:);
            nodes2(hex(3,ind6nodes(ii)),:) = nodes2(hex(1,ind6nodes(ii)),:);
            nodes2(hex(5,ind6nodes(ii)),:) = nodes(hex(5,ind6nodes(ii)),:)...
                +nodes(hex(7,ind6nodes(ii)),:)-...
                nodes(hex(8,ind6nodes(ii)),:);
            nodes2(hex(7,ind6nodes(ii)),:) = nodes2(hex(5,ind6nodes(ii)),:);
        case 3
            nodes2(hex(4,ind6nodes(ii)),:) = nodes(hex(4,ind6nodes(ii)),:)...
                +nodes(hex(2,ind6nodes(ii)),:)-...
                nodes(hex(1,ind6nodes(ii)),:);
            nodes2(hex(2,ind6nodes(ii)),:) = nodes2(hex(4,ind6nodes(ii)),:);
            nodes2(hex(8,ind6nodes(ii)),:) = nodes(hex(8,ind6nodes(ii)),:)...
                +nodes(hex(6,ind6nodes(ii)),:)-...
                nodes(hex(5,ind6nodes(ii)),:);
            nodes2(hex(6,ind6nodes(ii)),:) = nodes2(hex(8,ind6nodes(ii)),:);
        case 4
            nodes2(hex(3,ind6nodes(ii)),:) = nodes(hex(3,ind6nodes(ii)),:)...
                +nodes(hex(1,ind6nodes(ii)),:)-...
                nodes(hex(2,ind6nodes(ii)),:);
            nodes2(hex(1,ind6nodes(ii)),:) = nodes2(hex(3,ind6nodes(ii)),:);
            nodes2(hex(7,ind6nodes(ii)),:) = nodes(hex(7,ind6nodes(ii)),:)...
                +nodes(hex(5,ind6nodes(ii)),:)-...
                nodes(hex(6,ind6nodes(ii)),:);
            nodes2(hex(5,ind6nodes(ii)),:) = nodes2(hex(7,ind6nodes(ii)),:);
    end

end

hex(:,ind6nodes) = [];

end

