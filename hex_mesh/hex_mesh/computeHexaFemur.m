function hex = computeHexaFemur( s2, s3 )
%computeHexaFemur Computes hexahedral elements of the mesh
%
% Inputs:
%   s2 number of elements in the radial direction
%   s3 total number of elements
%
% Outputs:
%   hex: hexahedral elements <Mx8>
%
%
%
% Author: Borja Rodriguez-Vila
% E-mail: borja.rodriguez.vila@upm.es
%% Create index to the nodes 
indexL = (1:s3*2);
index = reshape(indexL,s2,s3/s2,2);
%% Associate each node with its neighbors
% Initialize the number of hexahedra and its 8 components
hex = NaN(8,s2-1,s3/s2-1);
% for each radial direction
for ii=1:s2-1
    % for each axial direction
    for jj=1:s3/s2-1
%   
%       4 ---------- 3
%      /|           /|
%     / |          / |
%    1 -|-------- 2  |
%    |  8 --------|- 7
%    | /          | /
%    |/           |/
%    5 ---------- 6
%
        hex(1,ii,jj) = index(ii,jj,1);
        hex(2,ii,jj) = index(ii+1,jj,1);
        hex(3,ii,jj) = index(ii+1,jj+1,1);
        hex(4,ii,jj) = index(ii,jj+1,1);
        hex(5,ii,jj) = index(ii,jj,2);
        hex(6,ii,jj) = index(ii+1,jj,2);
        hex(7,ii,jj) = index(ii+1,jj+1,2);
        hex(8,ii,jj) = index(ii,jj+1,2);
    end
end
    
hex = reshape(hex, 8, []);
end

