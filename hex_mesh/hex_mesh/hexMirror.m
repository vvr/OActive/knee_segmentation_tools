function [ hex, nodes ] = hexMirror( hex, nodes, indMirror)
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%       4 ---------- 3 ---------- 10
%      /|           /|           /|
%     / |          / |          / |
%    1 -|-------- 2 -|-------- 9  |
%    |  8 --------|- 7 --------|- 12
%    | /          | /          | /
%    |/           |/           |/
%    5 ---------- 6 ---------- 11

% Nodes 2 and 3 are NaN...
% This codes finds the commom NaN nodes
% The opposite not shared faces 1-4-5-8 and 9-10-11-12
% And interpolates the value (1 + 5 + 9 + 11)/4 to find coor of node 2
% And interpolates the value (4 + 8 + 10 + 12)/4 to find coor of node 3

if numel(indMirror)>0
    if size(indMirror,2)==1
        indMirror = indMirror';
    end
    for ii=1:size(indMirror,1)
        % take the node ids of the 6 node hex element
        n1 = hex(:,indMirror(ii,:));
        % keep the unique node ids only since 
        % the hexes have shared nodes
        NN = unique(n1);
        % keep only the NaN nodes
        elements = NN(isnan(nodes(NN,1)));
        if isempty(elements)
            continue;
        end
        % vectorize the 6 nodes ids found 2 commands above
        n1 = n1(:);
        % Find which elements of indMirror(:,1) are shared by indMirror(:,2)
        % and vise versa
        indEQ = [ismember(hex(:,indMirror(ii,1)),hex(:,indMirror(ii,2)));...
            ismember(hex(:,indMirror(ii,2)),hex(:,indMirror(ii,1)))];
        % from these indexes eliminate the values of n1
        % Find the node ids of the two opposite not common faces
        n1(indEQ) = [];
        % Interpolate the missing node by averaging the two side
        % edges of the opposite not common faces
        nodes(elements(1),:) = sum(nodes(n1([1,2,5,6]),:))/4;
        nodes(elements(2),:) = sum(nodes(n1([3,4,7,8]),:))/4;
    end
end

end

