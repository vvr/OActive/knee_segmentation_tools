function Vol = HexVolume( nodes, hex )
%HexVolume computes the volume of hexahedra
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

siz = size(hex,1);
Vol = NaN(siz,1);

for ii=1:siz
    X = NaN(8,3);
    for jj=1:8
        X(jj,:) = nodes(hex(ii,jj),:);
    end
    [K,v] = convhulln(X);
    Vol(ii)=v;
end

end

