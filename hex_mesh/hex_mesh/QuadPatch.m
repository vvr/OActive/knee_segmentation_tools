function V = QuadPatch( p0,p1,p2,p3, ratioH, ratioV )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

% take p0 and p1 and interpolate ratioH - 1 points
% between them
v1 = divideSize(p0,p1,ratioH);
v2 = divideSize(p2,p3,ratioH);
% initialize a matrix of points
V = NaN(2,ratioV+1,ratioH+1);
% create a grid of interpolated points
% between the 4 corners p0, p1, p2, p3
for i=1:ratioH+1
    V(:,:,i) = divideSize(v1(:,i),v2(:,i),ratioV);
end

end

