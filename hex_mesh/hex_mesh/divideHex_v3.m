function [nodes, hex, layer] = divideHex_v3(nodes, hex, layer, elements)
% divideHex divides the hexahedra pointed by elements into 2 wedges.
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

for ii = 1:size(elements,1)
    
    L = layer(elements(ii));
    %% Compute the internal angles of hexahedron
    % cos(theta) = dot(v1,v2)./(norm(v1)*norm(v2))
    v1_1 = nodes(hex(elements(ii),2),:)-nodes(hex(elements(ii),1),:);
    v2_1 = nodes(hex(elements(ii),4),:)-nodes(hex(elements(ii),1),:);
    v3_1 = nodes(hex(elements(ii),5),:)-nodes(hex(elements(ii),1),:);
    theta1_1 = acosd(dot(v1_1,cross(v2_1,v3_1))./(norm(v1_1)*norm(cross(v2_1,v3_1))));
    theta1_2 = acosd(dot(v2_1,cross(v3_1,v1_1))./(norm(v2_1)*norm(cross(v3_1,v1_1))));
    theta1_3 = acosd(dot(v3_1,cross(v1_1,v2_1))./(norm(v3_1)*norm(cross(v1_1,v2_1))));
    
    v1_2 = nodes(hex(elements(ii),3),:)-nodes(hex(elements(ii),2),:);
    v2_2 = nodes(hex(elements(ii),1),:)-nodes(hex(elements(ii),2),:);
    v3_2 = nodes(hex(elements(ii),6),:)-nodes(hex(elements(ii),2),:);
    theta2_1 = acosd(dot(v1_2,cross(v2_2,v3_2))./(norm(v1_2)*norm(cross(v2_2,v3_2))));
    theta2_2 = acosd(dot(v2_2,cross(v3_2,v1_2))./(norm(v2_2)*norm(cross(v3_2,v1_2))));
    theta2_3 = acosd(dot(v3_2,cross(v1_2,v2_2))./(norm(v3_2)*norm(cross(v1_2,v2_2))));
    
    v1_3 = nodes(hex(elements(ii),4),:)-nodes(hex(elements(ii),3),:);
    v2_3 = nodes(hex(elements(ii),2),:)-nodes(hex(elements(ii),3),:);
    v3_3 = nodes(hex(elements(ii),7),:)-nodes(hex(elements(ii),3),:);
    theta3_1 = acosd(dot(v1_3,cross(v2_3,v3_3))./(norm(v1_3)*norm(cross(v2_3,v3_3))));
    theta3_2 = acosd(dot(v2_3,cross(v3_3,v1_3))./(norm(v2_3)*norm(cross(v3_3,v1_3))));
    theta3_3 = acosd(dot(v3_3,cross(v1_3,v2_3))./(norm(v3_3)*norm(cross(v1_3,v2_3))));

    v1_4 = nodes(hex(elements(ii),1),:)-nodes(hex(elements(ii),4),:);
    v2_4 = nodes(hex(elements(ii),3),:)-nodes(hex(elements(ii),4),:);
    v3_4 = nodes(hex(elements(ii),8),:)-nodes(hex(elements(ii),4),:);
    theta4_1 = acosd(dot(v1_4,cross(v2_4,v3_4))./(norm(v1_4)*norm(cross(v2_4,v3_4))));
    theta4_2 = acosd(dot(v2_4,cross(v3_4,v1_4))./(norm(v2_4)*norm(cross(v3_4,v1_4))));
    theta4_3 = acosd(dot(v3_4,cross(v1_4,v2_4))./(norm(v3_4)*norm(cross(v1_4,v2_4))));

    v1_5 = nodes(hex(elements(ii),8),:)-nodes(hex(elements(ii),5),:);
    v2_5 = nodes(hex(elements(ii),6),:)-nodes(hex(elements(ii),5),:);
    v3_5 = nodes(hex(elements(ii),1),:)-nodes(hex(elements(ii),5),:);
    theta5_1 = acosd(dot(v1_5,cross(v2_5,v3_5))./(norm(v1_5)*norm(cross(v2_5,v3_5))));
    theta5_2 = acosd(dot(v2_5,cross(v3_5,v1_5))./(norm(v2_5)*norm(cross(v3_5,v1_5))));
    theta5_3 = acosd(dot(v3_5,cross(v1_5,v2_5))./(norm(v3_5)*norm(cross(v1_5,v2_5))));

    v1_6 = nodes(hex(elements(ii),5),:)-nodes(hex(elements(ii),6),:);
    v2_6 = nodes(hex(elements(ii),7),:)-nodes(hex(elements(ii),6),:);
    v3_6 = nodes(hex(elements(ii),2),:)-nodes(hex(elements(ii),6),:);
    theta6_1 = acosd(dot(v1_6,cross(v2_6,v3_6))./(norm(v1_6)*norm(cross(v2_6,v3_6))));
    theta6_2 = acosd(dot(v2_6,cross(v3_6,v1_6))./(norm(v2_6)*norm(cross(v3_6,v1_6))));
    theta6_3 = acosd(dot(v3_6,cross(v1_6,v2_6))./(norm(v3_6)*norm(cross(v1_6,v2_6))));

    v1_7 = nodes(hex(elements(ii),6),:)-nodes(hex(elements(ii),7),:);
    v2_7 = nodes(hex(elements(ii),8),:)-nodes(hex(elements(ii),7),:);
    v3_7 = nodes(hex(elements(ii),3),:)-nodes(hex(elements(ii),7),:);
    theta7_1 = acosd(dot(v1_7,cross(v2_7,v3_7))./(norm(v1_7)*norm(cross(v2_7,v3_7))));
    theta7_2 = acosd(dot(v2_7,cross(v3_7,v1_7))./(norm(v2_7)*norm(cross(v3_7,v1_7))));
    theta7_3 = acosd(dot(v3_7,cross(v1_7,v2_7))./(norm(v3_7)*norm(cross(v1_7,v2_7))));

    v1_8 = nodes(hex(elements(ii),7),:)-nodes(hex(elements(ii),8),:);
    v2_8 = nodes(hex(elements(ii),5),:)-nodes(hex(elements(ii),8),:);
    v3_8 = nodes(hex(elements(ii),4),:)-nodes(hex(elements(ii),8),:);
    theta8_1 = acosd(dot(v1_8,cross(v2_8,v3_8))./(norm(v1_8)*norm(cross(v2_8,v3_8))));
    theta8_2 = acosd(dot(v2_8,cross(v3_8,v1_8))./(norm(v2_8)*norm(cross(v3_8,v1_8))));
    theta8_3 = acosd(dot(v3_8,cross(v1_8,v2_8))./(norm(v3_8)*norm(cross(v1_8,v2_8))));
    
    
    theta = [theta1_1,theta1_2,theta1_3;...
        theta2_1,theta2_2,theta2_3;...
        theta3_1,theta3_2,theta3_3;...
        theta4_1,theta4_2,theta4_3;...
        theta5_1,theta5_2,theta5_3;...
        theta6_1,theta6_2,theta6_3;...
        theta7_1,theta7_2,theta7_3;...
        theta8_1,theta8_2,theta8_3];
    
    [a,b] = ind2sub(size(theta),find(theta>45));
    bad_ones = unique(a);
    
end


end

