function points = distribution(type,p,vL,fL,LH,LV,ratioV,ratioH,radius)
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%Puntos del rectangulo interior
switch type
    case 'lateral'
        % Take the origin point PL for lateral and create an box
        % (4 corner points LH, LV) around it, by projecting all points
        % on Z = 0 since we take p(1:2) x and y dimensions
        
        p1 = p(1:2) + [LH,LV];
        p2 = p(1:2) + [LH,-LV];
        p3 = p(1:2) + [-LH,-LV];
        p4 = p(1:2) + [-LH,LV];
        
        % We divide a circle in 4 areas like and X.
        % RatioH determines the density on top and down area 45-135o and
        % 225 - 315 whereas RatioV determines the density on right and left        
        % areas
        
        angles = [45:90/ratioH:135,135:90/ratioV:225,225:90/ratioH:315,315:90/ratioV:405]';
    case 'medial'
        p1 = p(1:2) + [0,LH];
        p2 = p(1:2) + [LV,0];
        p3 = p(1:2) + [0,-LH];
        p4 = p(1:2) + [-LV,0];
        angles = [90:90/ratioH:180,180:90/ratioV:270,270:90/ratioH:360,360:90/ratioV:450]';
end

%Patch1
% Create a grid of points between the 4 corners

V1 = QuadPatch( p1,p2,p4,p3,ratioV,ratioH );

v = vL;

% Project all vertices of the initial mesh to the Z = 0 plane

v(:,3) = 0;

% Divide the columns of the face list so each line contains
% one vertice id, and replace the vertice id with the coordinates
% So ie v0(5,:), v1(5,:), v2(5,:) contain the vertice coordinates
% of the face ids in fL(5,:)... Note that they are projected on 
% Z = 0 plane

v0 = v(fL(:,1),:);
v1 = v(fL(:,2),:);
v2 = v(fL(:,3),:);

points = reshape(V1,2,[]);

% patch('vertices',vL,'faces',fL)
% hold on
% axis equal
% borders contains the points on the perimeter of the grid

bordes = [squeeze(V1(:,:,1)),squeeze(V1(:,end,:)),fliplr(squeeze(V1(:,:,end))),fliplr(squeeze(V1(:,1,:)))]';

% Angles contains the unity vectors that have origin at 0,0,0
% and lie on z=0 plane with angles = angles

angles = [cosd(angles),sind(angles)];

% for every point on the perimeter

for i=1:size(bordes,1)
    % move the corresponding unity vector to the perimeter point
    % and extend it's length to 50
    
    ray = [bordes(i,:),bordes(i,:)+50*angles(i,:)];
    
    % Create a matrix that contains line segments of the face triangles from the 
    % vertice coordinates
    
    segments = [v0(:,1:2),v1(:,1:2);v0(:,1:2),v2(:,1:2);v1(:,1:2),v2(:,1:2)];
    out = lineSegmentIntersect(segments,ray);
    
    % Find the triangles sides that are intersected by the ray
    
    a = out.intAdjacencyMatrix==1;
    
    % Find the one with the maximum distance
    
    value = max(out.intNormalizedDistance2To1(a));
    
    % In the next two commands create a vector from the perimetry point
    % with length 0.99 * the above max value * 50
    initialP = bordes(i,:);
    finalP = bordes(i,:) + 0.99*value*50*angles(i,:);
%     patch('vertices',[[initialP,-5];[initialP,8];[finalP,8];[finalP,-5]],'faces',[1,2,3,4])

    % The points contains all the above vectors with all the intermediate
    % points determined in density by the radius parameter
    
    points = [points,divideSize(initialP,finalP,radius)];
end
points = points';

% We can see these points with scatter(points(:,1), points(:,2))

% plot(points(:,1),points(:,2),'.')
end