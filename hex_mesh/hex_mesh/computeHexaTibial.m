function [nodes, hex] = computeHexaTibial( interior,exterior,V,H,R )
%computeHexaTibial Computes nodes and hexahedrons of the hexahedral mesh
%   ratioV number of elements in the vertical direction
%   ratioH number of elements in the horizontal direction
%   radius number of elements in the radial direction
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Center of the model
% Center orthogonial nodes
% the first V*H elements
nodesCenter = [interior(1:V*H,:);exterior(1:V*H,:)];
% density of center defined by V*H
hexCenter = computeHexa( H, V*H );

% All the rest nodes except the central ones
% divided in up, left, down, right
nodesUp = [interior(V*H+1:(V+R)*H,:);exterior(V*H+1:(V+R)*H,:)];
% density of up defined by R*H
hexUp = computeHexa( R, R*H );

nodesLeft = [interior((V+R)*H+1:(V+R)*H+R*V,:);exterior((V+R)*H+1:(V+R)*H+R*V,:)];
% density of left defined by R*V
hexLeft = computeHexa( R, R*V );

nodesDown = [interior((V+R)*H+R*V+1:(V+2*R)*H+R*V,:);exterior((V+R)*H+R*V+1:(V+2*R)*H+R*V,:)];
% density of down defined by R*H
hexDown = computeHexa( R, R*H );

nodesRight = [interior((V+2*R)*H+R*V+1:(V+2*R)*H+2*R*V,:);exterior((V+2*R)*H+R*V+1:(V+2*R)*H+2*R*V,:)];
% density of right defined by R*V
hexRight = computeHexa( R, R*V );

% Concatenate all nodes to one matrix
nodes = [nodesCenter;nodesUp;nodesLeft;nodesDown;nodesRight];

% To overcome duplicate index values of hex from different areas e.g center
% left, up etc we start the numbering of the next hex from the previous hex
% Id's do not mean that new nodes for the hexa where created
% They are composed of those in the InternalP and ExternalP
hex = [hexCenter,hexUp+size(nodesCenter,1),hexLeft+size(nodesCenter,1)+size(nodesUp,1),...
    hexDown+size(nodesCenter,1)+size(nodesUp,1)+size(nodesLeft,1),...
    hexRight+size(nodesCenter,1)+size(nodesUp,1)+size(nodesLeft,1)+size(nodesDown,1)];

% if there are nodes with x1,y1,z2;x2,y2,z2;.. similar coordinates
% keep only one of them
[nodes, ia, ic] = unique(nodes,'rows');
% [C, ia, ic] = unique(A,'rows'); 
% C = A(ia,:) and A = C(ic,:)
% keep the corresponing index hexes
% and omit those referring to deleted nodes
% So if  index 342 is the same with 345
% then the all the hex elements above 342
% become elem-1 and 345 is replaced by 342
% so the value 344 becomes 343 , 600 becomes 599
hex = ic(hex)';

% Swaps element 2 with 4
% and 6 with 8
% possibly to fix the direction of the faces
% as counter clock wise

aux = hex(:,2);
hex(:,2) = hex(:,4);
hex(:,4) = aux;
aux = hex(:,6);
hex(:,6) = hex(:,8);
hex(:,8) = aux;
end

