function [femurMesh] = extractFemoral(pathFemur, pathPrepFile)

load(pathPrepFile, 'prep')
tic
disp('Extracting Femoral')
[ nodesF, hexF, layerF ] = FemurHexMesh( pathFemur, prep.PL, prep.PM, 2, 2 );
toc
[ sF, ~ ] = computeScaledJacobian( nodesF, hexF );
femurMesh = struct('nodes',nodesF,'hex',hexF,'layers',layerF,'quality',sF);
end

