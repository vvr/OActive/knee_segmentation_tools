function [ nodes, hex ] = MeniscusHexMesh( stlModel, orig,RadialRes, medial )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

clc
tic
if nargin<4
    medial = 0;
end

[ vMM, fMM ] = smoothModel( stlModel );
FV.vertices = vMM;
FV.faces = fMM;
Triangle.faces = [1,2,3];

    theta = 180+(0:360/RadialRes-1)*RadialRes;
    if medial
        theta = circshift(theta,size(theta,2)/2,2)';
    else
        theta = theta';
    end
%     theta = flipud(theta);
    angle = [cosd(theta), sind(theta),  zeros(size(theta,1),1)];
    
    t1 = repmat(orig,size(angle,1),1);
    t2 = t1 + 30*angle;
    t3 = t2 + repmat([0,0,60],size(angle,1),1);
    t2 = t2 + repmat([0,0,-60],size(angle,1),1);
    
    nodes = [];
    for ii=1:size(angle,1)
        Triangle.vertices = [t1(ii,:);t2(ii,:);t3(ii,:)];
%         temp = [t1(ii,:);t2(ii,:);t3(ii,:);t1(ii,:)];%
%         
%         plot3(FV.vertices(:,1),FV.vertices(:,2),FV.vertices(:,3),'.m')%
%         view([0 0 1])
%         axis([-60 10 -40 30 -100 40])
%         hold on%
%         drawnow%
        [intMatrix, intSurface] = SurfaceIntersection(Triangle, FV);
        points = intSurface.vertices;
        edges = intSurface.edges;
        
%         if ~isempty(points)
%             plot3(temp(:,1),temp(:,2),temp(:,3),'r');%
%             drawnow
%         else
%             plot3(temp(:,1),temp(:,2),temp(:,3),'g');%
%             drawnow
%         end
        
        if ~isempty(points)
            
            % find the closest point to the origin
            points2 = points;
            points2(:,3) = 3*points2(:,3);
            
            d = points - repmat(orig,size(points,1),1);
            d = sqrt(sum(d.^2,2));
            [~,ind] = min(d);
            A1 = points2(ind,:);
            P1 = points(ind,:);
            
            % find the furthest point from P1
            d1 = points2 - repmat(A1,size(points2,1),1);
            d1 = sqrt(sum(d1.^2,2));
            [~,ind] = max(d1);
            A2 = points2(ind,:);
            P2 = points(ind,:);
            
            % find the point which maximizes the area of the triangle
            % P1-P2-P3
            d2 = points2 - repmat(A2,size(points2,1),1);
            d2 = sqrt(sum(d2.^2,2));
            D = sqrt(sum((A2-A1).^2,2));
            s = 0.5*(d1+d2+D);
            A = sqrt(s.*(s-d1).*(s-d2).*(s-D));
            [~,ind] = max(A);
            A3 = points2(ind,:);
            P3 = points(ind,:);
            
            while abs(A3(3)-A2(3)) < min([1.5,0.6*(max(abs(points2(:,3)-A2(3))))])
                A(ind) = 0;
                [~,ind] = max(A);
                A3 = points2(ind,:);
            end
            
            P3 = points(ind,:);
            
            if P3(3) > P2(3)
               aux = P3;
               P3 = P2;
               P2 = aux;
            end

            p = points - repmat(orig,size(points,1),1);
            [theta,rho,z] = cart2pol(p(:,1),p(:,2),p(:,3));
            segments = [rho(edges(:,1)),z(edges(:,1)),rho(edges(:,2)),z(edges(:,2))];
            
            [theta1,rho1,z1] = cart2pol(P1(1)-orig(1),P1(2)-orig(2),P1(3)-orig(3));
            [theta2,rho2,z2] = cart2pol(P2(1)-orig(1),P2(2)-orig(2),P2(3)-orig(3));
            [theta3,rho3,z3] = cart2pol(P3(1)-orig(1),P3(2)-orig(2),P3(3)-orig(3));
            
            pSlice = nodesSlice([rho1,z1],[rho2,z2],[rho3,z3],segments);
            c1 = pSlice(:,1).*angle(ii,1)+orig(1);
            c2 = pSlice(:,1).*angle(ii,2)+orig(2);
 
            nodes = [nodes;[c1,c2,pSlice(:,2)+orig(3)]];
            
%             plot3(P1(1),P1(2),P1(3),'*b')
%             plot3(P2(1),P2(2),P2(3),'*y')
%             plot3(P3(1),P3(2),P3(3),'*c')
%             plot3(nodes(:,1),nodes(:,2),nodes(:,3),'*b');

        end
%         hold off%
%         pause(0.1)%
    end

    % 10 Hexes connected together
    H = [1,8,14,7;...
        8,2,16,14;...
        14,16,3,10;...
        4,7,14,10;...
        7,4,11,15;...
        15,13,1,7;...
        15,11,5,17;...
        15,17,6,13;...
        2,9,3,16;...
        17,5,12,6];
    H = [H,H+17];
    hex = [];
    for jj=1:(size(nodes,1)/17)-1
        hex = [hex;H+17*(jj-1)];
    end
 
    nodes = smoothHexMesh( nodes, hex, 'meniscus' );
    centers = repmat((7:17:size(nodes,1)),17,1);
    centers = nodes(centers(:),:);
    nodes = expandHexMesh_Meniscus( vMM,fMM,nodes,hex, centers,'line' );
    nodes = smoothHexMesh( nodes, hex, 'total' );
    nodes = optimizeHexMesh( nodes, hex ); 
    nodes = optimizeHexMesh( nodes, hex, 0.5 );
    
    [ nodes, hex, centers ] = refineHexMesh_Meniscus( nodes, hex, centers );
    
    nodes = smoothHexMesh( nodes, hex, 'meniscus' );
    nodes = expandHexMesh_Meniscus( vMM,fMM,nodes,hex, centers,'line' );
    nodes = smoothHexMesh( nodes, hex, 'total' );
    nodes = optimizeHexMesh( nodes, hex );
    nodes = optimizeHexMesh( nodes, hex, 0.5 );
    
    
    [ s, ~ ] = computeScaledJacobian( nodes, hex );
    doIt = ~isempty(find(s<0.5));
    it = 0;
    while doIt
        nodes2 = nodes;
        nodes2 = smoothHexMesh( nodes2, hex, 'meniscus' );
        nodes2 = expandHexMesh_Meniscus( vMM,fMM,nodes2,hex, centers,'ray' );
        nodes2 = smoothHexMesh( nodes2, hex, 'total' );
        nodes2 = optimizeHexMesh( nodes2, hex, 0.5 );
        
        V1 = hex(:,1:4);
        V2 = hex(:,5:8);
        ind = zeros(size(V1,1),1);
        for i=1:numel(ind)
            ind(i) = (~ismember(V2(i,:),V1,'rows')) | (~ismember(V1(i,:),V2,'rows'));
        end
        hex2 = hex;
        hex2(ind==1,:) = [];
        
        [ s, ~ ] = computeScaledJacobian( nodes2, hex2 );
        doIt = ~isempty(find(s<0.5));
        if isempty(find(s<0))
            nodes = nodes2;
        else
            doIt = 0;
        end
        it = it + 1;
        if it>20
            doIt = 0;
        end
    end
toc

V1 = hex(:,1:4);
V2 = hex(:,5:8);
ind = zeros(size(V1,1),1);
for i=1:numel(ind)
    ind(i) = (~ismember(V2(i,:),V1,'rows')) | (~ismember(V1(i,:),V2,'rows'));
end
hex(ind==1,:) = [];

end

