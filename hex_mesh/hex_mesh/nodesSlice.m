function points = nodesSlice(P1,P2,P3,segments)
% points = [P1;P2;P3;P12;C1;C;C2;P1_12;P2_12;P1_3;P2_3];
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

C = center(P1,P2,P3,'barycenter')';

ray1 = [C,P1];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if ~isempty(value) & value<0.9999
    P1 = C + value*(P1-C);
end

p1 = P3;

orig = C + 0.8*(P1-C);
angle = (P1-C)/norm(P1-C);
angle = [angle(2),-angle(1)];
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p3 = orig + value*100*angle;

angle = (P1-C)/norm(P1-C);
angle = [-angle(2),angle(1)];
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p2 = orig + value*100*angle;

orig = C;
angle = (C-P3)/norm(C-P3);
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p4 = orig + value*100*angle;

orig = C + 0.9*(P2-C);
angle = (P2-C)/norm(P2-C);
angle = [angle(2),-angle(1)];
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p6 = orig + value*100*angle;

angle = (P2-C)/norm(P2-C);
angle = [-angle(2),angle(1)];
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p5 = orig + value*100*angle;

p7 = (p1+p4)/2;

orig = p7;
angle = (0.5*(p1+p2)-p7)/norm(0.5*(p1+p2)-p7);
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p12 = orig + value*100*angle;

orig = p7;
angle = (0.5*(p2+p3)-p7)/norm(0.5*(p2+p3)-p7);
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p23 = orig + value*100*angle;

orig = p7;
angle = (0.5*(p3+p4)-p7)/norm(0.5*(p3+p4)-p7);
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p34 = orig + value*100*angle;

orig = p7;
angle = (0.5*(p4+p5)-p7)/norm(0.5*(p4+p5)-p7);
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p45 = orig + value*100*angle;

orig = p7;
angle = (0.5*(p5+p6)-p7)/norm(0.5*(p5+p6)-p7);
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p56 = orig + value*100*angle;

orig = p7;
angle = (0.5*(p6+p1)-p7)/norm(0.5*(p6+p1)-p7);
ray1 = [orig,orig+100*angle];
out = lineSegmentIntersect(segments,ray1);
a = out.intAdjacencyMatrix==1;
value = min(out.intNormalizedDistance2To1(a));
if isempty(value)
    value = 0;
end
p61 = orig + value*100*angle;

aux1 = (p7 + p23)/2;
aux2 = (p7 + p56)/2;

aux11 = (aux1 + p23)/2;
aux22 = (aux2 + p56)/2;
% 
% plot(p1(1),p1(2),'*r')
% hold on
% plot(p2(1),p2(2),'*r')
% plot(p3(1),p3(2),'*r')
% plot(p4(1),p4(2),'*r')
% plot(p5(1),p5(2),'*r')
% plot(p6(1),p6(2),'*r')
% plot(p7(1),p7(2),'+r')
% plot(p12(1),p12(2),'or')
% plot(p23(1),p23(2),'or')
% plot(p34(1),p34(2),'or')
% plot(p45(1),p45(2),'or')
% plot(p56(1),p56(2),'or')
% plot(p61(1),p61(2),'or')
% close

points = [p1;p2;p3;p4;p5;p6;p7;p12;p23;p34;p45;p56;p61;aux1;aux2;aux11;aux22];
end

