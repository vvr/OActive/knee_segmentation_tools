function nodes = optimizeHexMesh( nodes, hex, value )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

if nargin<3
    value = 0;
end
%optimizeHexMesh finds the nodes with scaledJacobian < 0 and substitutes
%them by the average of their neigbours
%   Detailed explanation goes here
doIt = 1;
%% Compute the neigbours of each vertex
vecinos = [hex(:,1:2);hex(:,2:3);hex(:,3:4);hex(:,4),hex(:,1); ...
    hex(:,5:6);hex(:,6:7);hex(:,7:8);hex(:,8),hex(:,5);...
    hex(:,2),hex(:,6);hex(:,6),hex(:,7);hex(:,7),hex(:,3);hex(:,3),hex(:,2);...
    hex(:,1),hex(:,5);hex(:,5),hex(:,8);hex(:,8),hex(:,4);hex(:,4),hex(:,1);...
    hex(:,1),hex(:,5);hex(:,5),hex(:,6);hex(:,6),hex(:,2);hex(:,2),hex(:,1);...
    hex(:,4),hex(:,8);hex(:,8),hex(:,7);hex(:,7),hex(:,3);hex(:,3),hex(:,4)];
vecinos = [vecinos;fliplr(vecinos)];
vecinos = unique(vecinos,'rows');
while(doIt)
    %% Compute scaled Jacobian
    [ s, J ] = computeScaledJacobian( nodes, hex );
    %% find the nodes with scaled Jacobian < 0
    [index, ia,ic] = unique(hex(J<value));
    if isempty(index)
        doIt = 0;
    else
        %% for each index, substitute the node by the average of its neighbours
        disp = nodes.*0;
        if value == 0
            for ii = 1:size(index,1)
                vec = vecinos(vecinos(:,1)==index(ii),2);
                disp(index(ii),:) =  disp(index(ii),:) + ...
                    sum(nodes(vec,:))/size(vec,1)-nodes(index(ii),:);
            end
        else
            ind = find(s<value);
            for ii = 1:size(ind,1)
                disp = disp + fixAngle(nodes,hex(ind(ii),:));
            end
            if norm(sum(disp,1))==0
                doIt = 0;
            end
        end
%         disp = smoothHexMesh( disp, hex, 'total' );
        nodes2 = nodes + disp;
        [ s, J ] = computeScaledJacobian( nodes2, hex );
        if isempty(find(s<0))
            nodes = nodes2;
        else
            doIt = 0;
        end
    end
end
end

