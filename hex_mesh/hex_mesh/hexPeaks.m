function [ hex, nodes ] = hexPeaks( hex, nodes, indPeaks )
%hexPeaks 
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%       4 ---------- 3 ---------- 10
%      /|           /|           /|
%     / |          / |          / |
%    1 -|-------- 2 -|-------- 9  |
%    |  8 --------|- 7 --------|- 12
%    | /          | /          | /
%    |/           |/           |/
%    5 ---------- 6 ---------- 11

% Should work for nodes 1-4-9-10 NaNs but this is not the case.
% What does it mean to have a peak?
% It works for 4 - 8 - 10 - 12 NaNs. Has to do with the orientation.

for ii = 1:size(indPeaks,1)
    if sum(isnan(nodes(hex(:,indPeaks(ii)),1)))<2
        continue;
    end
    switch find(isnan(nodes(hex(:,indPeaks(ii)),1)), 1,'first')
        case 1
            nodes(hex(1,indPeaks(ii)),:) = nodes(hex(2,indPeaks(ii)),:)+...
                nodes(hex(4,indPeaks(ii)),:)-nodes(hex(3,indPeaks(ii)),:);
            nodes(hex(5,indPeaks(ii)),:) = nodes(hex(6,indPeaks(ii)),:)+...
                nodes(hex(8,indPeaks(ii)),:)-nodes(hex(7,indPeaks(ii)),:);
        case 2
            nodes(hex(2,indPeaks(ii)),:) = nodes(hex(1,indPeaks(ii)),:)+...
                nodes(hex(3,indPeaks(ii)),:)-nodes(hex(4,indPeaks(ii)),:);
            nodes(hex(6,indPeaks(ii)),:) = nodes(hex(5,indPeaks(ii)),:)+...
                nodes(hex(7,indPeaks(ii)),:)-nodes(hex(8,indPeaks(ii)),:);
        case 3
            nodes(hex(3,indPeaks(ii)),:) = nodes(hex(4,indPeaks(ii)),:)+...
                nodes(hex(2,indPeaks(ii)),:)-nodes(hex(1,indPeaks(ii)),:);
            nodes(hex(7,indPeaks(ii)),:) = nodes(hex(8,indPeaks(ii)),:)+...
                nodes(hex(6,indPeaks(ii)),:)-nodes(hex(5,indPeaks(ii)),:);
        case 4
            nodes(hex(4,indPeaks(ii)),:) = nodes(hex(3,indPeaks(ii)),:)+...
                nodes(hex(1,indPeaks(ii)),:)-nodes(hex(2,indPeaks(ii)),:);
            nodes(hex(8,indPeaks(ii)),:) = nodes(hex(7,indPeaks(ii)),:)+...
                nodes(hex(5,indPeaks(ii)),:)-nodes(hex(6,indPeaks(ii)),:);
    end
end

end

