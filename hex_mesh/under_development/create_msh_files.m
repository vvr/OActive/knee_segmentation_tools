%% Femoral Cartilage MSH file
clear all; clc
addpath('msh/');

matfile = dir('../data/geometries/mat_files/femoral_cartilage_m.mat');
load(fullfile(matfile.folder, matfile.name));

export_path = dir('../data/geometries/output_hex_mesh/');
export_file_name = fullfile(export_path(1).folder,'femoral_cartilage.msh');

% find x width and y width so using percentages of the cartilage structure
% I can divide the cartilage to medial and lateral contant areas

x_width = max(femoral_cartilage_m.nodes(:,1)) - min(femoral_cartilage_m.nodes(:,1));
minX = min(femoral_cartilage_m.nodes(:,1));
y_width = max(femoral_cartilage_m.nodes(:,2)) - min(femoral_cartilage_m.nodes(:,2));
minY = min(femoral_cartilage_m.nodes(:,2));

% ============================================
% START Header

% These values are needed for the header of the msh file
% in order to determine the physical elements groups.

% The following values are chosen by us in order to distinguish between
% the physical groups. The values 1 2 3 are reserved for the 3 layers.

val_bone_connection = 4;
val_lateral_contact = 5;
val_medial_contact = 6;

% Nodes must be of type 0

physical_node = 0;
% Surfaces must be of type 2

physical_surface = 2;

% Volumes must be of type 3

physical_volume = 3;

% We pass these values to a structure so our function writemsh
% requires less parameters.

physical.names = {'bone_connection_nodes', 'lateral_contact_surface',...
    'medial_contact_surface', 'bone_layer', 'inner_layer', 'outer_layer'};
physical.type = [physical_node physical_surface physical_surface ...
    physical_volume physical_volume physical_volume];
physical.values = [val_bone_connection val_lateral_contact ...
    val_medial_contact 1 2 3];

% END header
%===============================================

[m,~] = size(femoral_cartilage_m.hex);

% The following lines determine the poins of the inner surface of the 
% femorla cartilage, those in contact with the femur bone.
%
% Node_1:   1 -  1 -  1 -  1 - 1
% Layer_1:  |    |    |    |   |
% Node_12: 1&2 -1&2 -1&2 -1&2 -1&2
% Layer_2:  |   |     |     |   |
% Node_23: 2&3 -2&3 -2&3 - 2&3 -2&3
% 
% The elements of layer 1 contain the inner nodes that are in contact
% with the bone (1) and nodes from inner surface of the layer 2. So in
% order to take only the nodes of inner surace I apply the following
% set operation: Node_1 = Layer1(Node_1,Node_2) -
% Layer2(Node_1,Node_2,Node_3)
% 

elemLayer1 = [];
elemLayer2 = [];
for i = 1:m
    if femoral_cartilage_m.layers(i) == 1
        elemLayer1 = [elemLayer1; femoral_cartilage_m.hex(i,:)];
    end
    if femoral_cartilage_m.layers(i) == 2
        elemLayer2 = [elemLayer2; femoral_cartilage_m.hex(i,:)];
    end
end

nodes1 = setdiff(elemLayer1, elemLayer2);
[s,~] = size(nodes1);
connection_nodes.ids = nodes1;
connection_nodes.physical_values = val_bone_connection * ones(s,1);


% Up to this point connection_nodes contains the inner surcafe nodes that are
% in contact with the bone


% Now we want to find the lateral and medial nodes of the two femur
% condyles.
% The femoral cartilage is oriented such the lateral - medial direction
% lies on the x axis and the normal direction of the condyle surfaces
% lie on the z axis.
% We assume that the two condyles are divided on the 50% of the x direction
% that the cartilage occupies. Also, because a part of the cartilage is not
% in contact with the tibial condyles but is in contact with the patellar
% cartilage we omit the region above 85% in the y direction.
%

elemLayer5 = [];
elemLayer6 = [];

for i = 1:m
    
    coor = [];
    layer = femoral_cartilage_m.layers(i);
    
    if layer == 6
        for j = 1:8
            h = femoral_cartilage_m.hex(i,j);
            coor = [coor;femoral_cartilage_m.nodes(h,:)];
        end
        
        count1 = 0;
        count2 = 0;
        
        for j=1:8
            
            if (coor(j,2) < (minY + y_width * 0.85) && coor(j,1) > (minX + x_width * 0.5))
                count1 = count1 + 1;
            elseif (coor(j,2) < (minY + y_width * 0.85) && coor(j,1) < (minX + x_width * 0.5))
                count2 = count2 + 1;
            end
            
        end
        
        if count1 == 8
            elemLayer5 = [elemLayer5;femoral_cartilage_m.hex(i,:)];
        elseif count2 == 8
            elemLayer6 = [elemLayer6;femoral_cartilage_m.hex(i,:)];
        end
        
    end
    
end

[s,~] = size(elemLayer5);

% From observation the outer face of an hexadreon are the
% the IDs of the last 4 nodes, 5-8.

surfaces1.ids = elemLayer5(:,5:8);
surfaces1.physical_values = val_lateral_contact * ones(s,1);
[s,~] = size(elemLayer6);
surfaces2.ids = elemLayer6(:,5:8);
surfaces2.physical_values = val_medial_contact * ones(s,1);

% We concatenate the to surfaces into one structure so we can
% pass one argument to the writemsh function

surfaces.ids = [surfaces1.ids;surfaces2.ids];
surfaces.physical_values = [surfaces1.physical_values;surfaces2.physical_values];


% Up to this point the Surfaces contains the two surfaces that are in 
% contact with the lateral - medial menisci.

% Now we group the 6 layers of the femoral cartilage volumetric mesh
% in 3 groups

hex = layerconcatenation(femoral_cartilage_m);

% We use the function writemsh to create the msh file

writemsh(export_file_name, physical, femoral_cartilage_m.nodes, surfaces, connection_nodes, hex);
%% Tibial Lateral Cartilage MSH file
clear all; clc
addpath('msh/');

matfile = dir('../data/geometries/mat_files/tibial_cartilage_lateral_m.mat');
load(fullfile(matfile.folder, matfile.name));

export_path = dir('../data/geometries/output_hex_mesh/');
export_file_name = fullfile(export_path(1).folder,'lateral_tibial_cartilage.msh');

% START Head
val_bone_connection = 4;
val_contact = 5;
physical_node = 0;
physical_surface = 2;
physical_volume = 3;

physical.names = {'bone_connection_nodes', 'contact_surface',...
    'bone_layer', 'inner_layer', 'outer_layer'};
physical.type = [physical_node physical_surface ...
    physical_volume physical_volume physical_volume];
physical.values = [val_bone_connection val_contact 1 2 3];
% END Header

[m,~] = size(tibial_cartilage_lateral_m.hex);

elemLayer1 = [];
elemLayer2 = [];
elemLayer6 = [];

% As before we need to find the connection points of the inferior
% surfaces, the ones that are in contact with the tibia. These points
% are on the external surface of the Layer 6 so we have to perform
% the Set operation 'difference' between the Layers 6 and 5.
% The Layer 6 is important in order to find the contact surface with the 
% meniscus.

for i = 1:m
    if tibial_cartilage_lateral_m.layers(i) == 1
        elemLayer1 = [elemLayer1; tibial_cartilage_lateral_m.hex(i,:)];
    end
    if tibial_cartilage_lateral_m.layers(i) == 2
        elemLayer2 = [elemLayer2; tibial_cartilage_lateral_m.hex(i,:)];
    end    
    if tibial_cartilage_lateral_m.layers(i) == 6
        elemLayer6 = [elemLayer6; tibial_cartilage_lateral_m.hex(i,:)];
    end
end

[s,~] = size(elemLayer6);

% Here we need to find the surface upon which the meniscus lies.
% So we take the elements of the 6th layer and keep the face that
% is composed by the first 4 node ids. Since the orientation is the
% opposite of that we want we flip the order of the nodes.

surfaces.ids = fliplr(elemLayer6(:,1:4));
surfaces.physical_values = val_contact * ones(s,1);

nodes1 = setdiff(elemLayer1, elemLayer2);
[s,~] = size(nodes1);
connection_nodes.ids = nodes1;
connection_nodes.physical_values = val_bone_connection * ones(s,1);

% Concatenation to 3 Layers

hex = layerconcatenation(tibial_cartilage_lateral_m);

writemsh(export_file_name, physical, tibial_cartilage_lateral_m.nodes, surfaces, connection_nodes, hex);
%% Tibial Medial Cartilage MSH file
clear all; clc
addpath('msh/');

matfile = dir('../data/geometries/mat_files/tibial_cartilage_medial_m.mat');
load(fullfile(matfile.folder, matfile.name));

export_path = dir('../data/geometries/output_hex_mesh/');
export_file_name = fullfile(export_path(1).folder,'medial_tibial_cartilage.msh');

% START Header
val_bone_connection = 4;
val_contact = 5;
physical_node = 0;
physical_surface = 2;
physical_volume = 3;

physical.names = {'bone_connection_nodes', 'contact_surface',...
    'bone_layer', 'inner_layer', 'outer_layer'};
physical.type = [physical_node physical_surface ...
    physical_volume physical_volume physical_volume];
physical.values = [val_bone_connection val_contact 1 2 3];
% END Header

[m,~] = size(tibial_cartilage_medial_m.hex);

elemLayer1 = [];
elemLayer2 = [];
elemLayer6 = [];

for i = 1:m
    if tibial_cartilage_medial_m.layers(i) == 1
        elemLayer1 = [elemLayer1; tibial_cartilage_medial_m.hex(i,:)];
    end
    if tibial_cartilage_medial_m.layers(i) == 2
        elemLayer2 = [elemLayer2; tibial_cartilage_medial_m.hex(i,:)];
    end
    if tibial_cartilage_medial_m.layers(i) == 6
        elemLayer6 = [elemLayer6; tibial_cartilage_medial_m.hex(i,:)];
    end
end

[s,~] = size(elemLayer6);
surfaces.ids = fliplr(elemLayer6(:,1:4));
surfaces.physical_values = val_contact * ones(s,1);

nodes1 = setdiff(elemLayer1, elemLayer2);
[s,~] = size(nodes1);
connection_nodes.ids = nodes1;
connection_nodes.physical_values = val_bone_connection * ones(s,1);

hex = layerconcatenation(tibial_cartilage_medial_m);

writemsh(export_file_name, physical, tibial_cartilage_medial_m.nodes, surfaces, connection_nodes, hex);
%% Lateral Meniscus MSH file
clear all; clc
addpath('msh/');

matfile = dir('../data/geometries/mat_files/meniscus_lateral_m.mat');
load(fullfile(matfile.folder, matfile.name));

export_path = dir('../data/geometries/output_hex_mesh/');
export_file_name = fullfile(export_path(1).folder,'lateral_meniscus.msh');

% START Header
anterior = 2;
posterior = 3;
superior = 4;
inferior = 5;
physical_node = 0;
physical_surface = 2;
physical_volume = 3;

physical.names = {'anterior_nodes', 'posterior_nodes', 'superior_contact_surface','inferior_contact_surface','volume'};
physical.type = [physical_node physical_node ...
    physical_surface physical_surface physical_volume];
physical.values = [anterior posterior superior inferior 1];
% END Header

% Nodes of the external surface of the volumetric mesh

exNodes = meniscus_external_nodes(meniscus_lateral_m.hex);

% Retrieve the outer faces of all elements that lie one the exteral surface
% as well as the nodes where the springs are connected

[out_surf,spring_nodes] = surfaceQuadsSprings(meniscus_lateral_m.hex, exNodes, anterior, posterior);

hex.ids = meniscus_lateral_m.hex;
[s,~] = size(hex.ids);
hex.layer = ones(s,1);

contact_surfaces = sup_inf_surfaces2(out_surf, meniscus_lateral_m.nodes, superior, inferior, spring_nodes.ids);

writemsh(export_file_name, physical, meniscus_lateral_m.nodes, contact_surfaces, spring_nodes, hex);
%% Medial Meniscus MSH file
clear all; clc
addpath('msh/');

matfile = dir('../data/geometries/mat_files/meniscus_medial_m.mat');
load(fullfile(matfile.folder, matfile.name));

export_path = dir('../data/geometries/output_hex_mesh/');
export_file_name = fullfile(export_path(1).folder,'medial_meniscus.msh');

% START Header
anterior = 2;
posterior = 3;
superior = 4;
inferior = 5;
physical_node = 0;
physical_surface = 2;
physical_volume = 3;

physical.names = {'anterior_nodes','posterior_nodes','superior_contact_surface','inferior_contact_surface','volume'};
physical.type = [physical_node physical_node ...
    physical_surface physical_surface physical_volume];
physical.values = [anterior posterior superior inferior 1];
% END Header

exNodes = meniscus_external_nodes(meniscus_medial_m.hex);

% Surface Patches
% Here the variables anterior and posterior are in the reversed order
% in comparison to the lateral meniscus.

[out_surf,spring_nodes] = surfaceQuadsSprings(meniscus_medial_m.hex, exNodes, posterior, anterior);

hex.ids = meniscus_medial_m.hex;
[s,~] = size(hex.ids);
hex.layer = ones(s,1);

contact_surfaces = sup_inf_surfaces2(out_surf, meniscus_medial_m.nodes, superior, inferior, spring_nodes.ids);

writemsh(export_file_name, physical, meniscus_medial_m.nodes, contact_surfaces, spring_nodes, hex);
%% Femur Bone Mesh
clear all; clc
addpath('msh/');

objfile = dir('../data/geometries/input_hex_mesh/femur.obj');
[v_f, f_f] = objread(fullfile(objfile.folder, objfile.name));

export_path = dir('../data/geometries/output_hex_mesh/');
export_file_name = fullfile(export_path(1).folder,'femur.msh');

physical.names = {'surface'};
physical.type = 2;
physical.values = 1;

faces.ids = f_f;
writemsh(export_file_name,physical,v_f,[],[],faces);
%% Tibia Bone Mesh
clear all; clc
addpath('msh/');

objfile = dir('../data/geometries/input_hex_mesh/tibia.obj');
[v_t, f_t] = objread(fullfile(objfile.folder, objfile.name));

export_path = dir('../data/geometries/output_hex_mesh/');
export_file_name = fullfile(export_path(1).folder,'tibia.msh');

physical.names = {'anterior_lateral_node','posterior_lateral_node',...
    'anterior_medial_node','posterior_medial_node','surface'};
physical.type = [0 0 0 0 2];
physical.values = [2 3 4 5 1];

[al, pl, am, pm] = mesisci_spring_points(fullfile(export_path(1).folder,'lateral_meniscus.msh'),...
    fullfile(export_path(1).folder,'medial_meniscus.msh'),v_t);
spring_nodes.ids = [al;pl;am;pm;];
spring_nodes.physical_values = [2;3;4;5];
faces.ids = f_t;
writemsh(export_file_name,physical,v_t,[],spring_nodes,faces);