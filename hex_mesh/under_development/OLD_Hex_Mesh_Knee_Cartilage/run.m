% This software accepts only binrary STL files
addpath('Software_V1.0/');
addpath('9932809_00mo/'); % Binary
%addpath('STLnew/'); % ASCI
%addpath('Input_Binary_Mesh/');

% demo data
 femoral_cartilage = 'femoral_9932809_00mo.stl';
 tibial_cartilage_lateral = 'lateral_tibial_9932809_00mo.stl';
 tibial_cartilage_medial = 'medial_tibial_9932809_00mo.stl';
 meniscus_lateral = 'lateral_meniscus_9932809_00mo.stl';
 meniscus_medial = 'medial_meniscus_9932809_00mo.stl';

%our data (must correct stlread)
%femoral_cartilage = 'Cartilage_femoral_NoOverlap.stl';
%tibial_cartilage_lateral = 'Cartilage_lateral_tibial_NoOverlap.stl';
%tibial_cartilage_medial = 'Cartilage_medial_tibial_NoOverlap.stl';
%meniscus_lateral = 'Menisci_lateral_meniscus_NoOverlap.stl';
%meniscus_medial = 'Menisci_medial_meniscus_NoOverlap.stl';


[femoral_cartilage_m, tibial_cartilage_lateral_m, tibial_cartilage_medial_m, ...
    meniscus_lateral_m, meniscus_medial_m ] = main(femoral_cartilage, ...
    tibial_cartilage_lateral, tibial_cartilage_medial, meniscus_lateral, ...
    meniscus_medial);


visualize(tibial_cartilage_lateral_m);
%writeObj(tibial_cartilage_medial_m,'Output_Mesh\CM_Hex.obj')
%writeObj(tibial_cartilage_lateral_m,'Output_Mesh\CL_Hex.obj')
%writeObj(femoral_cartilage_m,'Output_Mesh\CF_Hex.obj')
%writeObj(meniscus_medial_m,'Output_Mesh\MM_Hex.obj')
%writeObj(meniscus_lateral_m,'Output_Mesh\ML_Hex.obj')