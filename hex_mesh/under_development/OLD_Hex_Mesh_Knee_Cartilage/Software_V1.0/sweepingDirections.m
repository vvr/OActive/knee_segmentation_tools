function [ newRays ] = sweepingDirections( v, f, triangles, dir, orig )
%Average directions between the normal of the triangle and the previous
%direction
%   v: vertices of the STL triangular mesh
%   f: faces of the STL triangular mesh
%   triangles is an index of the triangle intersected by the ray
%   dir is the direction of the ray.
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

    %Vertices of the triangles
    v0 = v(f(:,1),:);
    v1 = v(f(:,2),:);
    v2 = v(f(:,3),:);
     
    %   Compute the normal vector of each triangle
    normals = cross(v2-v0, v1-v0);
    normals = normals./repmat(sqrt(sum(normals.^2,2)),1,3);
    
    % Intialize vector1 with the initial directions
    vector1 = dir;
    % where there is an intersection, vector1 is the normal to the
    % intersected triangle
    vector1(~isnan(triangles),:) = normals(triangles(~isnan(triangles)),:);
    vector2 = vector1;
    
    % for each direction
    for i=1:size(vector1,1)
        % we find the neighbors 
        distancia = sqrt(sum((orig-repmat(orig(i,:),[size(orig,1),1])).^2,2));
        indices = find(distancia<5);
        % if there is neighbors, we promediate their values
        if ~isempty(indices)
            vector2(i,:) = sum(vector1(indices,:),1);
            % and make the vector unitary
            vector2(i,:) = vector2(i,:)./sqrt(sum(vector2(i,:).^2,2));
        end
    end
            
    newRays = vector2;
    newRays(isnan(triangles),:) = NaN;
end

