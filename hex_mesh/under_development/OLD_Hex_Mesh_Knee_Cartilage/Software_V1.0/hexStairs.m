function [hex, nodes] = hexStairs( hex, nodes, indStairs )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

Remove = zeros(1,size(hex,2));

% for each stair
for ii=1:size(indStairs,1)
    switch find(isnan(nodes(hex(:,indStairs(ii,1)),1)), 1,'first')
        case 1
            p = find(ismember(hex(:,indStairs(ii,1)),hex(:,indStairs(ii,2))),1,'first');
            if p==4
                indStairs(ii,:) = fliplr(indStairs(ii,:));
            end
            % Creates auxiliar nodes and adds them to the list
            aux1 = nodes(hex(2,indStairs(ii,1)),:)+...
                nodes(hex(4,indStairs(ii,1)),:)-nodes(hex(3,indStairs(ii,1)),:);
            aux2 = nodes(hex(2,indStairs(ii,2)),:)+...
                nodes(hex(4,indStairs(ii,2)),:)-nodes(hex(3,indStairs(ii,2)),:);
            aux3 = nodes(hex(8,indStairs(ii,1)),:)+...
                aux1-nodes(hex(4,indStairs(ii,1)),:);
            aux4 = nodes(hex(8,indStairs(ii,2)),:)+...
                aux2-nodes(hex(4,indStairs(ii,2)),:);
            nodes = [nodes;aux1;aux2;aux3;aux4];
            % Remove the hexahedron in the middle of the stairs
            a = ismember(hex,hex(:,indStairs(ii,1)));
            indRemove = find(a(1,:)&a(4,:)&a(5,:)&a(8,:));
            indRemove(ismember(indRemove,indStairs(ii,1))) = [];
            Remove(indRemove) = 1;
            % Modifies the indices of the neigbours hexahedra 
            siz = size(nodes,1);
            a = ismember(hex,hex(:,indStairs(ii,1)));
            ind1 = find(a(1,:)&a(2,:)&a(5,:)&a(6,:));
            ind1(ismember(ind1,indStairs(ii,1))) = [];
            nodes(hex(1,ind1),:) = nodes(siz-3,:);
            nodes(hex(5,ind1),:) = nodes(siz-1,:);
            a = ismember(hex,hex(:,indStairs(ii,2)));
            ind = find(a(1,:)&a(4,:)&a(5,:)&a(8,:));
            ind(ismember(ind,indStairs(ii,2))) = [];
            nodes(hex(1,ind),:) = nodes(siz-2,:);
            nodes(hex(5,ind),:) = nodes(siz,:);
            % Create 2 new hexahedrons
            hex1 = [siz-3;hex(2,indStairs(ii,1));hex(3,indRemove);hex(3,indStairs(ii,1));...
                siz-1;hex(6,indStairs(ii,1));hex(7,indRemove);hex(7,indStairs(ii,1))];
            hex2 = [siz-2;hex(3,indStairs(ii,2));hex(3,indRemove);hex(4,indStairs(ii,2));...
                siz;hex(7,indStairs(ii,2));hex(7,indRemove);hex(8,indStairs(ii,2))];
            hex = [hex,hex1,hex2];   
        case 2
            p = find(ismember(hex(:,indStairs(ii,1)),hex(:,indStairs(ii,2))),1,'first');
            if p==1
                indStairs(ii,:) = fliplr(indStairs(ii,:));
            end
            % Creates auxiliar nodes and adds them to the list
            aux1 = nodes(hex(1,indStairs(ii,1)),:)+...
                nodes(hex(3,indStairs(ii,1)),:)-nodes(hex(4,indStairs(ii,1)),:);
            aux2 = nodes(hex(1,indStairs(ii,2)),:)+...
                nodes(hex(3,indStairs(ii,2)),:)-nodes(hex(4,indStairs(ii,2)),:);
            aux3 = nodes(hex(7,indStairs(ii,1)),:)+...
                aux1-nodes(hex(3,indStairs(ii,1)),:);
            aux4 = nodes(hex(7,indStairs(ii,2)),:)+...
                aux2-nodes(hex(3,indStairs(ii,2)),:);
            nodes = [nodes;aux1;aux2;aux3;aux4];
            % Remove the hexahedron in the middle of the stairs
            a = ismember(hex,hex(:,indStairs(ii,1)));
            indRemove = find(a(1,:)&a(2,:)&a(5,:)&a(6,:));
            indRemove(ismember(indRemove,indStairs(ii,1))) = [];
            Remove(indRemove) = 1;
            % Modifies the indices of the neigbours hexahedra 
            siz = size(nodes,1);
            a = ismember(hex,hex(:,indStairs(ii,1)));
            ind1 = find(a(2,:)&a(3,:)&a(6,:)&a(7,:));
            ind1(ismember(ind1,indStairs(ii,1))) = [];
            nodes(hex(2,ind1),:) = nodes(siz-3,:);
            nodes(hex(6,ind1),:) = nodes(siz-1,:);
            a = ismember(hex,hex(:,indStairs(ii,2)));
            ind = find(a(1,:)&a(2,:)&a(5,:)&a(6,:));
            ind(ismember(ind,indStairs(ii,2))) = [];
            nodes(hex(2,ind),:) = nodes(siz-2,:);
            nodes(hex(6,ind),:) = nodes(siz,:);
            % Create 2 new hexahedrons
            hex1 = [siz-3;hex(3,indStairs(ii,1));hex(4,indRemove);hex(4,indStairs(ii,1));...
                siz-1;hex(7,indStairs(ii,1));hex(8,indRemove);hex(8,indStairs(ii,1))];
            hex2 = [siz-2;hex(4,indStairs(ii,2));hex(4,indRemove);hex(1,indStairs(ii,2));...
                siz;hex(8,indStairs(ii,2));hex(8,indRemove);hex(5,indStairs(ii,2))];
            hex = [hex,hex1,hex2];  
        case 3
            p = find(ismember(hex(:,indStairs(ii,1)),hex(:,indStairs(ii,2))),1,'first');
            if p==4
                indStairs(ii,:) = fliplr(indStairs(ii,:));
            end
            % Creates auxiliar nodes and adds them to the list
            aux1 = nodes(hex(4,indStairs(ii,1)),:)+...
                nodes(hex(2,indStairs(ii,1)),:)-nodes(hex(1,indStairs(ii,1)),:);
            aux2 = nodes(hex(4,indStairs(ii,2)),:)+...
                nodes(hex(2,indStairs(ii,2)),:)-nodes(hex(1,indStairs(ii,2)),:);
            aux3 = nodes(hex(6,indStairs(ii,1)),:)+...
                aux1-nodes(hex(2,indStairs(ii,1)),:);
            aux4 = nodes(hex(6,indStairs(ii,2)),:)+...
                aux2-nodes(hex(2,indStairs(ii,2)),:);
            nodes = [nodes;aux1;aux2;aux3;aux4];
            % Remove the hexahedron in the middle of the stairs
            a = ismember(hex,hex(:,indStairs(ii,1)));
            indRemove = find(a(3,:)&a(4,:)&a(7,:)&a(8,:));
            indRemove(ismember(indRemove,indStairs(ii,1))) = [];
            Remove(indRemove) = 1;
            % Modifies the indices of the neigbours hexahedra 
            siz = size(nodes,1);
            a = ismember(hex,hex(:,indStairs(ii,1)));
            ind1 = find(a(2,:)&a(3,:)&a(6,:)&a(7,:));
            ind1(ismember(ind1,indStairs(ii,1))) = [];
            nodes(hex(3,ind1),:) = nodes(siz-3,:);
            nodes(hex(7,ind1),:) = nodes(siz-1,:);
            a = ismember(hex,hex(:,indStairs(ii,2)));
            ind = find(a(3,:)&a(4,:)&a(7,:)&a(8,:));
            ind(ismember(ind,indStairs(ii,2))) = [];
            nodes(hex(3,ind),:) = nodes(siz-2,:);
            nodes(hex(7,ind),:) = nodes(siz,:);
            % Create 2 new hexahedrons
            hex1 = [siz-3;hex(1,indStairs(ii,1));hex(1,indRemove);hex(2,indStairs(ii,1));...
                siz-1;hex(5,indStairs(ii,1));hex(5,indRemove);hex(6,indStairs(ii,1))];
            hex2 = [siz-2;hex(4,indStairs(ii,2));hex(1,indRemove);hex(1,indStairs(ii,2));...
                siz;hex(8,indStairs(ii,2));hex(5,indRemove);hex(5,indStairs(ii,2))];
            hex = [hex,hex1,hex2];
        case 4
            p = find(ismember(hex(:,indStairs(ii,1)),hex(:,indStairs(ii,2))),1,'first');
            if p==1
                indStairs(ii,:) = fliplr(indStairs(ii,:));
            end
            % Creates auxiliar nodes and adds them to the list
            aux1 = nodes(hex(3,indStairs(ii,1)),:)+...
                nodes(hex(1,indStairs(ii,1)),:)-nodes(hex(2,indStairs(ii,1)),:);
            aux2 = nodes(hex(3,indStairs(ii,2)),:)+...
                nodes(hex(1,indStairs(ii,2)),:)-nodes(hex(2,indStairs(ii,2)),:);
            aux3 = nodes(hex(5,indStairs(ii,1)),:)+...
                aux1-nodes(hex(1,indStairs(ii,1)),:);
            aux4 = nodes(hex(5,indStairs(ii,2)),:)+...
                aux2-nodes(hex(1,indStairs(ii,2)),:);
            nodes = [nodes;aux1;aux2;aux3;aux4];
            % Remove the hexahedron in the middle of the stairs
            a = ismember(hex,hex(:,indStairs(ii,1)));
            indRemove = find(a(1,:)&a(4,:)&a(5,:)&a(8,:));
            indRemove(ismember(indRemove,indStairs(ii,1))) = [];
            Remove(indRemove) = 1;
            % Modifies the indices of the neigbours hexahedra 
            siz = size(nodes,1);
            a = ismember(hex,hex(:,indStairs(ii,1)));
            ind1 = find(a(3,:)&a(4,:)&a(7,:)&a(8,:));
            ind1(ismember(ind1,indStairs(ii,1))) = [];
            nodes(hex(4,ind1),:) = nodes(siz-3,:);
            nodes(hex(8,ind1),:) = nodes(siz-1,:);
            a = ismember(hex,hex(:,indStairs(ii,2)));
            ind = find(a(1,:)&a(4,:)&a(5,:)&a(8,:));
            ind(ismember(ind,indStairs(ii,2))) = [];
            nodes(hex(4,ind),:) = nodes(siz-2,:);
            nodes(hex(8,ind),:) = nodes(siz,:);
            % Create 2 new hexahedrons
            hex1 = [siz-3;hex(2,indStairs(ii,1));hex(2,indRemove);hex(3,indStairs(ii,1));...
                siz-1;hex(6,indStairs(ii,1));hex(6,indRemove);hex(7,indStairs(ii,1))];
            hex2 = [siz-2;hex(1,indStairs(ii,2));hex(2,indRemove);hex(2,indStairs(ii,2));...
                siz;hex(5,indStairs(ii,2));hex(6,indRemove);hex(6,indStairs(ii,2))];
            hex = [hex,hex1,hex2];
    end
end
Remove(indStairs) = 1;
hex(:,find(Remove)) = [];

