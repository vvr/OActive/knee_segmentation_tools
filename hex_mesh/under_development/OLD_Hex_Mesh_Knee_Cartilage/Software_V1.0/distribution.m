function points = distribution(type,p,vL,fL,LH,LV,ratioV,ratioH,radius)
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%Puntos del rectangulo interior
switch type
    case 'lateral'
        p1 = p(1:2) + [LH,LV];
        p2 = p(1:2) + [LH,-LV];
        p3 = p(1:2) + [-LH,-LV];
        p4 = p(1:2) + [-LH,LV];
        angles = [45:90/ratioH:135,135:90/ratioV:225,225:90/ratioH:315,315:90/ratioV:405]';
    case 'medial'
        p1 = p(1:2) + [0,LH];
        p2 = p(1:2) + [LV,0];
        p3 = p(1:2) + [0,-LH];
        p4 = p(1:2) + [-LV,0];
        angles = [90:90/ratioH:180,180:90/ratioV:270,270:90/ratioH:360,360:90/ratioV:450]';
end

%Patch1
V1 = QuadPatch( p1,p2,p4,p3,ratioV,ratioH );

v = vL;
v(:,3) = 0;
v0 = v(fL(:,1),:);
v1 = v(fL(:,2),:);
v2 = v(fL(:,3),:);

points = reshape(V1,2,[]);

% patch('vertices',vL,'faces',fL)
% hold on
% axis equal
bordes = [squeeze(V1(:,:,1)),squeeze(V1(:,end,:)),fliplr(squeeze(V1(:,:,end))),fliplr(squeeze(V1(:,1,:)))]';
angles = [cosd(angles),sind(angles)];
for i=1:size(bordes,1)
    ray = [bordes(i,:),bordes(i,:)+50*angles(i,:)];
    segments = [v0(:,1:2),v1(:,1:2);v0(:,1:2),v2(:,1:2);v1(:,1:2),v2(:,1:2)];
    out = lineSegmentIntersect(segments,ray);
    a = out.intAdjacencyMatrix==1;
    value = max(out.intNormalizedDistance2To1(a));
    initialP = bordes(i,:);
    finalP = bordes(i,:) + 0.99*value*50*angles(i,:);
%     patch('vertices',[[initialP,-5];[initialP,8];[finalP,8];[finalP,-5]],'faces',[1,2,3,4])
    points = [points,divideSize(initialP,finalP,radius)];
end
points = points';
% plot(points(:,1),points(:,2),'.')
end