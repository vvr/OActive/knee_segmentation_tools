function hex = computeHexa( s2, s3 )
%computeHexa Computes nodes and hexahedra of the hexahedral mesh
%   s2 number of elements in the radial direction
%   s3 total number of elements 
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Create index to the nodes 
indexL = (1:s3*2);
index = reshape(indexL,s2,s3/s2,2);
%% Associate each node with its neighbors
% Initialize the number of hexahedra and its 8 components
hex = NaN(8,s2-1,s3/s2-1);
% for each radial direction
for ii=1:s2-1
    % for each axial direction
    for jj=1:s3/s2-1
%   
%       4 ---------- 3
%      /|           /|
%     / |          / |
%    1 -|-------- 2  |
%    |  8 --------|- 7
%    | /          | /
%    |/           |/
%    5 ---------- 6
%
        hex(1,ii,jj) = index(ii,jj,1);
        hex(2,ii,jj) = index(ii+1,jj,1);
        hex(3,ii,jj) = index(ii+1,jj+1,1);
        hex(4,ii,jj) = index(ii,jj+1,1);
        hex(5,ii,jj) = index(ii,jj,2);
        hex(6,ii,jj) = index(ii+1,jj,2);
        hex(7,ii,jj) = index(ii+1,jj+1,2);
        hex(8,ii,jj) = index(ii,jj+1,2);
    end
end
    
hex = reshape(hex, 8, []);
end

