function [ N, H ] = refineHexMesh( nodes, hex, ind6Nodes )
%   refineHexMesh divides a 8-nodes hexahedron into 4 8-node hexahedra
%   nodes is a Mx3 matrix with the vertices' coordinates
%   hex is a Nx8 matrix with the indices to the vertices of each hexahedron
%   ind6Nodes is a Nx1 with 0 when 8-node and 1,2,3 or 4 if 6-node
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

    siz = size(hex,1);
%% Create nodes and indices to those nodes
% Node 1
    v1 = nodes(hex(:,1),:);
    ind1 = 0*siz + (1:siz);
% Node 2
    v2 = nodes(hex(:,2),:);
    ind2 = 1*siz + (1:siz);
% Node 3
    v3 = nodes(hex(:,3),:);
    ind3 = 2*siz + (1:siz);
% Node 4
    v4 = nodes(hex(:,4),:);
    ind4 = 3*siz + (1:siz);
% Node 5
    v5 = nodes(hex(:,5),:);
    ind5 = 4*siz + (1:siz);
% Node 6
    v6 = nodes(hex(:,6),:);
    ind6 = 5*siz + (1:siz);
% Node 7
    v7 = nodes(hex(:,7),:);
    ind7 = 6*siz + (1:siz);
% Node 8
    v8 = nodes(hex(:,8),:);
    ind8 = 7*siz + (1:siz);
% New nodes
% Node between 1 and 2
    v1_2 = (v1 + v2)/2;
    ind1_2 = 8*siz + (1:siz);
% Node between 2 and 3
    v2_3 = (v2 + v3)/2;
    ind2_3 = 9*siz + (1:siz);
% Node between 3 and 4
    v3_4 = (v3 + v4)/2;
    ind3_4 = 10*siz + (1:siz);
% Node between 4 and 1
    v4_1 = (v4 + v1)/2;
    ind4_1 = 11*siz + (1:siz);
% Center of the face 1-2-3-4
    c1_4 = (v1 + v2 + v3 + v4)/4;
    indc1_4 = 12*siz + (1:siz);
% Node between 5 and 6
    v5_6 = (v5 + v6)/2;
    ind5_6 = 13*siz + (1:siz);
% Node between 6 and 7
    v6_7 = (v6 + v7)/2;
    ind6_7 = 14*siz + (1:siz);
% Node between 7 and 8
    v7_8 = (v7 + v8)/2;
    ind7_8 = 15*siz + (1:siz);
% Node between 8 and 5
    v8_5 = (v8 + v5)/2;
    ind8_5 = 16*siz + (1:siz);
% Center of the face 5-6-7-8
    c5_8 = (v5 + v6 + v7 + v8)/4;
    indc5_8 = 17*siz + (1:siz);
%% Find those hexahedra with 6 nodes and divide them into 1 8-node and 2 6-node
    c1_4(ind6Nodes == 2,:) = (v1(ind6Nodes == 2,:)+v3(ind6Nodes == 2,:))/2;
    c5_8(ind6Nodes == 2,:) = (v5(ind6Nodes == 2,:)+v7(ind6Nodes == 2,:))/2;
    c1_4(ind6Nodes == 4,:) = (v1(ind6Nodes == 4,:)+v3(ind6Nodes == 4,:))/2;
    c5_8(ind6Nodes == 4,:) = (v5(ind6Nodes == 4,:)+v7(ind6Nodes == 4,:))/2;
    c1_4(ind6Nodes == 1,:) = (v2(ind6Nodes == 1,:)+v4(ind6Nodes == 1,:))/2;
    c5_8(ind6Nodes == 1,:) = (v6(ind6Nodes == 1,:)+v8(ind6Nodes == 1,:))/2;
    c1_4(ind6Nodes == 3,:) = (v2(ind6Nodes == 3,:)+v4(ind6Nodes == 3,:))/2;
    c5_8(ind6Nodes == 3,:) = (v6(ind6Nodes == 3,:)+v8(ind6Nodes == 3,:))/2;

%% Create the new nodes and hexahedra
    hex1 = [indc1_4;ind2_3;ind3;ind3_4;indc5_8;ind6_7;ind7;ind7_8];
    hex2 = [ind4_1;indc1_4;ind3_4;ind4;ind8_5;indc5_8;ind7_8;ind8];
    hex3 = [ind1;ind1_2;indc1_4;ind4_1;ind5;ind5_6;indc5_8;ind8_5];
    hex4 = [ind1_2;ind2;ind2_3;indc1_4;ind5_6;ind6;ind6_7;indc5_8];

    H = [hex1,hex2,hex3,hex4]';
    N = [v1;v2;v3;v4;v5;v6;v7;v8;v1_2;v2_3;v3_4;v4_1;c1_4;v5_6;v6_7;v7_8;v8_5;c5_8];
    % and eliminates the redundant nodes
    [N, ia, ic] = unique(N,'rows');
    H = ic(H);
end

