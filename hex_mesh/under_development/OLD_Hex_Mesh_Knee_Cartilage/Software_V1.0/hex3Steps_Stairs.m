function [hex,nodes] = hex3Steps_Stairs( hex, nodes, indRef, indices )
%hex3Steps_Stairs creates new nodes in order to transform a 3 step stair into 2
%corners
%   
% 3-stairs: __                      __
%             |__                     |__ __ 
%                |__        ---->        |__|
%                   |__                     |__
%                      |                       |
%   Inputs:
%       hex: Mx8
%       nodes: Nx3
%       indRef: first step of the stairs
%       indices: the other steps
%
%   Outputs:
%       hex and nodes, including the new nodes.
%
%
%
% Author: Borja Rodriguez-Vila
% E-mail: borja.rodriguez.vila@upm.es

%% depending on the orientation of the NaN nodes of the elements
    switch find(isnan(nodes(hex(:,indRef),1)), 1,'first')
        case 1
            p = find(ismember(hex(:,indRef),hex(:,indices(1))),1,'first');
            if p==4
                infe = indices(2);
            else
                infe = indices(1);
            end
            % Modifies the second step to have 8 nodes
            nodes(hex(1,infe),:) = (nodes(hex(2,infe),:)+nodes(hex(4,infe),:)-...
                nodes(hex(3,infe),:));
            nodes(hex(5,infe),:) = (nodes(hex(1,infe),:)+nodes(hex(7,infe),:)-...
                nodes(hex(3,infe),:));
        case 2
            p = find(ismember(hex(:,indRef),hex(:,indices(1))),1,'first');
            if p==1
                infe = indices(2);
            else
                infe = indices(1);
            end
            % Modifies the second step to have 8 nodes
            nodes(hex(2,infe),:) = (nodes(hex(1,infe),:)+nodes(hex(3,infe),:)-...
                nodes(hex(4,infe),:));
            nodes(hex(6,infe),:) = (nodes(hex(2,infe),:)+nodes(hex(8,infe),:)-...
                nodes(hex(4,infe),:));
        case 3
            p = find(ismember(hex(:,indRef),hex(:,indices(1))),1,'first');
            if p==2
                infe = indices(2);
            else
                infe = indices(1);
            end
            % Modifies the second step to have 8 nodes
            nodes(hex(3,infe),:) = (nodes(hex(4,infe),:)+nodes(hex(2,infe),:)-...
                nodes(hex(1,infe),:));
            nodes(hex(7,infe),:) = (nodes(hex(3,infe),:)+nodes(hex(5,infe),:)-...
                nodes(hex(1,infe),:));
        case 4
            p = find(ismember(hex(:,indRef),hex(:,indices(1))),1,'first');
            if p==3
                infe = indices(2);
            else
                infe = indices(1);
            end
            % Modifies the second step to have 8 nodes
            nodes(hex(4,infe),:) = (nodes(hex(3,infe),:)+nodes(hex(1,infe),:)-...
                nodes(hex(2,infe),:));
            nodes(hex(8,infe),:) = (nodes(hex(4,infe),:)+nodes(hex(6,infe),:)-...
                nodes(hex(2,infe),:));
    end
end
