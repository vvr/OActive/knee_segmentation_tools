% Punto central
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

p = origL(1:2);
%Puntos del hexagono interior
p1 = p + [7,12];
p2 = p + [7,-12];
p3 = p + [-7,-12];
p4 = p + [-7,12];

%Patch1
V1 = QuadPatch( p1,p2,p4,p3,12,7 );

v = vL;
v(:,3) = 0;
v0 = v(fL(:,1),:);
v1 = v(fL(:,2),:);
v2 = v(fL(:,3),:);
ray = [V1(:,1,1)',p+2*(V1(:,1,1)'-p)];
segments = [v0(:,1:2),v1(:,1:2);v0(:,1:2),v2(:,1:2);v1(:,1:2),v2(:,1:2)];
out = lineSegmentIntersect(segments,ray);
a = find(out.intAdjacencyMatrix);
[value,ind] = max(out.intNormalizedDistance2To1(a));
plot(out.intMatrixX(a(ind)),out.intMatrixY(a(ind)),'go')
