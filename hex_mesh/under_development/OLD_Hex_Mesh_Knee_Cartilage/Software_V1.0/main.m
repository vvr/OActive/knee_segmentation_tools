function [ femurMesh,lateralTibiaMesh, medialTibiaMesh, lateralMeniscusMesh, medialMeniscusMesh ] ...
    = main( stlFemur, stlLateral, stlMedial, stlLateralMenisc,stlMedialMenisc )
% Automated Hexahedral Meshing of Knee Cartilage Structures (V1.0)
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.
%
%   DESCRIPTION: Software for creating of hexahedral meshes of five 
%   anatomical structures of the knee: femural and tibial cartilages 
%   (tibial plateau divided into lateral and medial) and both menisci
%
%   INPUT: STL files of triangular meshes of each anatomical structure
%
%   OUTPUT: Custom Matlab structs of each anatomical structure,
%   containing:
%       - nodes of the mesh (Nx3)
%       - hexahedra of the mesh (Mx8)
%       - layer to which a hexahedra belongs (Mx1)
%       - Scaled Jacobian value of the hexahedra (Mx1)
clc
tic
%% Pre-analysis of the situation to automatize the process
% Computes some points that helps modeling the structures
[PL,PM,origL,origM,origLM,origMM] = preparation(stlFemur, stlLateral, stlMedial, stlLateralMenisc,stlMedialMenisc);
toc
%% Femur Mesh
% Computes the femural mesh using PL, PM, radial resolution and axial
% resolution
tic
[ nodesF, hexF, layerF ] = FemurHexMesh( stlFemur, PL, PM, 5, 2 );
toc
[ sF, ~ ] = computeScaledJacobian( nodesF, hexF );
femurMesh = struct('nodes',nodesF,'hex',hexF,'layers',layerF,'quality',sF);
%% Lateral Tibia Mesh
% Computes the tibial mesh using origL
tic
[ nodesL, hexL, layerL  ] = TibiaHexMesh( stlLateral, origL+[0,0,10],5,8,13,10,11 );
toc
[ sL, ~ ] = computeScaledJacobian( nodesL, hexL );
lateralTibiaMesh = struct('nodes',nodesL,'hex',hexL,'layers',layerL,'quality',sL);
%% Medial Tibia Mesh
tic
[ nodesM, hexM, layerM  ] = TibiaHexMesh( stlMedial, origM+[0,0,10],3,11,21,7,9 );
toc
[ sM, ~ ] = computeScaledJacobian( nodesM, hexM );
medialTibiaMesh = struct('nodes',nodesM,'hex',hexM,'layers',layerM,'quality',sM);
%% Lateral Meniscus Mesh
% Computes meniscus mesh using an origin and angular resolution
tic
[ nodesLM, hexLM ] = MeniscusHexMesh( stlLateralMenisc, origLM,6 );
toc
[ sLM, ~ ] = computeScaledJacobian( nodesLM, hexLM );
layerLM = ones(size(hexLM,1),1);
lateralMeniscusMesh = struct('nodes',nodesLM,'hex',hexLM,'layers',layerLM,'quality',sLM);
%% Medial Meniscus Mesh
tic
[ nodesMM, hexMM ] = MeniscusHexMesh( stlMedialMenisc, origMM,6, 1 );
toc
[ sMM, ~ ] = computeScaledJacobian( nodesMM, hexMM );
layerMM = ones(size(hexMM,1),1);
medialMeniscusMesh = struct('nodes',nodesMM,'hex',hexMM,'layers',layerMM,'quality',sMM);
%%
end

