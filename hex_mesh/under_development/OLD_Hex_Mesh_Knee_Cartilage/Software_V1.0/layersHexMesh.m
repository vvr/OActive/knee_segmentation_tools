function [ N, H, M ] = layersHexMesh( nodes, hex, direction )
%   layersHexMesh divides the mesh into the direction bone-exterior
%   nodes is amtrix of Mx3
%   hex is matrix of Nx8
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

siz = size(hex,1);
ind = NaN(28,siz);
V = NaN(28,siz,3);
if nargin<3
    direction = 0;
end
%% Create nodes and indices to those nodes
for ii=1:8
    V(ii,:,:) = nodes(hex(:,ii),:);
    ind(ii,:) = (ii-1)*siz + (1:siz);
end
for ii=9:13
%     V(ii,:,:) = V(1,:,:) + (ii-8)*(V(5,:,:)-V(1,:,:))/6;
    if direction == 1
        V(9,:,:)  = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.07;
        V(10,:,:) = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.14;
        V(11,:,:) = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.42;
        V(12,:,:) = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.70;
        V(13,:,:) = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.85;
    else
        V(9,:,:)  = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.15;
        V(10,:,:) = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.30;
        V(11,:,:) = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.58;
        V(12,:,:) = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.86;
        V(13,:,:) = V(1,:,:) + (V(5,:,:)-V(1,:,:))*0.93;
    end
    ind(ii,:) = (ii-1)*siz + (1:siz);
end
for ii=14:18
%     V(ii,:,:) = V(2,:,:) + (ii-13)*(V(6,:,:)-V(2,:,:))/6;
    if direction == 1
        V(14,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.07;
        V(15,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.14;
        V(16,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.42;
        V(17,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.70;
        V(18,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.85;
    else
        V(14,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.15;
        V(15,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.30;
        V(16,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.58;
        V(17,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.86;
        V(18,:,:) = V(2,:,:) + (V(6,:,:)-V(2,:,:))*0.93;
    end
    ind(ii,:) = (ii-1)*siz + (1:siz);
end
for ii=19:23
%     V(ii,:,:) = V(3,:,:) + (ii-18)*(V(7,:,:)-V(3,:,:))/6;
    if direction == 1
        V(19,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.07;
        V(20,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.14;
        V(21,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.42;
        V(22,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.70;
        V(23,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.85;
    else
        V(19,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.15;
        V(20,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.30;
        V(21,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.58;
        V(22,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.86;
        V(23,:,:) = V(3,:,:) + (V(7,:,:)-V(3,:,:))*0.93;
    end
    ind(ii,:) = (ii-1)*siz + (1:siz);
end
for ii=24:28
%     V(ii,:,:) = V(4,:,:) + (ii-23)*(V(8,:,:)-V(4,:,:))/6;
    if direction == 1
        V(24,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.07;
        V(25,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.14;
        V(26,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.42;
        V(27,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.70;
        V(28,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.85;
    else
        V(24,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.15;
        V(25,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.30;
        V(26,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.58;
        V(27,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.86;
        V(28,:,:) = V(4,:,:) + (V(8,:,:)-V(4,:,:))*0.93;
    end
    ind(ii,:) = (ii-1)*siz + (1:siz);
end
%% Create the new hexahedra
h1 = [ind(1,:);ind(2,:);ind(3,:);ind(4,:);ind(9,:);ind(14,:);ind(19,:);ind(24,:)];
h2 = [ind(9,:);ind(14,:);ind(19,:);ind(24,:);ind(10,:);ind(15,:);ind(20,:);ind(25,:)];
h3 = [ind(10,:);ind(15,:);ind(20,:);ind(25,:);ind(11,:);ind(16,:);ind(21,:);ind(26,:)];
h4 = [ind(11,:);ind(16,:);ind(21,:);ind(26,:);ind(12,:);ind(17,:);ind(22,:);ind(27,:)];
h5 = [ind(12,:);ind(17,:);ind(22,:);ind(27,:);ind(13,:);ind(18,:);ind(23,:);ind(28,:)];
h6 = [ind(13,:);ind(18,:);ind(23,:);ind(28,:);ind(5,:);ind(6,:);ind(7,:);ind(8,:)];

H = [h1,h2,h3,h4,h5,h6]';
if direction ==1
    M = [6*ones(size(h1,2),1);5*ones(size(h2,2),1);...
        4*ones(size(h3,2),1);3*ones(size(h4,2),1);...
        2*ones(size(h5,2),1);ones(size(h6,2),1)];
else
    M = [ones(size(h1,2),1);2*ones(size(h2,2),1);...
        3*ones(size(h3,2),1);4*ones(size(h4,2),1);...
        5*ones(size(h5,2),1);6*ones(size(h6,2),1)];
end
N = [];
for i=1:size(V,1)
    N = [N;squeeze(V(i,:,:))];
end

[N, ia, ic] = unique(N,'rows');
H = ic(H);

end

