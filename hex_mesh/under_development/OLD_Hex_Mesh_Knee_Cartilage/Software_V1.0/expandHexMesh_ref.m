function nodes = expandHexMesh_ref( v,f,nodes,hex,flag )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%expandHexMesh Expand the hexahedral mesh until the nodes are in contact
%with the STL triangular model formed by v and f

    v0 = v(f(:,1),:);
    v1 = v(f(:,2),:);
    v2 = v(f(:,3),:);
    
%% Find the faces that must be moved: all but the interior and exterior faces 
faceNormals = NaN(size(hex,1),3);

if flag == 0
    FF = [0,0,0,0,1,1,1,1];
else
    FF = [1,1,1,1,0,0,0,0];
end
for ii=1:size(hex,1)
    caras = ismember(hex,hex(ii,:));
    numFaces = find(sum(caras,2)==4);
    if numel(numFaces) < 8
        face = [];
        for jj=1:numel(numFaces)
            face = [face;ismember(hex(ii,:),hex(numFaces(jj),:))];
        end
        borderFace = find(ismember(FF,face,'rows')==0);
        if ~isempty(borderFace)
            [ faceNormals(ii,:), bIn, bOut] = faceNormal(nodes,hex(ii,:),6);
        end
    end
end
if flag == 0
    indN = unique(hex(:,5:8));
else
    indN = unique(hex(:,1:4));
end

orig = nodes(indN,:);
rays = orig.*0;
for ii=1:size(orig,1) 
    [x,y] = find(hex==indN(ii));
    rays(ii,:) = sum(faceNormals(x,:),1)./numel(x);
end

rays = -rays;

displacement = nodes.*0;

% for each new ray, compute the intersection to find the external points
for jj = 1: size(orig,1)
    [intersect, t, U, V, xcoor] = TriangleRayIntersection (orig(jj,:), rays(jj,:), v0, v1, v2);
    if sum(intersect)>flag
        inter = t(intersect);
        [value, ind] = min(inter);
        if value < 10 
            valids = xcoor(intersect,:);
            displacement(indN(jj),:) = valids(ind,:)-orig(jj,:);
        end
    end
end

% displacement = smoothHexMesh( displacement, hex, 'surface' );

nodes = nodes + displacement;

end