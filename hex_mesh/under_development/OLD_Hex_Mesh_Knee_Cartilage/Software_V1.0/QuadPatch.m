function V = QuadPatch( p0,p1,p2,p3, ratioH, ratioV )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

v1 = divideSize(p0,p1,ratioH);
v2 = divideSize(p2,p3,ratioH);
V = NaN(2,ratioV+1,ratioH+1);
for i=1:ratioH+1
    V(:,:,i) = divideSize(v1(:,i),v2(:,i),ratioV);
end

end

