function hexMesh2STL( hex, nodes, STL_file_name, solid )
% hexMesh2STL 
% writes the hexahedral mesh as a serie of triangular patches in STL format
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

quadFaces = Hex2Quads(hex);
triFaces  = [quadFaces(:,1:3);quadFaces(:,[1,3,4])];

STL_Export(nodes, triFaces, STL_file_name,solid);

end

