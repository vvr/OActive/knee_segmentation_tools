function [nodes, hex] = divideHex_v2(nodes, hex, elements)
% divideHex divides the hexahedra pointed by elements into 2 wedges.
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

for ii = 1:size(elements,1)
    %% Compute the internal angles of hexahedron
    % cos(theta) = dot(v1,v2)./(norm(v1)*norm(v2))
    v1_1 = nodes(hex(elements(ii),2),:)-nodes(hex(elements(ii),1),:);
    v2_1 = nodes(hex(elements(ii),4),:)-nodes(hex(elements(ii),1),:);
    v3_1 = nodes(hex(elements(ii),5),:)-nodes(hex(elements(ii),1),:);
    theta1_1 = acosd(dot(v1_1,v2_1,2)./(norm(v1_1)*norm(v2_1)));
    theta1_2 = acosd(dot(v1_1,v3_1,2)./(norm(v1_1)*norm(v3_1)));
    theta1_3 = acosd(dot(v2_1,v3_1,2)./(norm(v2_1)*norm(v3_1)));
    
    v1_2 = nodes(hex(elements(ii),1),:)-nodes(hex(elements(ii),2),:);
    v2_2 = nodes(hex(elements(ii),3),:)-nodes(hex(elements(ii),2),:);
    v3_2 = nodes(hex(elements(ii),6),:)-nodes(hex(elements(ii),2),:);
    theta2_1 = acosd(dot(v1_2,v2_2,2)./(norm(v1_2)*norm(v2_2)));
    theta2_2 = acosd(dot(v1_2,v3_2,2)./(norm(v1_2)*norm(v3_2)));
    theta2_3 = acosd(dot(v2_2,v3_2,2)./(norm(v2_2)*norm(v3_2)));
    
    v1_3 = nodes(hex(elements(ii),2),:)-nodes(hex(elements(ii),3),:);
    v2_3 = nodes(hex(elements(ii),4),:)-nodes(hex(elements(ii),3),:);
    v3_3 = nodes(hex(elements(ii),7),:)-nodes(hex(elements(ii),3),:);
    theta3_1 = acosd(dot(v1_3,v2_3,2)./(norm(v1_3)*norm(v2_3)));
    theta3_2 = acosd(dot(v1_3,v3_3,2)./(norm(v1_3)*norm(v3_3)));
    theta3_3 = acosd(dot(v2_3,v3_3,2)./(norm(v2_3)*norm(v3_3)));

    v1_4 = nodes(hex(elements(ii),1),:)-nodes(hex(elements(ii),4),:);
    v2_4 = nodes(hex(elements(ii),3),:)-nodes(hex(elements(ii),4),:);
    v3_4 = nodes(hex(elements(ii),8),:)-nodes(hex(elements(ii),4),:);
    theta4_1 = acosd(dot(v1_4,v2_4,2)./(norm(v1_4)*norm(v2_4)));
    theta4_2 = acosd(dot(v1_4,v3_4,2)./(norm(v1_4)*norm(v3_4)));
    theta4_3 = acosd(dot(v2_4,v3_4,2)./(norm(v2_4)*norm(v3_4)));

    v1_5 = nodes(hex(elements(ii),6),:)-nodes(hex(elements(ii),5),:);
    v2_5 = nodes(hex(elements(ii),8),:)-nodes(hex(elements(ii),5),:);
    v3_5 = nodes(hex(elements(ii),1),:)-nodes(hex(elements(ii),5),:);
    theta5_1 = acosd(dot(v1_5,v2_5,2)./(norm(v1_5)*norm(v2_5)));
    theta5_2 = acosd(dot(v1_5,v3_5,2)./(norm(v1_5)*norm(v3_5)));
    theta5_3 = acosd(dot(v2_5,v3_5,2)./(norm(v2_5)*norm(v3_5)));

    v1_6 = nodes(hex(elements(ii),5),:)-nodes(hex(elements(ii),6),:);
    v2_6 = nodes(hex(elements(ii),7),:)-nodes(hex(elements(ii),6),:);
    v3_6 = nodes(hex(elements(ii),2),:)-nodes(hex(elements(ii),6),:);
    theta6_1 = acosd(dot(v1_6,v2_6,2)./(norm(v1_6)*norm(v2_6)));
    theta6_2 = acosd(dot(v1_6,v3_6,2)./(norm(v1_6)*norm(v3_6)));
    theta6_3 = acosd(dot(v2_6,v3_6,2)./(norm(v2_6)*norm(v3_6)));

    v1_7 = nodes(hex(elements(ii),6),:)-nodes(hex(elements(ii),7),:);
    v2_7 = nodes(hex(elements(ii),8),:)-nodes(hex(elements(ii),7),:);
    v3_7 = nodes(hex(elements(ii),3),:)-nodes(hex(elements(ii),7),:);
    theta7_1 = acosd(dot(v1_7,v2_7,2)./(norm(v1_7)*norm(v2_7)));
    theta7_2 = acosd(dot(v1_7,v3_7,2)./(norm(v1_7)*norm(v3_7)));
    theta7_3 = acosd(dot(v2_7,v3_7,2)./(norm(v2_7)*norm(v3_7)));

    v1_8 = nodes(hex(elements(ii),5),:)-nodes(hex(elements(ii),8),:);
    v2_8 = nodes(hex(elements(ii),7),:)-nodes(hex(elements(ii),8),:);
    v3_8 = nodes(hex(elements(ii),4),:)-nodes(hex(elements(ii),8),:);
    theta8_1 = acosd(dot(v1_8,v2_8,2)./(norm(v1_8)*norm(v2_8)));
    theta8_2 = acosd(dot(v1_8,v3_8,2)./(norm(v1_8)*norm(v3_8)));
    theta8_3 = acosd(dot(v2_8,v3_8,2)./(norm(v2_8)*norm(v3_8)));
    
    
    theta = [theta1_1,theta1_2,theta1_3;...
        theta2_1,theta2_2,theta2_3;...
        theta3_1,theta3_2,theta3_3;...
        theta4_1,theta4_2,theta4_3;...
        theta5_1,theta5_2,theta5_3;...
        theta6_1,theta6_2,theta6_3;...
        theta7_1,theta7_2,theta7_3;...
        theta8_1,theta8_2,theta8_3];
    [x,y] = max(theta(:));
    [a,b] = ind2sub(size(theta),y);
        switch b
            case 1
                if rem(a,2)
                    hex1 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),3),...
                        hex(elements(ii),3),hex(elements(ii),5),hex(elements(ii),6),...
                        hex(elements(ii),7),hex(elements(ii),7)];
                    hex2 = [hex(elements(ii),1),hex(elements(ii),1),hex(elements(ii),3),...
                        hex(elements(ii),4),hex(elements(ii),5),hex(elements(ii),5),...
                        hex(elements(ii),7),hex(elements(ii),8)];
                else
                    hex1 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),2),...
                        hex(elements(ii),4),hex(elements(ii),5),hex(elements(ii),6),...
                        hex(elements(ii),6),hex(elements(ii),8)];
                    hex2 = [hex(elements(ii),2),hex(elements(ii),3),hex(elements(ii),4),...
                        hex(elements(ii),4),hex(elements(ii),6),hex(elements(ii),7),...
                        hex(elements(ii),8),hex(elements(ii),8)];
                end
            case 2
                if rem(a,2)
                    hex1 = [hex(elements(ii),1),hex(elements(ii),1),hex(elements(ii),4),...
                        hex(elements(ii),4),hex(elements(ii),5),hex(elements(ii),6),...
                        hex(elements(ii),7),hex(elements(ii),8)];
                    hex2 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),3),...
                        hex(elements(ii),4),hex(elements(ii),6),hex(elements(ii),6),...
                        hex(elements(ii),7),hex(elements(ii),7)];
                else
                    hex1 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),3),...
                        hex(elements(ii),4),hex(elements(ii),5),hex(elements(ii),5),...
                        hex(elements(ii),8),hex(elements(ii),8)];
                    hex2 = [hex(elements(ii),2),hex(elements(ii),2),hex(elements(ii),3),...
                        hex(elements(ii),3),hex(elements(ii),5),hex(elements(ii),6),...
                        hex(elements(ii),7),hex(elements(ii),8)];
                end
            case 3
                if rem(a,2)
                    hex1 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),3),...
                        hex(elements(ii),4),hex(elements(ii),8),hex(elements(ii),7),...
                        hex(elements(ii),7),hex(elements(ii),8)];
                    hex2 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),2),...
                        hex(elements(ii),1),hex(elements(ii),5),hex(elements(ii),6),...
                        hex(elements(ii),7),hex(elements(ii),8)];
                else
                    hex1 = [hex(elements(ii),4),hex(elements(ii),3),hex(elements(ii),3),...
                        hex(elements(ii),4),hex(elements(ii),5),hex(elements(ii),6),...
                        hex(elements(ii),7),hex(elements(ii),8)];
                    hex2 = [hex(elements(ii),1),hex(elements(ii),2),hex(elements(ii),3),...
                        hex(elements(ii),4),hex(elements(ii),5),hex(elements(ii),6),...
                        hex(elements(ii),6),hex(elements(ii),5)];
                end
                
        end

        hex = [hex;hex1;hex2];

end

hex(elements,:) = [];

end

