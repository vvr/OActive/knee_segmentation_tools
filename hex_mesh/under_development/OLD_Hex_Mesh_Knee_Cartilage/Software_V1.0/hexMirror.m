function [ hex, nodes ] = hexMirror( hex, nodes, indMirror)
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

if numel(indMirror)>0
    if size(indMirror,2)==1
        indMirror = indMirror';
    end
    for ii=1:size(indMirror,1)
        n1 = hex(:,indMirror(ii,:));
        NN = unique(n1);
        elements = NN(isnan(nodes(NN,1)));
        n1 = n1(:);
        indEQ = [ismember(hex(:,indMirror(ii,1)),hex(:,indMirror(ii,2)));...
            ismember(hex(:,indMirror(ii,2)),hex(:,indMirror(ii,1)))];
        n1(indEQ) = [];
        nodes(elements(1),:) = sum(nodes(n1([1,2,5,6]),:))/4;
        nodes(elements(2),:) = sum(nodes(n1([3,4,7,8]),:))/4;
    end
end

end

