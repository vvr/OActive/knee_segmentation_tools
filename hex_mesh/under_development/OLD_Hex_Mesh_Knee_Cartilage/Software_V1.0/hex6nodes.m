function [ hex, nodes ] = hex6nodes( hex, nodes )
% hex6nodes_SU transforms hexahedra with 6 nodes depending on their shape
% and relationship with their neigbors.
% Peaks: __|__
% Mirror: |__ __|
% 3-stairs: __
%             |__
%                |__
%                   |__
%                      |
% stairs: __
%           |__
%              |__
%                 |
% corners: __
%            |__
%               |
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Find peaks
[x,y] = find(a==4);
indPeaks = unique(sort([x,y],2),'rows');
indAux = zeros(size(indPeaks,1),1);
for jj=1:size(indPeaks,1)
    H = hex(:,ind6nodes(indPeaks(jj,1)));
    indAux(jj) = sum(isnan(nodes(H(ismember(H,hex(:,ind6nodes(indPeaks(jj,2)))),1))))>0;
end
indPeaks(find(indAux),:) = [];
indPeaks = unique(indPeaks);
[hex, nodes] = hexPeaks( hex, nodes, ind6nodes(indPeaks));
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Check for mirror stairs
[x,y] = find(a==4);
indMirror = unique(sort([x,y],2),'rows');
[hex, nodes] = hexMirror( hex, nodes, ind6nodes(indMirror));
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Eliminates 3-stairs
[x,y] = find(a==2);
indStairs = unique(sort([x,y],2),'rows');

if ~isempty(indStairs)
    if sum(ismember(indStairs(:,1),indStairs(:,2)))==0
        check3Stairs = 0;
    else
        [x,y] = find(indStairs==indStairs(find(ismember(indStairs(:,1),indStairs(:,2)),1,'first')));
        triStairs = indStairs(x,:);
        check3Stairs = 1;
    end

    while check3Stairs
        ii=1;
        vacio = 1;
        while vacio
            indC = find(triStairs(:,2)==triStairs(ii,1));
            if ~isempty(indC)
                vacio = 0;
                couple = [triStairs(ii,2);triStairs(indC,1)];
                indRef = triStairs(ii,1);
            else
                ii = ii+1;
            end
        end
        [hex,nodes] = hex3Stairs( hex, nodes, ind6nodes(indRef),ind6nodes(couple));

        % Check if there are more 3-stairs
        [a, ind6nodes] = checkHEX( hex,nodes );
        [x,y] = find(a==2);
        indStairs = unique(sort([x,y],2),'rows');

        if sum(ismember(indStairs(:,1),indStairs(:,2)))==0
            check3Stairs = 0;
        else
            [x,~] = find(indStairs==indStairs(find(ismember(indStairs(:,1),indStairs(:,2)),1,'first')));
            triStairs = indStairs(x,:);
            check3Stairs = 1;
        end
    end
end
%% Check for NaN nodes
[a, ind6nodes] = checkHEX( hex,nodes );
%% Check for new mirrors that may have appeared
if ismember(4,unique(a))
    [x,y] = find(a==4);
    indMirror = unique(sort([x,y],2),'rows');
    [hex, nodes] = hexMirror( hex, nodes, ind6nodes(indMirror));
    [a, ind6nodes] = checkHEX( hex,nodes );
end
%% Find stairs
[x,y] = find(a==2);
indStairs = unique(sort([x,y],2),'rows');
if ~isempty(indStairs)
    [hex, nodes] = hexStairs( hex, nodes, ind6nodes(indStairs));
end
%% Check for NaN nodes
indexNaN = checkNaN( hex,nodes );
ind6nodes = find(sum(indexNaN,2)==2);

%% Solve normals corners
[hex, nodes] = corners( hex,nodes, ind6nodes );
end

