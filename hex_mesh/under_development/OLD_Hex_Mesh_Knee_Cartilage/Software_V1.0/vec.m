function [vec1, vec2, vec3] = vec( hex, indexV )
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.

switch indexV
    case 1
        vec1 = hex([2,4]');
        vec2 = hex(3);
        vec3 = hex(7);
    case 2
        vec1 = hex([1,3]');
        vec2 = hex(4);
        vec3 = hex(8);
    case 3
        vec1 = hex([2,4]');
        vec2 = hex(1);
        vec3 = hex(5);
    case 4
        vec1 = hex([1,3]');
        vec2 = hex(2);
        vec3 = hex(6);
    case 5
        vec1 = hex([6,8]');
        vec2 = hex(7);
        vec3 = hex(3); 
    case 6
        vec1 = hex([5,7]');
        vec2 = hex(8);
        vec3 = hex(4); 
    case 7
        vec1 = hex([6,8]');
        vec2 = hex(5);
        vec3 = hex(1);
    case 8
        vec1 = hex([5,7]');
        vec2 = hex(6);
        vec3 = hex(2);
end
end

