function [rays, points, siz1, siz2] = RaysForFemur(P1,P2,RadialRes, LongRes, verbose)
%Given 2 points, obtain the direction of the vector which link them, and 
%compute the ray for TriangleRayIntersection using cylindrical coordinates
%using the vector as main axis.
%P1: first point
%P2: second point
%RadialRes: angle between consecutive rays, in degress
%PP: Points of the main axis where the rays are going to be computed
%if verbose = 1, draw the rays
%
% B. Rodríguez-Vila and D.M. Pierce 
% Interdisciplinary Mechanics Laboratory, http://im.engr.uconn.edu
% University of Connecticut, Storrs
% June 05, 2017
% Copyright (c) 2017, All Rights Reserved.
%
% CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
% EITHER EXPRESSED OR IMPLIED.
   
%% Radial resolution: angles in a single plane
    %Creation of the angles of the rays
    theta = (0:360/RadialRes-1)*RadialRes;
    theta = [circshift(theta,size(theta,2)/2,2),180];
    siz = size(theta,2);
    recta3D = [cosd(theta); sind(theta); zeros(1,siz)];
    %Main vector 
    vector = P2 - P1;
    vector = vector/norm(vector);
    %Rotation of the rays to be perpendicular to the main vector
    R = rotateAtoB([0,0,1]',vector');
    r = R*recta3D;

%% Creation of the spherical extremes
    % Angles of the rays
    phi = (0:90/RadialRes-1)*RadialRes;
    p = fliplr(phi);
    recta3D_left = [];
    recta3D_right = [];
    for ii=1:size(phi,2)
        recta3D_right = [recta3D_right, [cosd(theta).*sind(p(ii)); sind(theta).*sind(p(ii)); ones(1,siz).*cosd(p(ii))]];
        recta3D_left = [recta3D_left, [cosd(theta).*sind(phi(ii)); sind(theta).*sind(phi(ii)); -ones(1,siz).*cosd(phi(ii))]];
    end
    casquete_left = R*recta3D_left;
    casquete_right = R*recta3D_right;
%% Longitudinal resolution
    s = (P2-P1)./round(norm(P2-P1)/LongRes);
    L = (0:1:norm(P2-P1)/norm(s));
    T = [L*s(1);L*s(2);L*s(3)];
    PP = repmat(P1',1,size(T,2))+T;
%     PP = [PP,P2'];
%%     
    %Variable with the end point of the vector defining the rays
    rays = zeros(3,siz,size(PP,2));
    %Variable with the starting point of the vector defining the rays
    points = rays;
    %Application of the rays on every point of LongRes
    for ii=1:size(PP,2)
        points(:,:,ii) = repmat(PP(:,ii),1,siz);
        rays(:,:,ii) = r;
    end
    [~,siz1,siz2] = size(points);
    siz2 = siz2 + 2*size(phi,2);
    points = reshape(points,3,[]);
    points = [repmat(P1',1,size(casquete_left,2)),points,repmat(P2',1,size(casquete_left,2))]';
    rays = reshape(rays,3,[]);
    rays = [casquete_left, rays, casquete_right]';
%% Paint the rays    
    %If verbose is not set, no draw
    if nargin < 5
        verbose = 0;
    end
    if(verbose)
        for jj=1:size(rays,1)
            line([points(jj,1),rays(jj,1)+points(jj,1)],...
                [points(jj,2),rays(jj,2)+points(jj,2)],...
                [points(jj,3),rays(jj,3)+points(jj,3)]),hold on
        end
    end
end