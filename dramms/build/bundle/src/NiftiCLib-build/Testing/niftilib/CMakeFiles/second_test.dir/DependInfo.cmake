# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/Testing/niftilib/nifti_test2.c" "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/Testing/niftilib/CMakeFiles/second_test.dir/nifti_test2.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_ZLIB"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/niftilib"
  "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/znzlib"
  "/home/filippos/programs/dr/build/bundle/src/NiftiCLib/niftilib/../utils"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/niftilib/CMakeFiles/niftiio.dir/DependInfo.cmake"
  "/home/filippos/programs/dr/build/bundle/src/NiftiCLib-build/znzlib/CMakeFiles/znz.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
