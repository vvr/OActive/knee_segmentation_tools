# ============================================================================
# Copyright (c) 2011-2012 University of Pennsylvania
# Copyright (c) 2013-2014 Andreas Schuh
# All rights reserved.
#
# See COPYING file for license information or visit
# http://opensource.andreasschuh.com/cmake-basis/download.html#license
# ============================================================================

##############################################################################
# @file  configure_script.cmake.in
# @brief Build script module.
#
# @note This file is generated by BASIS from the template file
#       configure_script.cmake.in which is part of the BASIS installation.
#
# This script is configured using CMake's configure_sript() function instead
# of providing its arguments using the -D option of CMake before the -P option
# such that build command can be made dependent on the configured build script.
# It calls the function basis_configure_script() twice, once to configure
# the script file for use in the build tree, and once for installation (optional).
##############################################################################

cmake_minimum_required (VERSION 2.8.4)

include ("/home/filippos/programs/dr/share/dramms/basis/cmake-modules/CommonTools.cmake") # basis_configure_script()

# settings
set (SOURCE_FILE          "/home/filippos/programs/dr/share/dramms/basis/utilities/basis.sh.in")
set (OUTPUT_FILE          "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/bash/dramms/basis.sh")
set (INSTALL_FILE         "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/basis_sh.dir/build/dramms/basis.sh")
set (DESTINATION          "/home/filippos/programs/dr/lib/dramms/bash/dramms")
set (BUILD_LINK_DEPENDS   "")
set (INSTALL_LINK_DEPENDS "")
set (CACHE_FILE           "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/basis_sh.dir/cache.cmake")
set (CONFIG_FILE          "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/config/BasisScriptConfig.cmake;/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/basis_sh.dir/ScriptConfig.cmake")
set (LANGUAGE             "BASH")
set (OPTIONS              "")
# configure ("build") script - always touch files to update build timestamp
basis_configure_script (
  "${SOURCE_FILE}" "${OUTPUT_FILE}"
  LINK_DEPENDS "${BUILD_LINK_DEPENDS}"
  CACHE_FILE   "${CACHE_FILE}"
  CONFIG_FILE  "${CONFIG_FILE}"
  LANGUAGE     "${LANGUAGE}"
  ${OPTIONS}
)
if (INSTALL_FILE AND DESTINATION)
  basis_configure_script (
    "${SOURCE_FILE}" "${INSTALL_FILE}"
    DESTINATION  "${DESTINATION}"
    LINK_DEPENDS "${INSTALL_LINK_DEPENDS}"
    CACHE_FILE   "${CACHE_FILE}"
    CONFIG_FILE  "${CONFIG_FILE}"
    LANGUAGE     "${LANGUAGE}"
    ${OPTIONS}
  )
endif ()
