# Install script for directory: /home/filippos/programs/dramms-1.5.1-source

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/filippos/programs/dr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc/dramms" TYPE FILE OPTIONAL RENAME "COPYING" FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/COPYING.txt")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Runtime" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "dramms" FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/CMakeFiles/dramms.dir/build/dramms")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Development" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/dramms/bash/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/basis_sh.dir/build/dramms/basis.sh")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Runtime" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/DRAMMSExports.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/DRAMMSExports.cmake"
         "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/Export/lib/cmake/dramms/DRAMMSExports.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/DRAMMSExports-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/DRAMMSExports.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/Export/lib/cmake/dramms/DRAMMSExports.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/Export/lib/cmake/dramms/DRAMMSExports-release.cmake")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Development" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/DRAMMSExports.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/DRAMMSExports.cmake"
         "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/Export/lib/cmake/dramms/DRAMMSExports.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/DRAMMSExports-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms/DRAMMSExports.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/Export/lib/cmake/dramms/DRAMMSExports.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/Export/lib/cmake/dramms/DRAMMSExports-release.cmake")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/Export/lib/cmake/dramms/DRAMMSCustomExports.cmake")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/config/DRAMMSConfig.cmake")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/DRAMMSConfigVersion.cmake")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/dramms" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/DRAMMSUse.cmake")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  # -----------------------------------------------------------------------
  # basis_install_directory(): /home/filippos/programs/dramms-1.5.1-source/include
  set (BASIS_INSTALL_DIRECTORY_FILES)
  set (BASIS_INSTALL_DIRECTORY_SOURCE      "/home/filippos/programs/dramms-1.5.1-source/include")
  set (BASIS_INSTALL_DIRECTORY_DESTINATION "$ENV{DESTDIR}/home/filippos/programs/dr/include")
  set (BASIS_INSTALL_DIRECTORY_MATCH       "")
  set (BASIS_INSTALL_DIRECTORY_EXCLUDE     ".*\\.in|CMakeLists.txt$|/.svn/|/.git/|.DS_Store$|.*~$")
  file (GLOB_RECURSE BASIS_INSTALL_DIRECTORY_ALL_FILES "${BASIS_INSTALL_DIRECTORY_SOURCE}/*")
  foreach (BASIS_INSTALL_DIRECTORY_FILE IN LISTS BASIS_INSTALL_DIRECTORY_ALL_FILES)
    if (NOT BASIS_INSTALL_DIRECTORY_MATCH                                            OR
            BASIS_INSTALL_DIRECTORY_FILE MATCHES "${BASIS_INSTALL_DIRECTORY_MATCH}" AND
        NOT BASIS_INSTALL_DIRECTORY_FILE MATCHES "${BASIS_INSTALL_DIRECTORY_EXCLUDE}")
      list (APPEND BASIS_INSTALL_DIRECTORY_FILES "${BASIS_INSTALL_DIRECTORY_FILE}")
   endif ()
  endforeach ()
  foreach (BASIS_INSTALL_DIRECTORY_FILE IN LISTS BASIS_INSTALL_DIRECTORY_FILES)
    file (RELATIVE_PATH BASIS_INSTALL_DIRECTORY_FILE "${BASIS_INSTALL_DIRECTORY_SOURCE}" "${BASIS_INSTALL_DIRECTORY_FILE}")
    execute_process (
      COMMAND "/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake" -E compare_files
                  "${BASIS_INSTALL_DIRECTORY_SOURCE}/${BASIS_INSTALL_DIRECTORY_FILE}"
                  "${BASIS_INSTALL_DIRECTORY_DESTINATION}/${BASIS_INSTALL_DIRECTORY_FILE}"
      RESULT_VARIABLE RC
      OUTPUT_QUIET
      ERROR_QUIET
    )
    if (RC EQUAL 0)
      message (STATUS "Up-to-date: ${BASIS_INSTALL_DIRECTORY_DESTINATION}/${BASIS_INSTALL_DIRECTORY_FILE}")
    else ()
      message (STATUS "Installing: ${BASIS_INSTALL_DIRECTORY_DESTINATION}/${BASIS_INSTALL_DIRECTORY_FILE}")
      execute_process (
        COMMAND "/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake" -E copy_if_different
            "${BASIS_INSTALL_DIRECTORY_SOURCE}/${BASIS_INSTALL_DIRECTORY_FILE}"
            "${BASIS_INSTALL_DIRECTORY_DESTINATION}/${BASIS_INSTALL_DIRECTORY_FILE}"
        RESULT_VARIABLE RC
        OUTPUT_QUIET
        ERROR_QUIET
      )
      if (RC EQUAL 0)
        list (APPEND CMAKE_INSTALL_MANIFEST_FILES "${BASIS_INSTALL_DIRECTORY_DESTINATION}/${BASIS_INSTALL_DIRECTORY_FILE}")
      else ()
        message (STATUS "Failed to install ${BASIS_INSTALL_DIRECTORY_DESTINATION}/${BASIS_INSTALL_DIRECTORY_FILE}")
      endif ()
    endif ()
  endforeach ()
  # -----------------------------------------------------------------------
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/filippos/.cmake/packages/DRAMMS/SBIA-DRAMMS-1.5.1")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/filippos/.cmake/packages/DRAMMS" TYPE FILE RENAME "SBIA-DRAMMS-1.5.1" FILES "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/DRAMMSRegistryFile")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/cmake_install.cmake")
  include("/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/uninstall/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
