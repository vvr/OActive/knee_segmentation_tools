# CMake variables dump created by BASIS
set (A "")
set (ARG "")
set (ARGN "")
set (BASH_EXECUTABLE "/bin/bash")
set (BASH_LIBRARY_TARGET "bashlib")
set (BASH_VERSION_MAJOR "4")
set (BASH_VERSION_MINOR "4")
set (BASH_VERSION_PATCH "19")
set (BASH_VERSION_STRING "4.4.19")
set (BASIS_ALL_DOC "OFF")
set (BASIS_BASHPATH "/home/filippos/programs/dr/lib/dramms/bash")
set (BASIS_BASH_EXECUTABLE "/bin/bash")
set (BASIS_BASH_TEMPLATES_DIR "/home/filippos/programs/dr/share/dramms/basis/utilities")
set (BASIS_BASH_UTILITIES_LIBRARY "basis.utilities_bash")
set (BASIS_BASH___DIR__ "$(cd -- \"$(dirname -- \"\${BASH_SOURCE}\")\" && pwd -P)")
set (BASIS_BASH___FILE__ "$(cd -- \"$(dirname -- \"\${BASH_SOURCE}\")\" && pwd -P)/$(basename -- \"$BASH_SOURCE\")")
set (BASIS_BUILD_ONLY "OFF")
set (BASIS_COMPILE_MATLAB "ON")
set (BASIS_COMPILE_SCRIPTS "OFF")
set (BASIS_CONFIG "/home/filippos/programs/dr/lib/cmake/dramms/BASISConfig.cmake")
set (BASIS_CONFIGURE_PUBLIC_HEADERS "FALSE")
set (BASIS_CONSIDERED_CONFIGS "/home/filippos/programs/dr/lib/cmake/dramms/BASISConfig.cmake")
set (BASIS_CONSIDERED_VERSIONS "3.2.0")
set (BASIS_CREATE_EXPORTS_FILE "ON")
set (BASIS_CUSTOM_EXPORTS_FILE "/home/filippos/programs/dr/lib/cmake/dramms/BASISCustomExports.cmake")
set (BASIS_CXX_TEMPLATES_DIR "/home/filippos/programs/dr/share/dramms/basis/utilities")
set (BASIS_CXX_UTILITIES_LIBRARY "basis.utilities_cxx")
set (BASIS_DEBUG "OFF")
set (BASIS_DIR "/home/filippos/programs/dr/lib/cmake/dramms")
set (BASIS_DOXYGEN_DOXYFILE "/home/filippos/programs/dr/share/dramms/basis/cmake-modules/Doxyfile.in")
set (BASIS_EXPORT "TRUE")
set (BASIS_EXPORTS_FILE "/home/filippos/programs/dr/lib/cmake/dramms/BASISExports.cmake")
set (BASIS_FOUND "1")
set (BASIS_GE_CONFIG "CONFIG")
set (BASIS_INCLUDE_DIR "/home/filippos/programs/dr/include")
set (BASIS_INCLUDE_DIRS "/home/filippos/programs/dr/include")
set (BASIS_INSTALL_PREFIX "/home/filippos/programs/dr")
set (BASIS_INSTALL_PUBLIC_HEADERS_OF_CXX_UTILITIES "FALSE")
set (BASIS_INSTALL_RPATH "ON")
set (BASIS_INSTALL_SCHEME "usr")
set (BASIS_INSTALL_SCHEME "usr")
set (BASIS_INSTALL_SITE_PACKAGES "OFF")
set (BASIS_JYTHONPATH "/home/filippos/programs/dr/lib/dramms/jython")
set (BASIS_JYTHON_TEMPLATES_DIR "/home/filippos/programs/dr/")
set (BASIS_JYTHON_UTILITIES_LIBRARY "basis.")
set (BASIS_LANGUAGES "CMake;CXX;Python;Jython;Perl;Matlab;Bash")
set (BASIS_LANGUAGES_L "cmake;cxx;python;jython;perl;matlab;bash")
set (BASIS_LANGUAGES_U "CMAKE;CXX;PYTHON;JYTHON;PERL;MATLAB;BASH")
set (BASIS_LIBRARIES "basis.testlib")
set (BASIS_LIBRARY_COMPONENT "Development")
set (BASIS_LIBRARY_DIR "/home/filippos/programs/dr/lib/dramms/basis")
set (BASIS_LIBRARY_DIRS "/home/filippos/programs/dr/lib/dramms/basis")
set (BASIS_MATLABPATH "/home/filippos/programs/dr/lib/dramms/matlab")
set (BASIS_MATLAB_TEMPLATES_DIR "/home/filippos/programs/dr/share/dramms/basis/utilities")
set (BASIS_MATLAB_UTILITIES_LIBRARY "basis.")
set (BASIS_METADATA_LIST "AUTHOR;NAME;SUBPROJECT;PACKAGE;WEBSITE;VENDOR;PROVIDER;PACKAGE_NAME;PACKAGE_VENDOR;PACKAGE_WEBSITE;PACKAGE_LOGO;PROVIDER_NAME;PROVIDER_WEBSITE;PROVIDER_LOGO;DIVISION_NAME;DIVISION_WEBSITE;DIVISION_LOGO;COPYRIGHT;LICENSE;CONTACT;VERSION;TEMPLATE;INCLUDE_DIR;CODE_DIR;MODULES_DIR;CONFIG_DIR;DATA_DIR;DOC_DIR;DOCRES_DIR;EXAMPLE_DIR;LIBRARY_DIR;TESTING_DIR;AUTHORS;DESCRIPTION;DEPENDS;OPTIONAL_DEPENDS;TEST_DEPENDS;OPTIONAL_TEST_DEPENDS;INCLUDE_DIRS;CODE_DIRS;MODULE_DIRS;SUBDIRS")
set (BASIS_METADATA_LIST_MULTI "AUTHORS;DESCRIPTION;DEPENDS;OPTIONAL_DEPENDS;TEST_DEPENDS;OPTIONAL_TEST_DEPENDS;INCLUDE_DIRS;CODE_DIRS;MODULE_DIRS;SUBDIRS")
set (BASIS_METADATA_LIST_SINGLE "AUTHOR;NAME;SUBPROJECT;PACKAGE;WEBSITE;VENDOR;PROVIDER;PACKAGE_NAME;PACKAGE_VENDOR;PACKAGE_WEBSITE;PACKAGE_LOGO;PROVIDER_NAME;PROVIDER_WEBSITE;PROVIDER_LOGO;DIVISION_NAME;DIVISION_WEBSITE;DIVISION_LOGO;COPYRIGHT;LICENSE;CONTACT;VERSION;TEMPLATE;INCLUDE_DIR;CODE_DIR;MODULES_DIR;CONFIG_DIR;DATA_DIR;DOC_DIR;DOCRES_DIR;EXAMPLE_DIR;LIBRARY_DIR;TESTING_DIR")
set (BASIS_MODULE_PATH "/home/filippos/programs/dr/share/dramms/basis/cmake-modules")
set (BASIS_NAMESPACE_DELIMITER_BASH ".")
set (BASIS_NAMESPACE_DELIMITER_CMAKE ".")
set (BASIS_NAMESPACE_DELIMITER_CXX ".")
set (BASIS_NAMESPACE_DELIMITER_JYTHON ".")
set (BASIS_NAMESPACE_DELIMITER_MATLAB ".")
set (BASIS_NAMESPACE_DELIMITER_PERL "::")
set (BASIS_NAMESPACE_DELIMITER_PYTHON ".")
set (BASIS_PERL5LIB "/home/filippos/programs/dr/lib/dramms/perl")
set (BASIS_PERL_EXECUTABLE "/usr/bin/perl")
set (BASIS_PERL_TEMPLATES_DIR "/home/filippos/programs/dr/share/dramms/basis/utilities")
set (BASIS_PERL_UTILITIES_LIBRARY "basis.utilities_perl")
set (BASIS_PROPERTIES_ON_TARGETS "<CONFIG>_OUTPUT_NAME;<CONFIG>_POSTFIX;ARCHIVE_OUTPUT_DIRECTORY;ARCHIVE_OUTPUT_DIRECTORY_<CONFIG>;ARCHIVE_OUTPUT_NAME;ARCHIVE_OUTPUT_NAME_<CONFIG>;AUTOMOC;BUILD_WITH_INSTALL_RPATH;BUNDLE;BUNDLE_EXTENSION;COMPILE_DEFINITIONS;COMPILE_DEFINITIONS_<CONFIG>;COMPILE_FLAGS;DEBUG_POSTFIX;DEFINE_SYMBOL;ENABLE_EXPORTS;EXCLUDE_FROM_ALL;EchoString;FOLDER;FRAMEWORK;Fortran_FORMAT;Fortran_MODULE_DIRECTORY;GENERATOR_FILE_NAME;HAS_CXX;IMPLICIT_DEPENDS_INCLUDE_TRANSFORM;IMPORTED;IMPORTED_CONFIGURATIONS;IMPORTED_IMPLIB;IMPORTED_IMPLIB_<CONFIG>;IMPORTED_LINK_DEPENDENT_LIBRARIES;IMPORTED_LINK_DEPENDENT_LIBRARIES_<CONFIG>;IMPORTED_LINK_INTERFACE_LANGUAGES;IMPORTED_LINK_INTERFACE_LANGUAGES_<CONFIG>;IMPORTED_LINK_INTERFACE_LIBRARIES;IMPORTED_LINK_INTERFACE_LIBRARIES_<CONFIG>;IMPORTED_LINK_INTERFACE_MULTIPLICITY;IMPORTED_LINK_INTERFACE_MULTIPLICITY_<CONFIG>;IMPORTED_LOCATION;IMPORTED_LOCATION_<CONFIG>;IMPORTED_NO_SONAME;IMPORTED_NO_SONAME_<CONFIG>;IMPORTED_SONAME;IMPORTED_SONAME_<CONFIG>;IMPORT_PREFIX;IMPORT_SUFFIX;INSTALL_NAME_DIR;INSTALL_RPATH;INSTALL_RPATH_USE_LINK_PATH;INTERPROCEDURAL_OPTIMIZATION;INTERPROCEDURAL_OPTIMIZATION_<CONFIG>;LABELS;LIBRARY_OUTPUT_DIRECTORY;LIBRARY_OUTPUT_DIRECTORY_<CONFIG>;LIBRARY_OUTPUT_NAME;LIBRARY_OUTPUT_NAME_<CONFIG>;LINKER_LANGUAGE;LINK_DEPENDS;LINK_FLAGS;LINK_FLAGS_<CONFIG>;LINK_INTERFACE_LIBRARIES;LINK_INTERFACE_LIBRARIES_<CONFIG>;LINK_INTERFACE_MULTIPLICITY;LINK_INTERFACE_MULTIPLICITY_<CONFIG>;LINK_SEARCH_END_STATIC;LINK_SEARCH_START_STATIC;LOCATION;LOCATION_<CONFIG>;MACOSX_BUNDLE;MACOSX_BUNDLE_INFO_PLIST;MACOSX_FRAMEWORK_INFO_PLIST;MAP_IMPORTED_CONFIG_<CONFIG>;OSX_ARCHITECTURES;OSX_ARCHITECTURES_<CONFIG>;OUTPUT_NAME;OUTPUT_NAME_<CONFIG>;POST_INSTALL_SCRIPT;PREFIX;PRE_INSTALL_SCRIPT;PRIVATE_HEADER;PROJECT_LABEL;PUBLIC_HEADER;RESOURCE;RULE_LAUNCH_COMPILE;RULE_LAUNCH_CUSTOM;RULE_LAUNCH_LINK;RUNTIME_OUTPUT_DIRECTORY;RUNTIME_OUTPUT_DIRECTORY_<CONFIG>;RUNTIME_OUTPUT_NAME;RUNTIME_OUTPUT_NAME_<CONFIG>;SKIP_BUILD_RPATH;SOURCES;SOVERSION;STATIC_LIBRARY_FLAGS;STATIC_LIBRARY_FLAGS_<CONFIG>;SUFFIX;TYPE;VERSION;VS_GLOBAL_<variable>;VS_KEYWORD;VS_SCC_LOCALPATH;VS_SCC_PROJECTNAME;VS_SCC_PROVIDER;WIN32_EXECUTABLE;XCODE_ATTRIBUTE_<an-attribute>;BASIS_INCLUDE_DIRECTORIES;BASIS_LINK_DIRECTORIES;BASIS_LINK_DEPENDS;BASIS_TYPE;BASIS_UTILITIES;BUNDLED;SCRIPT_DEFINITIONS;SCRIPT_DEFINITIONS_FILE;LANGUAGE;COMPILE;EXPORT;LIBEXEC;TEST;MFILE;COMPONENT;LIBRARY_COMPONENT;RUNTIME_COMPONENT;ARCHIVE_INSTALL_DIRECTORY;LIBRARY_HEADER_DIRECTORY;LIBRARY_INSTALL_DIRECTORY;RUNTIME_INSTALL_DIRECTORY;OUTPUT_DIRECTORY;INSTALL_DIRECTORY;HTML_OUTPUT_DIRECTORY;HTML_INSTALL_DIRECTORY;DIRHTML_OUTPUT_DIRECTORY;DIRHTML_INSTALL_DIRECTORY;SINGLEHTML_OUTPUT_DIRECTORY;SINGLEHTML_INSTALL_DIRECTORY;LINKCHECK_OUTPUT_DIRECTORY;LINKCHECK_INSTALL_DIRECTORY;XML_OUTPUT_DIRECTORY;XML_INSTALL_DIRECTORY;MAN_OUTPUT_DIRECTORY;MAN_INSTALL_DIRECTORY;TEXT_OUTPUT_DIRECTORY;TEXT_INSTALL_DIRECTORY;TEXINFO_OUTPUT_DIRECTORY;TEXINFO_INSTALL_DIRECTORY;LATEX_OUTPUT_DIRECTORY;LATEX_INSTALL_DIRECTORY;PDF_OUTPUT_DIRECTORY;PDF_INSTALL_DIRECTORY;RTF_OUTPUT_DIRECTORY;RTF_INSTALL_DIRECTORY;DOXYFILE;OUTPUT;TAGFILE;BUILD_DIRECTORY;CONFIG_DIRECTORY;BINARY_DIRECTORY;SOURCE_DIRECTORY;BUILDERS")
set (BASIS_PROPERTIES_ON_TARGETS_RE "^([^ ]+_OUTPUT_NAME|[^ ]+_POSTFIX|ARCHIVE_OUTPUT_DIRECTORY|ARCHIVE_OUTPUT_DIRECTORY_[^ ]+|ARCHIVE_OUTPUT_NAME|ARCHIVE_OUTPUT_NAME_[^ ]+|AUTOMOC|BUILD_WITH_INSTALL_RPATH|BUNDLE|BUNDLE_EXTENSION|COMPILE_DEFINITIONS|COMPILE_DEFINITIONS_[^ ]+|COMPILE_FLAGS|DEBUG_POSTFIX|DEFINE_SYMBOL|ENABLE_EXPORTS|EXCLUDE_FROM_ALL|EchoString|FOLDER|FRAMEWORK|Fortran_FORMAT|Fortran_MODULE_DIRECTORY|GENERATOR_FILE_NAME|HAS_CXX|IMPLICIT_DEPENDS_INCLUDE_TRANSFORM|IMPORTED|IMPORTED_CONFIGURATIONS|IMPORTED_IMPLIB|IMPORTED_IMPLIB_[^ ]+|IMPORTED_LINK_DEPENDENT_LIBRARIES|IMPORTED_LINK_DEPENDENT_LIBRARIES_[^ ]+|IMPORTED_LINK_INTERFACE_LANGUAGES|IMPORTED_LINK_INTERFACE_LANGUAGES_[^ ]+|IMPORTED_LINK_INTERFACE_LIBRARIES|IMPORTED_LINK_INTERFACE_LIBRARIES_[^ ]+|IMPORTED_LINK_INTERFACE_MULTIPLICITY|IMPORTED_LINK_INTERFACE_MULTIPLICITY_[^ ]+|IMPORTED_LOCATION|IMPORTED_LOCATION_[^ ]+|IMPORTED_NO_SONAME|IMPORTED_NO_SONAME_[^ ]+|IMPORTED_SONAME|IMPORTED_SONAME_[^ ]+|IMPORT_PREFIX|IMPORT_SUFFIX|INSTALL_NAME_DIR|INSTALL_RPATH|INSTALL_RPATH_USE_LINK_PATH|INTERPROCEDURAL_OPTIMIZATION|INTERPROCEDURAL_OPTIMIZATION_[^ ]+|LABELS|LIBRARY_OUTPUT_DIRECTORY|LIBRARY_OUTPUT_DIRECTORY_[^ ]+|LIBRARY_OUTPUT_NAME|LIBRARY_OUTPUT_NAME_[^ ]+|LINKER_LANGUAGE|LINK_DEPENDS|LINK_FLAGS|LINK_FLAGS_[^ ]+|LINK_INTERFACE_LIBRARIES|LINK_INTERFACE_LIBRARIES_[^ ]+|LINK_INTERFACE_MULTIPLICITY|LINK_INTERFACE_MULTIPLICITY_[^ ]+|LINK_SEARCH_END_STATIC|LINK_SEARCH_START_STATIC|LOCATION|LOCATION_[^ ]+|MACOSX_BUNDLE|MACOSX_BUNDLE_INFO_PLIST|MACOSX_FRAMEWORK_INFO_PLIST|MAP_IMPORTED_CONFIG_[^ ]+|OSX_ARCHITECTURES|OSX_ARCHITECTURES_[^ ]+|OUTPUT_NAME|OUTPUT_NAME_[^ ]+|POST_INSTALL_SCRIPT|PREFIX|PRE_INSTALL_SCRIPT|PRIVATE_HEADER|PROJECT_LABEL|PUBLIC_HEADER|RESOURCE|RULE_LAUNCH_COMPILE|RULE_LAUNCH_CUSTOM|RULE_LAUNCH_LINK|RUNTIME_OUTPUT_DIRECTORY|RUNTIME_OUTPUT_DIRECTORY_[^ ]+|RUNTIME_OUTPUT_NAME|RUNTIME_OUTPUT_NAME_[^ ]+|SKIP_BUILD_RPATH|SOURCES|SOVERSION|STATIC_LIBRARY_FLAGS|STATIC_LIBRARY_FLAGS_[^ ]+|SUFFIX|TYPE|VERSION|VS_GLOBAL_[^ ]+|VS_KEYWORD|VS_SCC_LOCALPATH|VS_SCC_PROJECTNAME|VS_SCC_PROVIDER|WIN32_EXECUTABLE|XCODE_ATTRIBUTE_[^ ]+|BASIS_INCLUDE_DIRECTORIES|BASIS_LINK_DIRECTORIES|BASIS_LINK_DEPENDS|BASIS_TYPE|BASIS_UTILITIES|BUNDLED|SCRIPT_DEFINITIONS|SCRIPT_DEFINITIONS_FILE|LANGUAGE|COMPILE|EXPORT|LIBEXEC|TEST|MFILE|COMPONENT|LIBRARY_COMPONENT|RUNTIME_COMPONENT|ARCHIVE_INSTALL_DIRECTORY|LIBRARY_HEADER_DIRECTORY|LIBRARY_INSTALL_DIRECTORY|RUNTIME_INSTALL_DIRECTORY|OUTPUT_DIRECTORY|INSTALL_DIRECTORY|HTML_OUTPUT_DIRECTORY|HTML_INSTALL_DIRECTORY|DIRHTML_OUTPUT_DIRECTORY|DIRHTML_INSTALL_DIRECTORY|SINGLEHTML_OUTPUT_DIRECTORY|SINGLEHTML_INSTALL_DIRECTORY|LINKCHECK_OUTPUT_DIRECTORY|LINKCHECK_INSTALL_DIRECTORY|XML_OUTPUT_DIRECTORY|XML_INSTALL_DIRECTORY|MAN_OUTPUT_DIRECTORY|MAN_INSTALL_DIRECTORY|TEXT_OUTPUT_DIRECTORY|TEXT_INSTALL_DIRECTORY|TEXINFO_OUTPUT_DIRECTORY|TEXINFO_INSTALL_DIRECTORY|LATEX_OUTPUT_DIRECTORY|LATEX_INSTALL_DIRECTORY|PDF_OUTPUT_DIRECTORY|PDF_INSTALL_DIRECTORY|RTF_OUTPUT_DIRECTORY|RTF_INSTALL_DIRECTORY|DOXYFILE|OUTPUT|TAGFILE|BUILD_DIRECTORY|CONFIG_DIRECTORY|BINARY_DIRECTORY|SOURCE_DIRECTORY|BUILDERS)$")
set (BASIS_PROPERTIES_ON_TESTS "ATTACHED_FILES;ATTACHED_FILES_ON_FAIL;COST;DEPENDS;ENVIRONMENT;FAIL_REGULAR_EXPRESSION;LABELS;MEASUREMENT;PASS_REGULAR_EXPRESSION;PROCESSORS;REQUIRED_FILES;RESOURCE_LOCK;RUN_SERIAL;TIMEOUT;WILL_FAIL;WORKING_DIRECTORY")
set (BASIS_PROPERTIES_ON_TESTS_RE "^(ATTACHED_FILES|ATTACHED_FILES_ON_FAIL|COST|DEPENDS|ENVIRONMENT|FAIL_REGULAR_EXPRESSION|LABELS|MEASUREMENT|PASS_REGULAR_EXPRESSION|PROCESSORS|REQUIRED_FILES|RESOURCE_LOCK|RUN_SERIAL|TIMEOUT|WILL_FAIL|WORKING_DIRECTORY)$")
set (BASIS_PYTHONPATH "/home/filippos/programs/dr/lib/dramms/python")
set (BASIS_PYTHON_TEMPLATES_DIR "/home/filippos/programs/dr/share/dramms/basis/utilities")
set (BASIS_PYTHON_UTILITIES_LIBRARY "basis.utilities_python")
set (BASIS_REGISTER "ON")
set (BASIS_RESERVED_TARGET_NAMES "all;bundle;bundle_source;changelog;clean;depend;doc;headers;headers_check;package;package_source;scripts;test;uninstall")
set (BASIS_REVISION_INFO "ON")
set (BASIS_RUNTIME_COMPONENT "Runtime")
set (BASIS_SCRIPT_CONFIG_FILE "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/config/BasisScriptConfig.cmake")
set (BASIS_SCRIPT_EXECUTE_PROCESS "/home/filippos/programs/dr/share/dramms/basis/cmake-modules/ExecuteProcess.cmake")
set (BASIS_SET_TARGET_PROPERTIES_IMPORT "FALSE")
set (BASIS_SLICER_METADATA_LIST "HOMEPAGE;ICONURL;STATUS;SCREENSHOTURLS;ACKNOWLEDGEMENTS;CATEGORY;CONTRIBUTORS;LICENSE_SHORT_DESCRIPTION")
set (BASIS_SLICER_METADATA_LIST_MULTI "ACKNOWLEDGEMENTS;CATEGORY;CONTRIBUTORS;LICENSE_SHORT_DESCRIPTION")
set (BASIS_SLICER_METADATA_LIST_SINGLE "HOMEPAGE;ICONURL;STATUS;SCREENSHOTURLS")
set (BASIS_SPHINX_CONFIG "/home/filippos/programs/dr/share/dramms/basis/cmake-modules/sphinx_conf.py.in")
set (BASIS_SPHINX_EXTENSIONS_PATH "/home/filippos/programs/dr/lib/dramms/python/basis/sphinx/ext/")
set (BASIS_SPHINX_HTML_THEME "default")
set (BASIS_SPHINX_HTML_THEME_OPTIONS "PROJECT_LOGO;None;SHOW_SBIA_LOGO;false;SHOW_PENN_LOGO;false")
set (BASIS_SPHINX_HTML_THEME_PATH "/home/filippos/programs/dr/share/dramms/basis/sphinx-themes")
set (BASIS_SUPERBUILD_MODULES "OFF")
set (BASIS_SUPPORT_SLICER_MODULES "FALSE")
set (BASIS_SVN_USERS_FILE "/home/filippos/programs/dr/share/dramms/basis/cmake-modules/SubversionUsers.txt")
set (BASIS_TEMPLATE_DIR "/home/filippos/programs/dr/share/dramms/basis/templates-")
set (BASIS_TEST_LIBRARY "basis.testlib")
set (BASIS_TEST_MAIN_LIBRARY "basis.testmain")
set (BASIS_USE_BASIS_INCLUDED "TRUE")
set (BASIS_USE_DRAMMSFastPD_INCLUDED "TRUE")
set (BASIS_USE_FILE "/home/filippos/programs/dr/lib/cmake/dramms/BASISUse.cmake")
set (BASIS_USE_FULLY_QUALIFIED_UIDS "OFF")
set (BASIS_USE_NiftiCLib_INCLUDED "TRUE")
set (BASIS_USE_TARGET_UIDS "OFF")
set (BASIS_UTILITIES "FALSE")
set (BASIS_UTILITIES_ENABLED "CXX;BASH")
set (BASIS_VERBOSE "OFF")
set (BASIS_VERSION "3.2.0")
set (BASIS_VERSION_COUNT "3")
set (BASIS_VERSION_MAJOR "3")
set (BASIS_VERSION_MINOR "2")
set (BASIS_VERSION_PATCH "0")
set (BASIS_VERSION_TWEAK "0")
set (BASIS_basis_project_CALLED "TRUE")
set (BINARY_ARCHIVE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib")
set (BINARY_BASH_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/bash")
set (BINARY_CODE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src")
set (BINARY_CONFIG_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/config")
set (BINARY_DATA_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/data")
set (BINARY_DOC_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/doc")
set (BINARY_EXAMPLE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/example")
set (BINARY_INCLUDE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/include")
set (BINARY_JYTHON_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/jython")
set (BINARY_LIBEXEC_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib")
set (BINARY_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib")
set (BINARY_MATLAB_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/matlab")
set (BINARY_MODULES_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/modules")
set (BINARY_PERL_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/perl")
set (BINARY_PYTHON_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/python")
set (BINARY_RUNTIME_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/bin")
set (BINARY_TESTING_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/test")
set (BUILD_BASIS_UTILITIES_FOR_BASH "OFF")
set (BUILD_BASIS_UTILITIES_FOR_CXX "OFF")
set (BUILD_BASIS_UTILITIES_FOR_PERL "OFF")
set (BUILD_BASIS_UTILITIES_FOR_PYTHON "OFF")
set (BUILD_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/CMakeFiles/dramms")
set (BUILD_DOCUMENTATION "OFF")
set (BUILD_TESTING "OFF")
set (BUNDLE_NAME "DRAMMS")
set (BUNDLE_PROJECT "FALSE")
set (BUNDLE_PROJECTS "BASIS;NiftiCLib;FastPD")
set (CMAKE_AR "/usr/bin/ar")
set (CMAKE_AR "/usr/bin/ar")
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib")
set (CMAKE_BASE_NAME "g++")
set (CMAKE_BINARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build")
set (CMAKE_BUILD_TOOL "/usr/bin/make")
set (CMAKE_BUILD_TYPE "Release")
set (CMAKE_BUILD_WITH_INSTALL_RPATH "FALSE")
set (CMAKE_C11_COMPILE_FEATURES "c_std_11;c_static_assert")
set (CMAKE_C11_EXTENSION_COMPILE_OPTION "-std=gnu11")
set (CMAKE_C11_STANDARD_COMPILE_OPTION "-std=c11")
set (CMAKE_C90_COMPILE_FEATURES "c_std_90;c_function_prototypes")
set (CMAKE_C90_EXTENSION_COMPILE_OPTION "-std=gnu90")
set (CMAKE_C90_STANDARD_COMPILE_OPTION "-std=c90")
set (CMAKE_C99_COMPILE_FEATURES "c_std_99;c_restrict;c_variadic_macros")
set (CMAKE_C99_EXTENSION_COMPILE_OPTION "-std=gnu99")
set (CMAKE_C99_STANDARD_COMPILE_OPTION "-std=c99")
set (CMAKE_CFG_INTDIR ".")
set (CMAKE_COLOR_MAKEFILE "ON")
set (CMAKE_COMMAND "/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake")
set (CMAKE_COMPILER_IS_GNUCC "1")
set (CMAKE_COMPILER_IS_GNUCXX "1")
set (CMAKE_CPACK_COMMAND "/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cpack")
set (CMAKE_CROSSCOMPILING "FALSE")
set (CMAKE_CTEST_COMMAND "/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/ctest")
set (CMAKE_CURRENT_BINARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src")
set (CMAKE_CURRENT_LIST_DIR "/home/filippos/programs/dramms-1.5.1-source/src")
set (CMAKE_CURRENT_LIST_FILE "/home/filippos/programs/dramms-1.5.1-source/src/CMakeLists.txt")
set (CMAKE_CURRENT_SOURCE_DIR "/home/filippos/programs/dramms-1.5.1-source/src")
set (CMAKE_CXX11_COMPILE_FEATURES "cxx_std_11;cxx_alias_templates;cxx_alignas;cxx_alignof;cxx_attributes;cxx_auto_type;cxx_constexpr;cxx_decltype;cxx_decltype_incomplete_return_types;cxx_default_function_template_args;cxx_defaulted_functions;cxx_defaulted_move_initializers;cxx_delegating_constructors;cxx_deleted_functions;cxx_enum_forward_declarations;cxx_explicit_conversions;cxx_extended_friend_declarations;cxx_extern_templates;cxx_final;cxx_func_identifier;cxx_generalized_initializers;cxx_inheriting_constructors;cxx_inline_namespaces;cxx_lambdas;cxx_local_type_template_args;cxx_long_long_type;cxx_noexcept;cxx_nonstatic_member_init;cxx_nullptr;cxx_override;cxx_range_for;cxx_raw_string_literals;cxx_reference_qualified_functions;cxx_right_angle_brackets;cxx_rvalue_references;cxx_sizeof_member;cxx_static_assert;cxx_strong_enums;cxx_thread_local;cxx_trailing_return_types;cxx_unicode_literals;cxx_uniform_initialization;cxx_unrestricted_unions;cxx_user_literals;cxx_variadic_macros;cxx_variadic_templates")
set (CMAKE_CXX11_EXTENSION_COMPILE_OPTION "-std=gnu++11")
set (CMAKE_CXX11_STANDARD_COMPILE_OPTION "-std=c++11")
set (CMAKE_CXX14_COMPILE_FEATURES "cxx_std_14;cxx_aggregate_default_initializers;cxx_attribute_deprecated;cxx_binary_literals;cxx_contextual_conversions;cxx_decltype_auto;cxx_digit_separators;cxx_generic_lambdas;cxx_lambda_init_captures;cxx_relaxed_constexpr;cxx_return_type_deduction;cxx_variable_templates")
set (CMAKE_CXX14_EXTENSION_COMPILE_OPTION "-std=gnu++14")
set (CMAKE_CXX14_STANDARD_COMPILE_OPTION "-std=c++14")
set (CMAKE_CXX17_COMPILE_FEATURES "cxx_std_17")
set (CMAKE_CXX17_EXTENSION_COMPILE_OPTION "-std=gnu++1z")
set (CMAKE_CXX17_STANDARD_COMPILE_OPTION "-std=c++1z")
set (CMAKE_CXX98_COMPILE_FEATURES "cxx_std_98;cxx_template_template_parameters")
set (CMAKE_CXX98_EXTENSION_COMPILE_OPTION "-std=gnu++98")
set (CMAKE_CXX98_STANDARD_COMPILE_OPTION "-std=c++98")
set (CMAKE_CXX_ABI_COMPILED "TRUE")
set (CMAKE_CXX_ARCHIVE_APPEND "<CMAKE_AR> q  <TARGET> <LINK_FLAGS> <OBJECTS>")
set (CMAKE_CXX_ARCHIVE_CREATE "<CMAKE_AR> qc <TARGET> <LINK_FLAGS> <OBJECTS>")
set (CMAKE_CXX_ARCHIVE_FINISH "<CMAKE_RANLIB> <TARGET>")
set (CMAKE_CXX_CL_SHOWINCLUDES_PREFIX "")
set (CMAKE_CXX_COMPILER "/usr/bin/c++")
set (CMAKE_CXX_COMPILER "/usr/bin/c++")
set (CMAKE_CXX_COMPILER_ABI "ELF")
set (CMAKE_CXX_COMPILER_ARG1 "")
set (CMAKE_CXX_COMPILER_ENV_VAR "CXX")
set (CMAKE_CXX_COMPILER_ID "GNU")
set (CMAKE_CXX_COMPILER_ID_PLATFORM_CONTENT "#define STRINGIFY_HELPER(X) #X
#define STRINGIFY(X) STRINGIFY_HELPER(X)

/* Identify known platforms by name.  */
#if defined(__linux) || defined(__linux__) || defined(linux)
# define PLATFORM_ID \"Linux\"

#elif defined(__CYGWIN__)
# define PLATFORM_ID \"Cygwin\"

#elif defined(__MINGW32__)
# define PLATFORM_ID \"MinGW\"

#elif defined(__APPLE__)
# define PLATFORM_ID \"Darwin\"

#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
# define PLATFORM_ID \"Windows\"

#elif defined(__FreeBSD__) || defined(__FreeBSD)
# define PLATFORM_ID \"FreeBSD\"

#elif defined(__NetBSD__) || defined(__NetBSD)
# define PLATFORM_ID \"NetBSD\"

#elif defined(__OpenBSD__) || defined(__OPENBSD)
# define PLATFORM_ID \"OpenBSD\"

#elif defined(__sun) || defined(sun)
# define PLATFORM_ID \"SunOS\"

#elif defined(_AIX) || defined(__AIX) || defined(__AIX__) || defined(__aix) || defined(__aix__)
# define PLATFORM_ID \"AIX\"

#elif defined(__sgi) || defined(__sgi__) || defined(_SGI)
# define PLATFORM_ID \"IRIX\"

#elif defined(__hpux) || defined(__hpux__)
# define PLATFORM_ID \"HP-UX\"

#elif defined(__HAIKU__)
# define PLATFORM_ID \"Haiku\"

#elif defined(__BeOS) || defined(__BEOS__) || defined(_BEOS)
# define PLATFORM_ID \"BeOS\"

#elif defined(__QNX__) || defined(__QNXNTO__)
# define PLATFORM_ID \"QNX\"

#elif defined(__tru64) || defined(_tru64) || defined(__TRU64__)
# define PLATFORM_ID \"Tru64\"

#elif defined(__riscos) || defined(__riscos__)
# define PLATFORM_ID \"RISCos\"

#elif defined(__sinix) || defined(__sinix__) || defined(__SINIX__)
# define PLATFORM_ID \"SINIX\"

#elif defined(__UNIX_SV__)
# define PLATFORM_ID \"UNIX_SV\"

#elif defined(__bsdos__)
# define PLATFORM_ID \"BSDOS\"

#elif defined(_MPRAS) || defined(MPRAS)
# define PLATFORM_ID \"MP-RAS\"

#elif defined(__osf) || defined(__osf__)
# define PLATFORM_ID \"OSF1\"

#elif defined(_SCO_SV) || defined(SCO_SV) || defined(sco_sv)
# define PLATFORM_ID \"SCO_SV\"

#elif defined(__ultrix) || defined(__ultrix__) || defined(_ULTRIX)
# define PLATFORM_ID \"ULTRIX\"

#elif defined(__XENIX__) || defined(_XENIX) || defined(XENIX)
# define PLATFORM_ID \"Xenix\"

#elif defined(__WATCOMC__)
# if defined(__LINUX__)
#  define PLATFORM_ID \"Linux\"

# elif defined(__DOS__)
#  define PLATFORM_ID \"DOS\"

# elif defined(__OS2__)
#  define PLATFORM_ID \"OS2\"

# elif defined(__WINDOWS__)
#  define PLATFORM_ID \"Windows3x\"

# else /* unknown platform */
#  define PLATFORM_ID
# endif

#else /* unknown platform */
# define PLATFORM_ID

#endif

/* For windows compilers MSVC and Intel we can determine
   the architecture of the compiler being used.  This is because
   the compilers do not have flags that can change the architecture,
   but rather depend on which compiler is being used
*/
#if defined(_WIN32) && defined(_MSC_VER)
# if defined(_M_IA64)
#  define ARCHITECTURE_ID \"IA64\"

# elif defined(_M_X64) || defined(_M_AMD64)
#  define ARCHITECTURE_ID \"x64\"

# elif defined(_M_IX86)
#  define ARCHITECTURE_ID \"X86\"

# elif defined(_M_ARM)
#  if _M_ARM == 4
#   define ARCHITECTURE_ID \"ARMV4I\"
#  elif _M_ARM == 5
#   define ARCHITECTURE_ID \"ARMV5I\"
#  else
#   define ARCHITECTURE_ID \"ARMV\" STRINGIFY(_M_ARM)
#  endif

# elif defined(_M_MIPS)
#  define ARCHITECTURE_ID \"MIPS\"

# elif defined(_M_SH)
#  define ARCHITECTURE_ID \"SHx\"

# else /* unknown architecture */
#  define ARCHITECTURE_ID \"\"
# endif

#elif defined(__WATCOMC__)
# if defined(_M_I86)
#  define ARCHITECTURE_ID \"I86\"

# elif defined(_M_IX86)
#  define ARCHITECTURE_ID \"X86\"

# else /* unknown architecture */
#  define ARCHITECTURE_ID \"\"
# endif

#else
#  define ARCHITECTURE_ID
#endif

/* Convert integer to decimal digit literals.  */
#define DEC(n)                   \\
  ('0' + (((n) / 10000000)%10)), \\
  ('0' + (((n) / 1000000)%10)),  \\
  ('0' + (((n) / 100000)%10)),   \\
  ('0' + (((n) / 10000)%10)),    \\
  ('0' + (((n) / 1000)%10)),     \\
  ('0' + (((n) / 100)%10)),      \\
  ('0' + (((n) / 10)%10)),       \\
  ('0' +  ((n) % 10))

/* Convert integer to hex digit literals.  */
#define HEX(n)             \\
  ('0' + ((n)>>28 & 0xF)), \\
  ('0' + ((n)>>24 & 0xF)), \\
  ('0' + ((n)>>20 & 0xF)), \\
  ('0' + ((n)>>16 & 0xF)), \\
  ('0' + ((n)>>12 & 0xF)), \\
  ('0' + ((n)>>8  & 0xF)), \\
  ('0' + ((n)>>4  & 0xF)), \\
  ('0' + ((n)     & 0xF))

/* Construct a string literal encoding the version number components. */
#ifdef COMPILER_VERSION_MAJOR
char const info_version[] = {
  'I', 'N', 'F', 'O', ':',
  'c','o','m','p','i','l','e','r','_','v','e','r','s','i','o','n','[',
  COMPILER_VERSION_MAJOR,
# ifdef COMPILER_VERSION_MINOR
  '.', COMPILER_VERSION_MINOR,
#  ifdef COMPILER_VERSION_PATCH
   '.', COMPILER_VERSION_PATCH,
#   ifdef COMPILER_VERSION_TWEAK
    '.', COMPILER_VERSION_TWEAK,
#   endif
#  endif
# endif
  ']','\\0'};
#endif

/* Construct a string literal encoding the version number components. */
#ifdef SIMULATE_VERSION_MAJOR
char const info_simulate_version[] = {
  'I', 'N', 'F', 'O', ':',
  's','i','m','u','l','a','t','e','_','v','e','r','s','i','o','n','[',
  SIMULATE_VERSION_MAJOR,
# ifdef SIMULATE_VERSION_MINOR
  '.', SIMULATE_VERSION_MINOR,
#  ifdef SIMULATE_VERSION_PATCH
   '.', SIMULATE_VERSION_PATCH,
#   ifdef SIMULATE_VERSION_TWEAK
    '.', SIMULATE_VERSION_TWEAK,
#   endif
#  endif
# endif
  ']','\\0'};
#endif

/* Construct the string literal in pieces to prevent the source from
   getting matched.  Store it in a pointer rather than an array
   because some compilers will just produce instructions to fill the
   array rather than assigning a pointer to a static array.  */
char const* info_platform = \"INFO\" \":\" \"platform[\" PLATFORM_ID \"]\";
char const* info_arch = \"INFO\" \":\" \"arch[\" ARCHITECTURE_ID \"]\";

")
set (CMAKE_CXX_COMPILER_ID_RUN "1")
set (CMAKE_CXX_COMPILER_ID_TEST_FLAGS "-c")
set (CMAKE_CXX_COMPILER_ID_TOOL_MATCH_INDEX "2")
set (CMAKE_CXX_COMPILER_ID_TOOL_MATCH_REGEX "
Ld[^
]*(
[ 	]+[^
]*)*
[ 	]+([^ 	
]+)[^
]*-o[^
]*CompilerIdCXX/(\\./)?(CompilerIdCXX.xctest/)?CompilerIdCXX[ 	
\\\"]")
set (CMAKE_CXX_COMPILER_ID_VENDORS "IAR")
set (CMAKE_CXX_COMPILER_ID_VENDOR_REGEX_IAR "IAR .+ Compiler")
set (CMAKE_CXX_COMPILER_INIT "NOTFOUND")
set (CMAKE_CXX_COMPILER_LIST "c++;CC;g++;aCC;cl;bcc;xlC;clang++")
set (CMAKE_CXX_COMPILER_LOADED "1")
set (CMAKE_CXX_COMPILER_NAMES "c++")
set (CMAKE_CXX_COMPILER_PRODUCED_FILES "a.out")
set (CMAKE_CXX_COMPILER_PRODUCED_OUTPUT "")
set (CMAKE_CXX_COMPILER_VERSION "7.3.0")
set (CMAKE_CXX_COMPILER_WORKS "TRUE")
set (CMAKE_CXX_COMPILER_WRAPPER "")
set (CMAKE_CXX_COMPILE_FEATURES "cxx_std_98;cxx_template_template_parameters;cxx_std_11;cxx_alias_templates;cxx_alignas;cxx_alignof;cxx_attributes;cxx_auto_type;cxx_constexpr;cxx_decltype;cxx_decltype_incomplete_return_types;cxx_default_function_template_args;cxx_defaulted_functions;cxx_defaulted_move_initializers;cxx_delegating_constructors;cxx_deleted_functions;cxx_enum_forward_declarations;cxx_explicit_conversions;cxx_extended_friend_declarations;cxx_extern_templates;cxx_final;cxx_func_identifier;cxx_generalized_initializers;cxx_inheriting_constructors;cxx_inline_namespaces;cxx_lambdas;cxx_local_type_template_args;cxx_long_long_type;cxx_noexcept;cxx_nonstatic_member_init;cxx_nullptr;cxx_override;cxx_range_for;cxx_raw_string_literals;cxx_reference_qualified_functions;cxx_right_angle_brackets;cxx_rvalue_references;cxx_sizeof_member;cxx_static_assert;cxx_strong_enums;cxx_thread_local;cxx_trailing_return_types;cxx_unicode_literals;cxx_uniform_initialization;cxx_unrestricted_unions;cxx_user_literals;cxx_variadic_macros;cxx_variadic_templates;cxx_std_14;cxx_aggregate_default_initializers;cxx_attribute_deprecated;cxx_binary_literals;cxx_contextual_conversions;cxx_decltype_auto;cxx_digit_separators;cxx_generic_lambdas;cxx_lambda_init_captures;cxx_relaxed_constexpr;cxx_return_type_deduction;cxx_variable_templates;cxx_std_17")
set (CMAKE_CXX_COMPILE_OBJECT "<CMAKE_CXX_COMPILER>  <DEFINES> <INCLUDES> <FLAGS> -o <OBJECT> -c <SOURCE>")
set (CMAKE_CXX_COMPILE_OPTIONS_PIC "-fPIC")
set (CMAKE_CXX_COMPILE_OPTIONS_PIE "-fPIE")
set (CMAKE_CXX_COMPILE_OPTIONS_SYSROOT "--sysroot=")
set (CMAKE_CXX_COMPILE_OPTIONS_VISIBILITY "-fvisibility=")
set (CMAKE_CXX_COMPILE_OPTIONS_VISIBILITY_INLINES_HIDDEN "-fvisibility-inlines-hidden")
set (CMAKE_CXX_CREATE_ASSEMBLY_SOURCE "<CMAKE_CXX_COMPILER> <DEFINES> <INCLUDES> <FLAGS> -S <SOURCE> -o <ASSEMBLY_SOURCE>")
set (CMAKE_CXX_CREATE_PREPROCESSED_SOURCE "<CMAKE_CXX_COMPILER> <DEFINES> <INCLUDES> <FLAGS> -E <SOURCE> > <PREPROCESSED_SOURCE>")
set (CMAKE_CXX_CREATE_SHARED_LIBRARY "<CMAKE_CXX_COMPILER> <CMAKE_SHARED_LIBRARY_CXX_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>")
set (CMAKE_CXX_CREATE_SHARED_MODULE "<CMAKE_CXX_COMPILER> <CMAKE_SHARED_LIBRARY_CXX_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>")
set (CMAKE_CXX_FLAGS "")
set (CMAKE_CXX_FLAGS_DEBUG "-g")
set (CMAKE_CXX_FLAGS_DEBUG_INIT "-g")
set (CMAKE_CXX_FLAGS_INIT "")
set (CMAKE_CXX_FLAGS_MINSIZEREL "-Os -DNDEBUG")
set (CMAKE_CXX_FLAGS_MINSIZEREL_INIT "-Os -DNDEBUG")
set (CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG")
set (CMAKE_CXX_FLAGS_RELEASE_INIT "-O3 -DNDEBUG")
set (CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g -DNDEBUG")
set (CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT "-O2 -g -DNDEBUG")
set (CMAKE_CXX_IGNORE_EXTENSIONS "inl;h;hpp;HPP;H;o;O;obj;OBJ;def;DEF;rc;RC")
set (CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "/usr/include")
set (CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES "/usr/lib/gcc/x86_64-linux-gnu/7;/usr/lib/x86_64-linux-gnu;/usr/lib;/lib/x86_64-linux-gnu;/lib")
set (CMAKE_CXX_IMPLICIT_LINK_FRAMEWORK_DIRECTORIES "")
set (CMAKE_CXX_IMPLICIT_LINK_LIBRARIES "stdc++;m;c")
set (CMAKE_CXX_INFORMATION_LOADED "1")
set (CMAKE_CXX_LIBRARY_ARCHITECTURE "x86_64-linux-gnu")
set (CMAKE_CXX_LINKER_PREFERENCE "30")
set (CMAKE_CXX_LINKER_PREFERENCE_PROPAGATES "1")
set (CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER>  <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>")
set (CMAKE_CXX_OUTPUT_EXTENSION ".o")
set (CMAKE_CXX_PLATFORM_ID "Linux")
set (CMAKE_CXX_SIMULATE_ID "")
set (CMAKE_CXX_SIMULATE_VERSION "")
set (CMAKE_CXX_SIZEOF_DATA_PTR "8")
set (CMAKE_CXX_SOURCE_FILE_EXTENSIONS "C;M;c++;cc;cpp;cxx;mm;CPP")
set (CMAKE_CXX_STANDARD_COMPUTED_DEFAULT "14")
set (CMAKE_CXX_STANDARD_DEFAULT "14")
set (CMAKE_CXX_VERBOSE_FLAG "-v")
set (CMAKE_C_ABI_COMPILED "TRUE")
set (CMAKE_C_ARCHIVE_APPEND "<CMAKE_AR> q  <TARGET> <LINK_FLAGS> <OBJECTS>")
set (CMAKE_C_ARCHIVE_CREATE "<CMAKE_AR> qc <TARGET> <LINK_FLAGS> <OBJECTS>")
set (CMAKE_C_ARCHIVE_FINISH "<CMAKE_RANLIB> <TARGET>")
set (CMAKE_C_CL_SHOWINCLUDES_PREFIX "")
set (CMAKE_C_COMPILER "/usr/bin/cc")
set (CMAKE_C_COMPILER "/usr/bin/cc")
set (CMAKE_C_COMPILER_ABI "ELF")
set (CMAKE_C_COMPILER_ARG1 "")
set (CMAKE_C_COMPILER_ENV_VAR "CC")
set (CMAKE_C_COMPILER_ID "GNU")
set (CMAKE_C_COMPILER_ID_PLATFORM_CONTENT "#define STRINGIFY_HELPER(X) #X
#define STRINGIFY(X) STRINGIFY_HELPER(X)

/* Identify known platforms by name.  */
#if defined(__linux) || defined(__linux__) || defined(linux)
# define PLATFORM_ID \"Linux\"

#elif defined(__CYGWIN__)
# define PLATFORM_ID \"Cygwin\"

#elif defined(__MINGW32__)
# define PLATFORM_ID \"MinGW\"

#elif defined(__APPLE__)
# define PLATFORM_ID \"Darwin\"

#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
# define PLATFORM_ID \"Windows\"

#elif defined(__FreeBSD__) || defined(__FreeBSD)
# define PLATFORM_ID \"FreeBSD\"

#elif defined(__NetBSD__) || defined(__NetBSD)
# define PLATFORM_ID \"NetBSD\"

#elif defined(__OpenBSD__) || defined(__OPENBSD)
# define PLATFORM_ID \"OpenBSD\"

#elif defined(__sun) || defined(sun)
# define PLATFORM_ID \"SunOS\"

#elif defined(_AIX) || defined(__AIX) || defined(__AIX__) || defined(__aix) || defined(__aix__)
# define PLATFORM_ID \"AIX\"

#elif defined(__sgi) || defined(__sgi__) || defined(_SGI)
# define PLATFORM_ID \"IRIX\"

#elif defined(__hpux) || defined(__hpux__)
# define PLATFORM_ID \"HP-UX\"

#elif defined(__HAIKU__)
# define PLATFORM_ID \"Haiku\"

#elif defined(__BeOS) || defined(__BEOS__) || defined(_BEOS)
# define PLATFORM_ID \"BeOS\"

#elif defined(__QNX__) || defined(__QNXNTO__)
# define PLATFORM_ID \"QNX\"

#elif defined(__tru64) || defined(_tru64) || defined(__TRU64__)
# define PLATFORM_ID \"Tru64\"

#elif defined(__riscos) || defined(__riscos__)
# define PLATFORM_ID \"RISCos\"

#elif defined(__sinix) || defined(__sinix__) || defined(__SINIX__)
# define PLATFORM_ID \"SINIX\"

#elif defined(__UNIX_SV__)
# define PLATFORM_ID \"UNIX_SV\"

#elif defined(__bsdos__)
# define PLATFORM_ID \"BSDOS\"

#elif defined(_MPRAS) || defined(MPRAS)
# define PLATFORM_ID \"MP-RAS\"

#elif defined(__osf) || defined(__osf__)
# define PLATFORM_ID \"OSF1\"

#elif defined(_SCO_SV) || defined(SCO_SV) || defined(sco_sv)
# define PLATFORM_ID \"SCO_SV\"

#elif defined(__ultrix) || defined(__ultrix__) || defined(_ULTRIX)
# define PLATFORM_ID \"ULTRIX\"

#elif defined(__XENIX__) || defined(_XENIX) || defined(XENIX)
# define PLATFORM_ID \"Xenix\"

#elif defined(__WATCOMC__)
# if defined(__LINUX__)
#  define PLATFORM_ID \"Linux\"

# elif defined(__DOS__)
#  define PLATFORM_ID \"DOS\"

# elif defined(__OS2__)
#  define PLATFORM_ID \"OS2\"

# elif defined(__WINDOWS__)
#  define PLATFORM_ID \"Windows3x\"

# else /* unknown platform */
#  define PLATFORM_ID
# endif

#else /* unknown platform */
# define PLATFORM_ID

#endif

/* For windows compilers MSVC and Intel we can determine
   the architecture of the compiler being used.  This is because
   the compilers do not have flags that can change the architecture,
   but rather depend on which compiler is being used
*/
#if defined(_WIN32) && defined(_MSC_VER)
# if defined(_M_IA64)
#  define ARCHITECTURE_ID \"IA64\"

# elif defined(_M_X64) || defined(_M_AMD64)
#  define ARCHITECTURE_ID \"x64\"

# elif defined(_M_IX86)
#  define ARCHITECTURE_ID \"X86\"

# elif defined(_M_ARM)
#  if _M_ARM == 4
#   define ARCHITECTURE_ID \"ARMV4I\"
#  elif _M_ARM == 5
#   define ARCHITECTURE_ID \"ARMV5I\"
#  else
#   define ARCHITECTURE_ID \"ARMV\" STRINGIFY(_M_ARM)
#  endif

# elif defined(_M_MIPS)
#  define ARCHITECTURE_ID \"MIPS\"

# elif defined(_M_SH)
#  define ARCHITECTURE_ID \"SHx\"

# else /* unknown architecture */
#  define ARCHITECTURE_ID \"\"
# endif

#elif defined(__WATCOMC__)
# if defined(_M_I86)
#  define ARCHITECTURE_ID \"I86\"

# elif defined(_M_IX86)
#  define ARCHITECTURE_ID \"X86\"

# else /* unknown architecture */
#  define ARCHITECTURE_ID \"\"
# endif

#else
#  define ARCHITECTURE_ID
#endif

/* Convert integer to decimal digit literals.  */
#define DEC(n)                   \\
  ('0' + (((n) / 10000000)%10)), \\
  ('0' + (((n) / 1000000)%10)),  \\
  ('0' + (((n) / 100000)%10)),   \\
  ('0' + (((n) / 10000)%10)),    \\
  ('0' + (((n) / 1000)%10)),     \\
  ('0' + (((n) / 100)%10)),      \\
  ('0' + (((n) / 10)%10)),       \\
  ('0' +  ((n) % 10))

/* Convert integer to hex digit literals.  */
#define HEX(n)             \\
  ('0' + ((n)>>28 & 0xF)), \\
  ('0' + ((n)>>24 & 0xF)), \\
  ('0' + ((n)>>20 & 0xF)), \\
  ('0' + ((n)>>16 & 0xF)), \\
  ('0' + ((n)>>12 & 0xF)), \\
  ('0' + ((n)>>8  & 0xF)), \\
  ('0' + ((n)>>4  & 0xF)), \\
  ('0' + ((n)     & 0xF))

/* Construct a string literal encoding the version number components. */
#ifdef COMPILER_VERSION_MAJOR
char const info_version[] = {
  'I', 'N', 'F', 'O', ':',
  'c','o','m','p','i','l','e','r','_','v','e','r','s','i','o','n','[',
  COMPILER_VERSION_MAJOR,
# ifdef COMPILER_VERSION_MINOR
  '.', COMPILER_VERSION_MINOR,
#  ifdef COMPILER_VERSION_PATCH
   '.', COMPILER_VERSION_PATCH,
#   ifdef COMPILER_VERSION_TWEAK
    '.', COMPILER_VERSION_TWEAK,
#   endif
#  endif
# endif
  ']','\\0'};
#endif

/* Construct a string literal encoding the version number components. */
#ifdef SIMULATE_VERSION_MAJOR
char const info_simulate_version[] = {
  'I', 'N', 'F', 'O', ':',
  's','i','m','u','l','a','t','e','_','v','e','r','s','i','o','n','[',
  SIMULATE_VERSION_MAJOR,
# ifdef SIMULATE_VERSION_MINOR
  '.', SIMULATE_VERSION_MINOR,
#  ifdef SIMULATE_VERSION_PATCH
   '.', SIMULATE_VERSION_PATCH,
#   ifdef SIMULATE_VERSION_TWEAK
    '.', SIMULATE_VERSION_TWEAK,
#   endif
#  endif
# endif
  ']','\\0'};
#endif

/* Construct the string literal in pieces to prevent the source from
   getting matched.  Store it in a pointer rather than an array
   because some compilers will just produce instructions to fill the
   array rather than assigning a pointer to a static array.  */
char const* info_platform = \"INFO\" \":\" \"platform[\" PLATFORM_ID \"]\";
char const* info_arch = \"INFO\" \":\" \"arch[\" ARCHITECTURE_ID \"]\";

")
set (CMAKE_C_COMPILER_ID_RUN "1")
set (CMAKE_C_COMPILER_ID_TEST_FLAGS "-c;-Aa;-D__CLASSIC_C__")
set (CMAKE_C_COMPILER_ID_TOOL_MATCH_INDEX "2")
set (CMAKE_C_COMPILER_ID_TOOL_MATCH_REGEX "
Ld[^
]*(
[ 	]+[^
]*)*
[ 	]+([^ 	
]+)[^
]*-o[^
]*CompilerIdC/(\\./)?(CompilerIdC.xctest/)?CompilerIdC[ 	
\\\"]")
set (CMAKE_C_COMPILER_ID_VENDORS "IAR")
set (CMAKE_C_COMPILER_ID_VENDOR_REGEX_IAR "IAR .+ Compiler")
set (CMAKE_C_COMPILER_INIT "NOTFOUND")
set (CMAKE_C_COMPILER_LIST "cc;gcc;cl;bcc;xlc;clang")
set (CMAKE_C_COMPILER_LOADED "1")
set (CMAKE_C_COMPILER_NAMES "cc")
set (CMAKE_C_COMPILER_PRODUCED_FILES "a.out")
set (CMAKE_C_COMPILER_PRODUCED_OUTPUT "")
set (CMAKE_C_COMPILER_VERSION "7.3.0")
set (CMAKE_C_COMPILER_WORKS "TRUE")
set (CMAKE_C_COMPILER_WRAPPER "")
set (CMAKE_C_COMPILE_FEATURES "c_std_90;c_function_prototypes;c_std_99;c_restrict;c_variadic_macros;c_std_11;c_static_assert")
set (CMAKE_C_COMPILE_OBJECT "<CMAKE_C_COMPILER> <DEFINES> <INCLUDES> <FLAGS> -o <OBJECT>   -c <SOURCE>")
set (CMAKE_C_COMPILE_OPTIONS_PIC "-fPIC")
set (CMAKE_C_COMPILE_OPTIONS_PIE "-fPIE")
set (CMAKE_C_COMPILE_OPTIONS_SYSROOT "--sysroot=")
set (CMAKE_C_COMPILE_OPTIONS_VISIBILITY "-fvisibility=")
set (CMAKE_C_CREATE_ASSEMBLY_SOURCE "<CMAKE_C_COMPILER> <DEFINES> <INCLUDES> <FLAGS> -S <SOURCE> -o <ASSEMBLY_SOURCE>")
set (CMAKE_C_CREATE_PREPROCESSED_SOURCE "<CMAKE_C_COMPILER> <DEFINES> <INCLUDES> <FLAGS> -E <SOURCE> > <PREPROCESSED_SOURCE>")
set (CMAKE_C_CREATE_SHARED_LIBRARY "<CMAKE_C_COMPILER> <CMAKE_SHARED_LIBRARY_C_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>")
set (CMAKE_C_CREATE_SHARED_MODULE "<CMAKE_C_COMPILER> <CMAKE_SHARED_LIBRARY_C_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>")
set (CMAKE_C_FLAGS "")
set (CMAKE_C_FLAGS_DEBUG "-g")
set (CMAKE_C_FLAGS_DEBUG_INIT "-g")
set (CMAKE_C_FLAGS_INIT "")
set (CMAKE_C_FLAGS_MINSIZEREL "-Os -DNDEBUG")
set (CMAKE_C_FLAGS_MINSIZEREL_INIT "-Os -DNDEBUG")
set (CMAKE_C_FLAGS_RELEASE "-O3 -DNDEBUG")
set (CMAKE_C_FLAGS_RELEASE_INIT "-O3 -DNDEBUG")
set (CMAKE_C_FLAGS_RELWITHDEBINFO "-O2 -g -DNDEBUG")
set (CMAKE_C_FLAGS_RELWITHDEBINFO_INIT "-O2 -g -DNDEBUG")
set (CMAKE_C_IGNORE_EXTENSIONS "h;H;o;O;obj;OBJ;def;DEF;rc;RC")
set (CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "/usr/include")
set (CMAKE_C_IMPLICIT_LINK_DIRECTORIES "/usr/lib/gcc/x86_64-linux-gnu/7;/usr/lib/x86_64-linux-gnu;/usr/lib;/lib/x86_64-linux-gnu;/lib")
set (CMAKE_C_IMPLICIT_LINK_FRAMEWORK_DIRECTORIES "")
set (CMAKE_C_IMPLICIT_LINK_LIBRARIES "c")
set (CMAKE_C_INFORMATION_LOADED "1")
set (CMAKE_C_LIBRARY_ARCHITECTURE "x86_64-linux-gnu")
set (CMAKE_C_LINKER_PREFERENCE "10")
set (CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>")
set (CMAKE_C_OUTPUT_EXTENSION ".o")
set (CMAKE_C_PLATFORM_ID "Linux")
set (CMAKE_C_SIMULATE_ID "")
set (CMAKE_C_SIMULATE_VERSION "")
set (CMAKE_C_SIZEOF_DATA_PTR "8")
set (CMAKE_C_SOURCE_FILE_EXTENSIONS "c;m")
set (CMAKE_C_STANDARD_COMPUTED_DEFAULT "11")
set (CMAKE_C_STANDARD_DEFAULT "11")
set (CMAKE_C_VERBOSE_FLAG "-v")
set (CMAKE_DEPFILE_FLAGS_C "-MD -MT <OBJECT> -MF <DEPFILE>")
set (CMAKE_DEPFILE_FLAGS_CXX "-MD -MT <OBJECT> -MF <DEPFILE>")
set (CMAKE_DL_LIBS "dl")
set (CMAKE_EDIT_COMMAND "/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/ccmake")
set (CMAKE_EXECUTABLE_FORMAT "ELF")
set (CMAKE_EXECUTABLE_RPATH_LINK_CXX_FLAG "-Wl,-rpath-link,")
set (CMAKE_EXECUTABLE_RPATH_LINK_C_FLAG "-Wl,-rpath-link,")
set (CMAKE_EXECUTABLE_RUNTIME_CXX_FLAG "-Wl,-rpath,")
set (CMAKE_EXECUTABLE_RUNTIME_CXX_FLAG_SEP ":")
set (CMAKE_EXECUTABLE_RUNTIME_C_FLAG "-Wl,-rpath,")
set (CMAKE_EXECUTABLE_RUNTIME_C_FLAG_SEP ":")
set (CMAKE_EXECUTABLE_SUFFIX "")
set (CMAKE_EXE_EXPORTS_CXX_FLAG "-Wl,--export-dynamic")
set (CMAKE_EXE_EXPORTS_C_FLAG "-Wl,--export-dynamic")
set (CMAKE_EXE_LINKER_FLAGS "")
set (CMAKE_EXE_LINKER_FLAGS_DEBUG "")
set (CMAKE_EXE_LINKER_FLAGS_DEBUG_INIT "")
set (CMAKE_EXE_LINKER_FLAGS_INIT "")
set (CMAKE_EXE_LINKER_FLAGS_MINSIZEREL "")
set (CMAKE_EXE_LINKER_FLAGS_MINSIZEREL_INIT "")
set (CMAKE_EXE_LINKER_FLAGS_RELEASE "")
set (CMAKE_EXE_LINKER_FLAGS_RELEASE_INIT "")
set (CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO "")
set (CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO_INIT "")
set (CMAKE_EXE_LINK_DYNAMIC_CXX_FLAGS "-Wl,-Bdynamic")
set (CMAKE_EXE_LINK_DYNAMIC_C_FLAGS "-Wl,-Bdynamic")
set (CMAKE_EXE_LINK_STATIC_CXX_FLAGS "-Wl,-Bstatic")
set (CMAKE_EXE_LINK_STATIC_C_FLAGS "-Wl,-Bstatic")
set (CMAKE_EXPORT_COMPILE_COMMANDS "OFF")
set (CMAKE_EXTRA_GENERATOR "")
set (CMAKE_FILES_DIRECTORY "/CMakeFiles")
set (CMAKE_FIND_LIBRARY_PREFIXES "lib")
set (CMAKE_FIND_LIBRARY_SUFFIXES ".so;.a")
set (CMAKE_GENERATOR "Unix Makefiles")
set (CMAKE_GENERATOR_PLATFORM "")
set (CMAKE_GENERATOR_TOOLSET "")
set (CMAKE_HOME_DIRECTORY "/home/filippos/programs/dramms-1.5.1-source")
set (CMAKE_HOST_PATH "/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin")
set (CMAKE_HOST_SYSTEM "Linux-4.15.0-45-generic")
set (CMAKE_HOST_SYSTEM_NAME "Linux")
set (CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")
set (CMAKE_HOST_SYSTEM_VERSION "4.15.0-45-generic")
set (CMAKE_HOST_UNIX "1")
set (CMAKE_INCLUDE_FLAG_C "-I")
set (CMAKE_INCLUDE_FLAG_CXX "-I")
set (CMAKE_INCLUDE_FLAG_C_SEP "")
set (CMAKE_INCLUDE_SYSTEM_FLAG_C "-isystem ")
set (CMAKE_INCLUDE_SYSTEM_FLAG_CXX "-isystem ")
set (CMAKE_INSTALL_DEFAULT_COMPONENT_NAME "Unspecified")
set (CMAKE_INSTALL_PREFIX "/home/filippos/programs/dr")
set (CMAKE_INSTALL_RPATH_USE_LINK_PATH "FALSE")
set (CMAKE_INSTALL_SO_NO_EXE "1")
set (CMAKE_INTERNAL_PLATFORM_ABI "ELF")
set (CMAKE_LIBRARY_ARCHITECTURE "x86_64-linux-gnu")
set (CMAKE_LIBRARY_ARCHITECTURE_REGEX "[a-z0-9_]+(-[a-z0-9_]+)?-linux-gnu[a-z0-9_]*")
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib")
set (CMAKE_LIBRARY_PATH_FLAG "-L")
set (CMAKE_LIBRARY_PATH_TERMINATOR "")
set (CMAKE_LINKER "/usr/bin/ld")
set (CMAKE_LINKER "/usr/bin/ld")
set (CMAKE_LINK_LIBRARY_FLAG "-l")
set (CMAKE_LINK_LIBRARY_SUFFIX "")
set (CMAKE_MAJOR_VERSION "3")
set (CMAKE_MAKE_PROGRAM "/usr/bin/make")
set (CMAKE_MATCH_0 "")
set (CMAKE_MATCH_1 "")
set (CMAKE_MATCH_2 "")
set (CMAKE_MATCH_COUNT "0")
set (CMAKE_MINIMUM_REQUIRED_VERSION "2.8.4")
set (CMAKE_MINOR_VERSION "8")
set (CMAKE_MODULE_LINKER_FLAGS "")
set (CMAKE_MODULE_LINKER_FLAGS_DEBUG "")
set (CMAKE_MODULE_LINKER_FLAGS_DEBUG_INIT "")
set (CMAKE_MODULE_LINKER_FLAGS_INIT "")
set (CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL "")
set (CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL_INIT "")
set (CMAKE_MODULE_LINKER_FLAGS_RELEASE "")
set (CMAKE_MODULE_LINKER_FLAGS_RELEASE_INIT "")
set (CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO "")
set (CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO_INIT "")
set (CMAKE_MODULE_PATH "/home/filippos/programs/dramms-1.5.1-source/config;/home/filippos/programs/dr/share/dramms/basis/cmake-modules")
set (CMAKE_NM "/usr/bin/nm")
set (CMAKE_OBJCOPY "/usr/bin/objcopy")
set (CMAKE_OBJDUMP "/usr/bin/objdump")
set (CMAKE_PARENT_LIST_FILE "/home/filippos/programs/dramms-1.5.1-source/src/CMakeLists.txt")
set (CMAKE_PATCH_VERSION "2")
set (CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "/lib;/lib32;/lib64;/usr/lib;/usr/lib32;/usr/lib64")
set (CMAKE_PLATFORM_INFO_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles/3.8.2")
set (CMAKE_PLATFORM_INFO_INITIALIZED "1")
set (CMAKE_PLATFORM_USES_PATH_WHEN_NO_SONAME "1")
set (CMAKE_PREFIX_PATH "/home/filippos/programs/dr/build/bundle/lib;/home/filippos/programs/dr/build/bundle;/home/filippos/programs/dr/lib/cmake/dramms")
set (CMAKE_PROJECT_NAME "DRAMMS")
set (CMAKE_RANLIB "/usr/bin/ranlib")
set (CMAKE_RANLIB "/usr/bin/ranlib")
set (CMAKE_ROOT "/home/filippos/Support/cmake-3.8.2-Linux-x86_64/share/cmake-3.8")
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/bin")
set (CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS "-shared")
set (CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS "-shared")
set (CMAKE_SHARED_LIBRARY_CXX_FLAGS "-fPIC")
set (CMAKE_SHARED_LIBRARY_C_FLAGS "-fPIC")
set (CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS "-rdynamic")
set (CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "-rdynamic")
set (CMAKE_SHARED_LIBRARY_LINK_DYNAMIC_CXX_FLAGS "-Wl,-Bdynamic")
set (CMAKE_SHARED_LIBRARY_LINK_DYNAMIC_C_FLAGS "-Wl,-Bdynamic")
set (CMAKE_SHARED_LIBRARY_LINK_STATIC_CXX_FLAGS "-Wl,-Bstatic")
set (CMAKE_SHARED_LIBRARY_LINK_STATIC_C_FLAGS "-Wl,-Bstatic")
set (CMAKE_SHARED_LIBRARY_PREFIX "lib")
set (CMAKE_SHARED_LIBRARY_RPATH_LINK_CXX_FLAG "-Wl,-rpath-link,")
set (CMAKE_SHARED_LIBRARY_RPATH_LINK_C_FLAG "-Wl,-rpath-link,")
set (CMAKE_SHARED_LIBRARY_RUNTIME_CXX_FLAG "-Wl,-rpath,")
set (CMAKE_SHARED_LIBRARY_RUNTIME_CXX_FLAG_SEP ":")
set (CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG "-Wl,-rpath,")
set (CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG_SEP ":")
set (CMAKE_SHARED_LIBRARY_SONAME_CXX_FLAG "-Wl,-soname,")
set (CMAKE_SHARED_LIBRARY_SONAME_C_FLAG "-Wl,-soname,")
set (CMAKE_SHARED_LIBRARY_SUFFIX ".so")
set (CMAKE_SHARED_LINKER_FLAGS "")
set (CMAKE_SHARED_LINKER_FLAGS_DEBUG "")
set (CMAKE_SHARED_LINKER_FLAGS_DEBUG_INIT "")
set (CMAKE_SHARED_LINKER_FLAGS_INIT "")
set (CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL "")
set (CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL_INIT "")
set (CMAKE_SHARED_LINKER_FLAGS_RELEASE "")
set (CMAKE_SHARED_LINKER_FLAGS_RELEASE_INIT "")
set (CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO "")
set (CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO_INIT "")
set (CMAKE_SHARED_MODULE_CREATE_CXX_FLAGS "-shared")
set (CMAKE_SHARED_MODULE_CREATE_C_FLAGS "-shared")
set (CMAKE_SHARED_MODULE_CXX_FLAGS "-fPIC")
set (CMAKE_SHARED_MODULE_C_FLAGS "-fPIC")
set (CMAKE_SHARED_MODULE_LINK_DYNAMIC_CXX_FLAGS "-Wl,-Bdynamic")
set (CMAKE_SHARED_MODULE_LINK_DYNAMIC_C_FLAGS "-Wl,-Bdynamic")
set (CMAKE_SHARED_MODULE_LINK_STATIC_CXX_FLAGS "-Wl,-Bstatic")
set (CMAKE_SHARED_MODULE_LINK_STATIC_C_FLAGS "-Wl,-Bstatic")
set (CMAKE_SHARED_MODULE_PREFIX "lib")
set (CMAKE_SHARED_MODULE_SUFFIX ".so")
set (CMAKE_SIZEOF_VOID_P "8")
set (CMAKE_SKIP_BUILD_RPATH "FALSE")
set (CMAKE_SKIP_INSTALL_RPATH "NO")
set (CMAKE_SKIP_RPATH "FALSE")
set (CMAKE_SKIP_RPATH "FALSE")
set (CMAKE_SOURCE_DIR "/home/filippos/programs/dramms-1.5.1-source")
set (CMAKE_STATIC_LIBRARY_PREFIX "lib")
set (CMAKE_STATIC_LIBRARY_SUFFIX ".a")
set (CMAKE_STATIC_LINKER_FLAGS "")
set (CMAKE_STATIC_LINKER_FLAGS_DEBUG "")
set (CMAKE_STATIC_LINKER_FLAGS_DEBUG_INIT "")
set (CMAKE_STATIC_LINKER_FLAGS_INIT "")
set (CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL "")
set (CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL_INIT "")
set (CMAKE_STATIC_LINKER_FLAGS_RELEASE "")
set (CMAKE_STATIC_LINKER_FLAGS_RELEASE_INIT "")
set (CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO "")
set (CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO_INIT "")
set (CMAKE_STRIP "/usr/bin/strip")
set (CMAKE_SYSTEM "Linux-4.15.0-45-generic")
set (CMAKE_SYSTEM_INCLUDE_PATH "/usr/include/w32api;/usr/X11R6/include;/usr/include/X11;/usr/pkg/include;/opt/csw/include;/opt/include;/usr/openwin/include")
set (CMAKE_SYSTEM_INFO_FILE "Platform/Linux")
set (CMAKE_SYSTEM_LIBRARY_PATH "/usr/lib/w32api;/usr/X11R6/lib;/usr/lib/X11;/usr/pkg/lib;/opt/csw/lib;/opt/lib;/usr/openwin/lib")
set (CMAKE_SYSTEM_LOADED "1")
set (CMAKE_SYSTEM_NAME "Linux")
set (CMAKE_SYSTEM_PREFIX_PATH "/usr/local;/usr;/;/home/filippos/Support/cmake-3.8.2-Linux-x86_64;/home/filippos/programs/dr")
set (CMAKE_SYSTEM_PROCESSOR "x86_64")
set (CMAKE_SYSTEM_PROGRAM_PATH "/usr/pkg/bin")
set (CMAKE_SYSTEM_SPECIFIC_INFORMATION_LOADED "1")
set (CMAKE_SYSTEM_SPECIFIC_INITIALIZE_LOADED "1")
set (CMAKE_SYSTEM_VERSION "4.15.0-45-generic")
set (CMAKE_TWEAK_VERSION "0")
set (CMAKE_UNAME "/bin/uname")
set (CMAKE_VERBOSE_MAKEFILE "FALSE")
set (CMAKE_VERSION "3.8.2")
set (CMPS "")
set (CONFIG_DIR "config")
set (CONFIG_FILES "/home/filippos/programs/dr/lib/cmake/dramms/BASISExports-release.cmake")
set (CPACK_SOURCE_IGNORE_FILES "build/bundle;build/CMakeFiles;build/CMakeCache.txt;build/Makefile;build/cmake_install.cmake")
set (CXX_TEST_WAS_RUN "1")
set (C_TEST_WAS_RUN "1")
set (DART_TESTING_TIMEOUT "")
set (DOC_EXT ".txt")
set (DRAMMSFastPD_CONFIG "/home/filippos/programs/dr/build/bundle/lib/DRAMMSFastPDConfig.cmake")
set (DRAMMSFastPD_CONSIDERED_CONFIGS "/home/filippos/programs/dr/build/bundle/lib/DRAMMSFastPDConfig.cmake")
set (DRAMMSFastPD_CONSIDERED_VERSIONS "unknown")
set (DRAMMSFastPD_DIR "/home/filippos/programs/dr/build/bundle/lib")
set (DRAMMSFastPD_FOUND "1")
set (DRAMMSFastPD_INCLUDE_DIR "/home/filippos/programs/dr/build/bundle/include/dramms/fastpd")
set (DRAMMSFastPD_LIBRARY "/home/filippos/programs/dr/build/bundle/lib/libdramms_fastpd.a")
set (DRAMMSFastPD_VERSION_COUNT "0")
set (DRAMMSFastPD_VERSION_MAJOR "0")
set (DRAMMSFastPD_VERSION_MINOR "0")
set (DRAMMSFastPD_VERSION_PATCH "0")
set (DRAMMSFastPD_VERSION_STRING "0.0.0")
set (DRAMMSFastPD_VERSION_TWEAK "0")
set (DRAMMS_BINARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build")
set (DRAMMS_BUNDLE_LINK_DIRS "/home/filippos/programs/dr/lib/dramms/basis")
set (DRAMMS_CUSTOM_EXPORT_TARGETS "")
set (DRAMMS_EXPORT_TARGETS "common;basis;CheckTwoImages;GenerateGaborListFile;GenerateImageListFile;CalculateImageSimilarity;CombineFeatureLists;CombineTransforms;CalculateGaborTextures;MatchHistograms;ApplyTransform;CalculateJacobianMap;CalculateRavensMaps;ImageOperation;TransformOperation;ConvertDeformation;ConvertImage;Deform2D;Deform3D")
set (DRAMMS_IMPORTED_LOCATIONS "/home/filippos/programs/dr/lib/dramms/basis/libutilities.a;/home/filippos/programs/dr/lib/dramms/basis/libtest.a;/home/filippos/programs/dr/lib/dramms/basis/libtestmain.a;/home/filippos/programs/dr/lib/dramms/basis/libbasis.a;/home/filippos/programs/dr/lib/dramms/basis/testdriver;/home/filippos/programs/dr/lib/dramms/bash;/home/filippos/programs/dr/share/dramms/basis/cmake-modules/basistest.ctest;/home/filippos/programs/dr/lib/dramms/basis/basistest-svn;/home/filippos/programs/dr/lib/dramms/basis/basistest-slave;/home/filippos/programs/dr/lib/dramms/basis/basistest-master;/home/filippos/programs/dr/lib/dramms/basis/basistest-cron;/home/filippos/programs/dr/lib/dramms/basis/basistest;/home/filippos/programs/dr/lib/dramms/bash;/usr/lib/x86_64-linux-gnu/libz.so;/home/filippos/programs/dr/build/bundle/lib/libznz.a;/home/filippos/programs/dr/build/bundle/lib/libniftiio.a")
set (DRAMMS_IMPORTED_RANKS "0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1")
set (DRAMMS_IMPORTED_TARGETS "basis.utilities_cxx;basis.testlib;basis.testmain;basis.basis;basis.testdriver;basis.utilities_bash;basis.basistest_ctest;basis.basistest-svn;basis.basistest-slave;basis.basistest-master;basis.basistest-cron;basis.basistest;basis.basis_sh;ZLIB::ZLIB;niftiznz;niftiio")
set (DRAMMS_IMPORTED_TYPES "STATIC;STATIC;STATIC;STATIC;EXECUTABLE;SCRIPT_LIBRARY;SCRIPT_MODULE;EXECUTABLE;EXECUTABLE;EXECUTABLE;EXECUTABLE;EXECUTABLE;SCRIPT_LIBRARY;UNKNOWN;STATIC;STATIC")
set (DRAMMS_INSTALL_EXPORT_TARGETS "common;basis;CheckTwoImages;GenerateGaborListFile;GenerateImageListFile;CalculateImageSimilarity;CombineFeatureLists;CombineTransforms;CalculateGaborTextures;MatchHistograms;ApplyTransform;CalculateJacobianMap;CalculateRavensMaps;ImageOperation;TransformOperation;ConvertDeformation;ConvertImage;Deform2D;Deform3D")
set (DRAMMS_PROJECT_INCLUDE_DIRS "/home/filippos/programs/dr/include;/home/filippos/programs/dr/build/bundle/include;/home/filippos/programs/dr/build/bundle/include/nifti;/home/filippos/programs/dr/build/bundle/include/dramms/fastpd;/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/include;/home/filippos/programs/dramms-1.5.1-source/include;/home/filippos/programs/dramms-1.5.1-source/src")
set (DRAMMS_PROJECT_LINK_DIRS "/home/filippos/programs/dr/lib/dramms/basis")
set (DRAMMS_PROJECT_USES_BASH_UTILITIES "FALSE")
set (DRAMMS_PROJECT_USES_CXX_UTILITIES "TRUE")
set (DRAMMS_PROJECT_USES_JAVA_UTILITIES "FALSE")
set (DRAMMS_PROJECT_USES_MATLAB_UTILITIES "FALSE")
set (DRAMMS_PROJECT_USES_PERL_UTILITIES "FALSE")
set (DRAMMS_PROJECT_USES_PYTHON_UTILITIES "FALSE")
set (DRAMMS_SOURCE_DIR "/home/filippos/programs/dramms-1.5.1-source")
set (DRAMMS_TARGETS "common;basis;CheckTwoImages;GenerateGaborListFile;GenerateImageListFile;CalculateImageSimilarity;CombineFeatureLists;CombineTransforms;CalculateGaborTextures;MatchHistograms;ApplyTransform;CalculateJacobianMap;CalculateRavensMaps;ImageOperation;TransformOperation;ConvertDeformation;ConvertImage;Deform2D;Deform3D")
set (DRAMMS_TEST_EXPORT_TARGETS "")
set (E "")
set (EXPORT "TRUE")
set (F "")
set (FINDPERL_VARNAME "PERL_PRIVLIB")
set (FIND_ARGN "REQUIRED")
set (FIND_PACKAGE_MESSAGE_DETAILS_NiftiCLib "[/home/filippos/programs/dr/build/bundle/include][/home/filippos/programs/dr/build/bundle/lib/libniftiio.a][/home/filippos/programs/dr/build/bundle/lib/libznz.a][v()]")
set (FIND_PACKAGE_MESSAGE_DETAILS_Perl "[/usr/bin/perl][v5.26.1()]")
set (FIND_PACKAGE_MESSAGE_DETAILS_ZLIB "[/usr/lib/x86_64-linux-gnu/libz.so][/usr/include][v1.2.11()]")
set (GIT_EXECUTABLE "/usr/bin/git")
set (GIT_FOUND "TRUE")
set (GIT_VERSION_STRING "2.17.1")
set (Git_FOUND "TRUE")
set (Git_VERSION_STRING "2.17.1")
set (INSTALL_ARCHIVE_DIR "lib/dramms")
set (INSTALL_BASH_LIBRARY_DIR "lib/dramms/bash")
set (INSTALL_BASH_SITE_DIR "lib/dramms/bash")
set (INSTALL_CONFIG_DIR "lib/cmake/dramms")
set (INSTALL_DATA_DIR "share/dramms/data")
set (INSTALL_DOC_DIR "doc/dramms")
set (INSTALL_EXAMPLE_DIR "share/dramms/example")
set (INSTALL_INCLUDE_DIR "include")
set (INSTALL_JYTHON_LIBRARY_DIR "lib/dramms/jython")
set (INSTALL_JYTHON_SITE_DIR "lib/dramms/jython")
set (INSTALL_LIBEXEC_DIR "lib/dramms")
set (INSTALL_LIBRARY_DIR "lib/dramms")
set (INSTALL_MAN_DIR "share/man")
set (INSTALL_MATLAB_LIBRARY_DIR "lib/dramms/matlab")
set (INSTALL_MATLAB_SITE_DIR "lib/dramms/matlab")
set (INSTALL_PERL_LIBRARY_DIR "lib/dramms/perl")
set (INSTALL_PERL_SITE_DIR "lib/dramms/perl")
set (INSTALL_PYTHON_LIBRARY_DIR "lib/dramms/python")
set (INSTALL_PYTHON_SITE_DIR "lib/dramms/python")
set (INSTALL_RUNTIME_DIR "bin")
set (INSTALL_SHARE_DIR "share/dramms")
set (INSTALL_TEXINFO_DIR "share/info")
set (JYTHON_LIBRARY_TARGET "jythonlib")
set (JYTHON_VERSION_MAJOR "0")
set (JYTHON_VERSION_MINOR "0")
set (JYTHON_VERSION_PATCH "0")
set (JYTHON_VERSION_STRING "0.0")
set (M "")
set (MATLAB_LIBRARY_TARGET "matlablib")
set (MATLAB_RELEASE "")
set (MATLAB_VERSION_MAJOR "0")
set (MATLAB_VERSION_MINOR "0")
set (MATLAB_VERSION_PATCH "0")
set (MATLAB_VERSION_STRING "0.0")
set (MODULE "")
set (MODULE_INFO_FILES "")
set (MSVC_CXX_ARCHITECTURE_ID "")
set (MSVC_C_ARCHITECTURE_ID "")
set (N "1")
set (NIFTICLIB_FOUND "TRUE")
set (NO_BASIS_IMPORTS "")
set (NS "BASIS_")
set (NiftiCLib_DIR "/home/filippos/programs/dr/build/bundle")
set (NiftiCLib_FOUND "TRUE")
set (NiftiCLib_INCLUDES "/home/filippos/programs/dr/build/bundle/include;/home/filippos/programs/dr/build/bundle/include/nifti")
set (NiftiCLib_INCLUDE_DIR "/home/filippos/programs/dr/build/bundle/include")
set (NiftiCLib_INCLUDE_DIRS "/home/filippos/programs/dr/build/bundle/include;/home/filippos/programs/dr/build/bundle/include/nifti")
set (NiftiCLib_LIB "/home/filippos/programs/dr/build/bundle/lib/libniftiio.a")
set (NiftiCLib_LIBRARIES "/usr/lib/x86_64-linux-gnu/libz.so;/home/filippos/programs/dr/build/bundle/lib/libznz.a;/home/filippos/programs/dr/build/bundle/lib/libniftiio.a")
set (NiftiCLib_LIBRARY "/home/filippos/programs/dr/build/bundle/lib/libniftiio.a")
set (NiftiCLib_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES ".so;.a")
set (NiftiCLib_znz_LIBRARY "/home/filippos/programs/dr/build/bundle/lib/libznz.a")
set (PERL "/usr/bin/perl")
set (PERLLIBS_FOUND "TRUE")
set (PERL_ARCHLIB "/usr/lib/x86_64-linux-gnu/perl/5.26")
set (PERL_ARCHLIB_OUTPUT_VARIABLE "installarchlib='/usr/lib/x86_64-linux-gnu/perl/5.26';
")
set (PERL_ARCHLIB_RESULT_VARIABLE "0")
set (PERL_ARCHNAME "x86_64-linux-gnu-thread-multi")
set (PERL_ARCHNAME_OUTPUT_VARIABLE "archname='x86_64-linux-gnu-thread-multi';
")
set (PERL_ARCHNAME_RESULT_VARIABLE "0")
set (PERL_CPPFLAGS_OUTPUT_VARIABLE "cppflags='-D_REENTRANT -D_GNU_SOURCE -DDEBIAN -fwrapv -fno-strict-aliasing -pipe -I/usr/local/include';
")
set (PERL_CPPFLAGS_RESULT_VARIABLE "0")
set (PERL_EXECUTABLE "/usr/bin/perl")
set (PERL_EXTRA_C_FLAGS "-D_REENTRANT -D_GNU_SOURCE -DDEBIAN -fwrapv -fno-strict-aliasing -pipe -I/usr/local/include")
set (PERL_FOUND "TRUE")
set (PERL_INCLUDE_DIR "/usr/lib/x86_64-linux-gnu/perl/5.26/CORE")
set (PERL_INCLUDE_DIRS "/usr/lib/x86_64-linux-gnu/perl/5.26/CORE")
set (PERL_INCLUDE_PATH "/usr/lib/x86_64-linux-gnu/perl/5.26/CORE")
set (PERL_LIBRARIES "/usr/lib/x86_64-linux-gnu/libperl.so.5.26")
set (PERL_LIBRARY "/usr/lib/x86_64-linux-gnu/libperl.so.5.26")
set (PERL_LIBRARY_OUTPUT_VARIABLE "libperl='libperl.so.5.26';
")
set (PERL_LIBRARY_RESULT_VARIABLE "0")
set (PERL_LIBRARY_TARGET "perllib")
set (PERL_MINUSV_OUTPUT_VARIABLE "Summary of my perl5 (revision 5 version 26 subversion 1) configuration:
   
  Platform:
    osname=linux
    osvers=4.9.0
    archname=x86_64-linux-gnu-thread-multi
    uname='linux localhost 4.9.0 #1 smp debian 4.9.0 x86_64 gnulinux '
    config_args='-Dusethreads -Duselargefiles -Dcc=x86_64-linux-gnu-gcc -Dcpp=x86_64-linux-gnu-cpp -Dld=x86_64-linux-gnu-gcc -Dccflags=-DDEBIAN -Wdate-time -D_FORTIFY_SOURCE=2 -g -O2 -fdebug-prefix-map=/build/perl-hx1dVS/perl-5.26.1=. -fstack-protector-strong -Wformat -Werror=format-security -Dldflags= -Wl,-Bsymbolic-functions -Wl,-z,relro -Dlddlflags=-shared -Wl,-Bsymbolic-functions -Wl,-z,relro -Dcccdlflags=-fPIC -Darchname=x86_64-linux-gnu -Dprefix=/usr -Dprivlib=/usr/share/perl/5.26 -Darchlib=/usr/lib/x86_64-linux-gnu/perl/5.26 -Dvendorprefix=/usr -Dvendorlib=/usr/share/perl5 -Dvendorarch=/usr/lib/x86_64-linux-gnu/perl5/5.26 -Dsiteprefix=/usr/local -Dsitelib=/usr/local/share/perl/5.26.1 -Dsitearch=/usr/local/lib/x86_64-linux-gnu/perl/5.26.1 -Dman1dir=/usr/share/man/man1 -Dman3dir=/usr/share/man/man3 -Dsiteman1dir=/usr/local/man/man1 -Dsiteman3dir=/usr/local/man/man3 -Duse64bitint -Dman1ext=1 -Dman3ext=3perl -Dpager=/usr/bin/sensible-pager -Uafs -Ud_csh -Ud_ualarm -Uusesfio -Uusenm -Ui_libutil -Ui_xlocale -Uversiononly -DDEBUGGING=-g -Doptimize=-O2 -dEs -Duseshrplib -Dlibperl=libperl.so.5.26.1'
    hint=recommended
    useposix=true
    d_sigaction=define
    useithreads=define
    usemultiplicity=define
    use64bitint=define
    use64bitall=define
    uselongdouble=undef
    usemymalloc=n
    default_inc_excludes_dot=define
    bincompat5005=undef
  Compiler:
    cc='x86_64-linux-gnu-gcc'
    ccflags ='-D_REENTRANT -D_GNU_SOURCE -DDEBIAN -fwrapv -fno-strict-aliasing -pipe -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64'
    optimize='-O2 -g'
    cppflags='-D_REENTRANT -D_GNU_SOURCE -DDEBIAN -fwrapv -fno-strict-aliasing -pipe -I/usr/local/include'
    ccversion=''
    gccversion='7.3.0'
    gccosandvers=''
    intsize=4
    longsize=8
    ptrsize=8
    doublesize=8
    byteorder=12345678
    doublekind=3
    d_longlong=define
    longlongsize=8
    d_longdbl=define
    longdblsize=16
    longdblkind=3
    ivtype='long'
    ivsize=8
    nvtype='double'
    nvsize=8
    Off_t='off_t'
    lseeksize=8
    alignbytes=8
    prototype=define
  Linker and Libraries:
    ld='x86_64-linux-gnu-gcc'
    ldflags =' -fstack-protector-strong -L/usr/local/lib'
    libpth=/usr/local/lib /usr/lib/gcc/x86_64-linux-gnu/7/include-fixed /usr/include/x86_64-linux-gnu /usr/lib /lib/x86_64-linux-gnu /lib/../lib /usr/lib/x86_64-linux-gnu /usr/lib/../lib /lib
    libs=-lgdbm -lgdbm_compat -ldb -ldl -lm -lpthread -lc -lcrypt
    perllibs=-ldl -lm -lpthread -lc -lcrypt
    libc=libc-2.27.so
    so=so
    useshrplib=true
    libperl=libperl.so.5.26
    gnulibc_version='2.27'
  Dynamic Linking:
    dlsrc=dl_dlopen.xs
    dlext=so
    d_dlsymun=undef
    ccdlflags='-Wl,-E'
    cccdlflags='-fPIC'
    lddlflags='-shared -L/usr/local/lib -fstack-protector-strong'


Characteristics of this binary (from libperl): 
  Compile-time options:
    HAS_TIMES
    MULTIPLICITY
    PERLIO_LAYERS
    PERL_COPY_ON_WRITE
    PERL_DONT_CREATE_GVSV
    PERL_IMPLICIT_CONTEXT
    PERL_MALLOC_WRAP
    PERL_OP_PARENT
    PERL_PRESERVE_IVUV
    USE_64_BIT_ALL
    USE_64_BIT_INT
    USE_ITHREADS
    USE_LARGE_FILES
    USE_LOCALE
    USE_LOCALE_COLLATE
    USE_LOCALE_CTYPE
    USE_LOCALE_NUMERIC
    USE_LOCALE_TIME
    USE_PERLIO
    USE_PERL_ATOF
    USE_REENTRANT_API
  Locally applied patches:
    DEBPKG:debian/cpan_definstalldirs - Provide a sensible INSTALLDIRS default for modules installed from CPAN.
    DEBPKG:debian/db_file_ver - https://bugs.debian.org/340047 Remove overly restrictive DB_File version check.
    DEBPKG:debian/doc_info - Replace generic man(1) instructions with Debian-specific information.
    DEBPKG:debian/enc2xs_inc - https://bugs.debian.org/290336 Tweak enc2xs to follow symlinks and ignore missing @INC directories.
    DEBPKG:debian/errno_ver - https://bugs.debian.org/343351 Remove Errno version check due to upgrade problems with long-running processes.
    DEBPKG:debian/libperl_embed_doc - https://bugs.debian.org/186778 Note that libperl-dev package is required for embedded linking
    DEBPKG:fixes/respect_umask - Respect umask during installation
    DEBPKG:debian/writable_site_dirs - Set umask approproately for site install directories
    DEBPKG:debian/extutils_set_libperl_path - EU:MM: set location of libperl.a under /usr/lib
    DEBPKG:debian/no_packlist_perllocal - Don't install .packlist or perllocal.pod for perl or vendor
    DEBPKG:debian/fakeroot - Postpone LD_LIBRARY_PATH evaluation to the binary targets.
    DEBPKG:debian/instmodsh_doc - Debian policy doesn't install .packlist files for core or vendor.
    DEBPKG:debian/ld_run_path - Remove standard libs from LD_RUN_PATH as per Debian policy.
    DEBPKG:debian/libnet_config_path - Set location of libnet.cfg to /etc/perl/Net as /usr may not be writable.
    DEBPKG:debian/perlivp - https://bugs.debian.org/510895 Make perlivp skip include directories in /usr/local
    DEBPKG:debian/deprecate-with-apt - https://bugs.debian.org/747628 Point users to Debian packages of deprecated core modules
    DEBPKG:debian/squelch-locale-warnings - https://bugs.debian.org/508764 Squelch locale warnings in Debian package maintainer scripts
    DEBPKG:debian/patchlevel - https://bugs.debian.org/567489 List packaged patches for 5.26.1-6ubuntu0.3 in patchlevel.h
    DEBPKG:fixes/document_makemaker_ccflags - https://bugs.debian.org/628522 [rt.cpan.org #68613] Document that CCFLAGS should include \$Config{ccflags}
    DEBPKG:debian/find_html2text - https://bugs.debian.org/640479 Configure CPAN::Distribution with correct name of html2text
    DEBPKG:debian/perl5db-x-terminal-emulator.patch - https://bugs.debian.org/668490 Invoke x-terminal-emulator rather than xterm in perl5db.pl
    DEBPKG:debian/cpan-missing-site-dirs - https://bugs.debian.org/688842 Fix CPAN::FirstTime defaults with nonexisting site dirs if a parent is writable
    DEBPKG:fixes/memoize_storable_nstore - [rt.cpan.org #77790] https://bugs.debian.org/587650 Memoize::Storable: respect 'nstore' option not respected
    DEBPKG:debian/makemaker-pasthru - https://bugs.debian.org/758471 Pass LD settings through to subdirectories
    DEBPKG:debian/makemaker-manext - https://bugs.debian.org/247370 Make EU::MakeMaker honour MANnEXT settings in generated manpage headers
    DEBPKG:debian/kfreebsd-softupdates - https://bugs.debian.org/796798 Work around Debian Bug#796798
    DEBPKG:fixes/autodie-scope - https://bugs.debian.org/798096 Fix a scoping issue with \"no autodie\" and the \"system\" sub
    DEBPKG:fixes/memoize-pod - [rt.cpan.org #89441] Fix POD errors in Memoize
    DEBPKG:debian/hurd-softupdates - https://bugs.debian.org/822735 Fix t/op/stat.t failures on hurd
    DEBPKG:fixes/math_complex_doc_great_circle - https://bugs.debian.org/697567 [rt.cpan.org #114104] Math::Trig: clarify definition of great_circle_midpoint
    DEBPKG:fixes/math_complex_doc_see_also - https://bugs.debian.org/697568 [rt.cpan.org #114105] Math::Trig: add missing SEE ALSO
    DEBPKG:fixes/math_complex_doc_angle_units - https://bugs.debian.org/731505 [rt.cpan.org #114106] Math::Trig: document angle units
    DEBPKG:fixes/cpan_web_link - https://bugs.debian.org/367291 CPAN: Add link to main CPAN web site
    DEBPKG:fixes/time_piece_doc - https://bugs.debian.org/817925 Time::Piece: Improve documentation for add_months and add_years
    DEBPKG:fixes/extutils_makemaker_reproducible - https://bugs.debian.org/835815 https://bugs.debian.org/834190 Make perllocal.pod files reproducible
    DEBPKG:fixes/file_path_hurd_errno - File-Path: Fix test failure in Hurd due to hard-coded ENOENT
    DEBPKG:debian/hppa_op_optimize_workaround - https://bugs.debian.org/838613 Temporarily lower the optimization of op.c on hppa due to gcc-6 problems
    DEBPKG:debian/installman-utf8 - https://bugs.debian.org/840211 Generate man pages with UTF-8 characters
    DEBPKG:fixes/file_path_chmod_race - https://bugs.debian.org/863870 [rt.cpan.org #121951] Prevent directory chmod race attack.
    DEBPKG:fixes/extutils_file_path_compat - Correct the order of tests of chmod(). (#294)
    DEBPKG:fixes/getopt-long-2 - [rt.cpan.org #120300] Withdraw part of commit 5d9947fb445327c7299d8beb009d609bc70066c0, which tries to implement more GNU getopt_long campatibility. GNU
    DEBPKG:fixes/getopt-long-3 - provide a default value for optional arguments
    DEBPKG:fixes/getopt-long-4 - https://bugs.debian.org/864544 [rt.cpan.org #122068] Fix issue #122068.
    DEBPKG:fixes/test-builder-reset - https://bugs.debian.org/865894 Reset inside subtest maintains parent
    DEBPKG:debian/hppa_opmini_optimize_workaround - https://bugs.debian.org/869122 Lower the optimization level of opmini.c on hppa
    DEBPKG:debian/sh4_op_optimize_workaround - https://bugs.debian.org/869373 Also lower the optimization level of op.c and opmini.c on sh4
    DEBPKG:fixes/json-pp-example - [rt.cpan.org #92793] https://bugs.debian.org/871837 fix RT-92793: bug in SYNOPSIS
    DEBPKG:debian/perldoc-pager - https://bugs.debian.org/870340 [rt.cpan.org #120229] Fix perldoc terminal escapes when sensible-pager is less
    DEBPKG:debian/prune_libs - https://bugs.debian.org/128355 Prune the list of libraries wanted to what we actually need.
    DEBPKG:debian/configure-regen - https://bugs.debian.org/762638 Regenerate Configure et al. after probe unit changes
    DEBPKG:fixes/rename-filexp.U-phase1 - regen-configure: rename filexp.U to filexp_path.U, phase 1
    DEBPKG:fixes/rename-filexp.U-phase2 - regen-configure: rename filexp.U to filexp_path.U, phase 2
    DEBPKG:fixes/packaging_test_skips - Skip various tests if PERL_BUILD_PACKAGING is set
    DEBPKG:debian/mod_paths - Tweak @INC ordering for Debian
    DEBPKG:fixes/encode-alias-regexp - https://bugs.debian.org/880085 fix https://github.com/dankogai/p5-encode/issues/127
    DEBPKG:fixes/regex-memory-leak - [910a6a8] https://bugs.debian.org/891196 [perl #132892] perl #132892: avoid leak by mortalizing temporary copy of pattern
    DEBPKG:fixes/CVE-2018-6797 - [perl #132227] (perl #132227) restart a node if we change to uni rules within the node and encounter a sharp S
    DEBPKG:fixes/CVE-2018-6798/pt1 - [perl #132063] Heap buffer overflow
    DEBPKG:fixes/CVE-2018-6798/pt2 - [perl #132063] 5.26.1: fix TRIE_READ_CHAR and DECL_TRIE_TYPE to account for non-utf8 target
    DEBPKG:fixes/CVE-2018-6798/pt3 - [perl #132063] (perl #132063) we should no longer warn for this code
    DEBPKG:fixes/CVE-2018-6798/pt4 - [perl #132063] utf8.c: Don't dump malformation past first NUL
    DEBPKG:fixes/CVE-2018-6913 - [perl #131844] (perl #131844) fix various space calculation issues in pp_pack.c
    DEBPKG:fixes/CVE-2018-12015.patch - [PATCH] [PATCH] Remove existing files before overwriting them
    DEBPKG:fixes/CVE-2018-18311.patch - [PATCH] Perl_my_setenv(); handle integer wrap
    DEBPKG:fixes/CVE-2018-18312.patch - [PATCH 242/242] PATCH: [perl #133423] for 5.26 maint
    DEBPKG:fixes/CVE-2018-18313.patch - [PATCH] regcomp.c: Convert some strchr to memchr
    DEBPKG:fixes/CVE-2018-18314.patch - [PATCH] fix #131649 - extended charclass can trigger assert
  Built under linux
  Compiled at Nov 19 2018 15:54:44
  @INC:
    /etc/perl
    /usr/local/lib/x86_64-linux-gnu/perl/5.26.1
    /usr/local/share/perl/5.26.1
    /usr/lib/x86_64-linux-gnu/perl5/5.26
    /usr/share/perl5
    /usr/lib/x86_64-linux-gnu/perl/5.26
    /usr/share/perl/5.26
    /usr/local/lib/site_perl
    /usr/lib/x86_64-linux-gnu/perl-base
")
set (PERL_MINUSV_RESULT_VARIABLE "0")
set (PERL_NEEDS_ADJUSTMENT "")
set (PERL_OUTPUT "5.26.1")
set (PERL_POSSIBLE_BIN_PATHS "/bin")
set (PERL_POSSIBLE_LIBRARY_NAMES "libperl.so.5.26")
set (PERL_PREFIX "/usr")
set (PERL_PREFIX_OUTPUT_VARIABLE "prefix='/usr';
")
set (PERL_PREFIX_RESULT_VARIABLE "0")
set (PERL_PRIVLIB "/usr/share/perl/5.26")
set (PERL_PRIVLIB_OUTPUT_VARIABLE "installprivlib='/usr/share/perl/5.26';
")
set (PERL_PRIVLIB_RESULT_VARIABLE "0")
set (PERL_RETURN_VALUE "0")
set (PERL_SITELIB "/usr/local/share/perl/5.26.1")
set (PERL_SITELIB_OUTPUT_VARIABLE "installsitelib='/usr/local/share/perl/5.26.1';
")
set (PERL_SITELIB_RESULT_VARIABLE "0")
set (PERL_SITESEARCH "UNKNOWN")
set (PERL_SITESEARCH_OUTPUT_VARIABLE "installsitesearch='UNKNOWN';
")
set (PERL_SITESEARCH_RESULT_VARIABLE "0")
set (PERL_VARNAME "installprivlib")
set (PERL_VENDORARCH "/usr/lib/x86_64-linux-gnu/perl5/5.26")
set (PERL_VENDORARCH_OUTPUT_VARIABLE "installvendorarch='/usr/lib/x86_64-linux-gnu/perl5/5.26';
")
set (PERL_VENDORARCH_RESULT_VARIABLE "0")
set (PERL_VENDORLIB "/usr/share/perl5")
set (PERL_VENDORLIB_OUTPUT_VARIABLE "installvendorlib='/usr/share/perl5';
")
set (PERL_VENDORLIB_RESULT_VARIABLE "0")
set (PERL_VERSION "5.26.1")
set (PERL_VERSION_OUTPUT_VARIABLE "version='5.26.1';")
set (PERL_VERSION_RESULT_VARIABLE "0")
set (PERL_VERSION_STRING "5.26.1")
set (PKG "DRAMMSFastPD")
set (PKG_DIR "/home/filippos/programs/dr/build/bundle/lib")
set (PRESET_CMAKE_SYSTEM_NAME "FALSE")
set (PROJECT_AUTHORS "Yangming Ou;Aristeidis Sotiras;Andreas Schuh")
set (PROJECT_AUTHORS_FILE "/home/filippos/programs/dramms-1.5.1-source/AUTHORS.txt")
set (PROJECT_BINARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build")
set (PROJECT_CODE_DIR "/home/filippos/programs/dramms-1.5.1-source/src")
set (PROJECT_CODE_DIRS "/home/filippos/programs/dramms-1.5.1-source/src")
set (PROJECT_CONFIG_DIR "/home/filippos/programs/dramms-1.5.1-source/config")
set (PROJECT_CONTACT "SBIA Group <sbia-software at uphs.upenn.edu>")
set (PROJECT_COPYING_FILE "/home/filippos/programs/dramms-1.5.1-source/COPYING.txt")
set (PROJECT_COPYRIGHT "2011--2013 University of Pennsylvania. 2014--2016 Massachusetts General Hospital, Harvard Medical School. 2016--, Boston Childrens Hosptial, Harvard Medical School.")
set (PROJECT_DATA_DIR "/home/filippos/programs/dramms-1.5.1-source/data")
set (PROJECT_DEPENDS "NiftiCLib;DRAMMSFastPD")
set (PROJECT_DESCRIPTION "Deformable Registration via Attribute Matching and Mutual-Saliency weighting")
set (PROJECT_DIVISION_NAME_L "")
set (PROJECT_DIVISION_NAME_U "")
set (PROJECT_DOCRES_DIR "/home/filippos/programs/dramms-1.5.1-source/doc/static")
set (PROJECT_DOC_DIR "/home/filippos/programs/dramms-1.5.1-source/doc")
set (PROJECT_EXAMPLE_DIR "/home/filippos/programs/dramms-1.5.1-source/example")
set (PROJECT_INCLUDE_DIR "/home/filippos/programs/dramms-1.5.1-source/include")
set (PROJECT_INCLUDE_DIRS "/home/filippos/programs/dramms-1.5.1-source/include")
set (PROJECT_INSTALL_FILE "/home/filippos/programs/dramms-1.5.1-source/INSTALL.txt")
set (PROJECT_IS_GIT_REPOSITORY "FALSE")
set (PROJECT_IS_MODULE "FALSE")
set (PROJECT_IS_SUBPROJECT "FALSE")
set (PROJECT_LIBRARY_DIR "/home/filippos/programs/dramms-1.5.1-source/lib")
set (PROJECT_LICENSE "See http://www.cbica.upenn.edu/sbia/software/license.html or COPYING file.")
set (PROJECT_LICENSE_FILE "/home/filippos/programs/dramms-1.5.1-source/COPYING.txt")
set (PROJECT_MODULES_DIR "/home/filippos/programs/dramms-1.5.1-source/modules")
set (PROJECT_MODULES_DISABLED "")
set (PROJECT_MODULES_ENABLED "")
set (PROJECT_NAME "DRAMMS")
set (PROJECT_NAMESPACE_BASH "dramms")
set (PROJECT_NAMESPACE_CMAKE "dramms")
set (PROJECT_NAMESPACE_CXX "dramms")
set (PROJECT_NAMESPACE_JAVA "")
set (PROJECT_NAMESPACE_JYTHON "dramms")
set (PROJECT_NAMESPACE_MATLAB "dramms")
set (PROJECT_NAMESPACE_PERL "DRAMMS")
set (PROJECT_NAMESPACE_PYTHON "dramms")
set (PROJECT_NAME_L "dramms")
set (PROJECT_NAME_RE "DRAMMS")
set (PROJECT_NAME_RE_L "dramms")
set (PROJECT_NAME_RE_U "DRAMMS")
set (PROJECT_NAME_U "DRAMMS")
set (PROJECT_PACKAGE_CONFIG_PREFIX "DRAMMS")
set (PROJECT_PACKAGE_LOGO "/home/filippos/programs/dramms-1.5.1-source/doc/static/")
set (PROJECT_PACKAGE_NAME "DRAMMS")
set (PROJECT_PACKAGE_NAME_L "dramms")
set (PROJECT_PACKAGE_NAME_RE "DRAMMS")
set (PROJECT_PACKAGE_NAME_RE_L "dramms")
set (PROJECT_PACKAGE_NAME_RE_U "DRAMMS")
set (PROJECT_PACKAGE_NAME_U "DRAMMS")
set (PROJECT_PACKAGE_VENDOR "SBIA")
set (PROJECT_PACKAGE_VENDOR_L "sbia")
set (PROJECT_PACKAGE_VENDOR_U "SBIA")
set (PROJECT_PROVIDER_NAME_L "")
set (PROJECT_PROVIDER_NAME_U "")
set (PROJECT_README_FILE "/home/filippos/programs/dramms-1.5.1-source/README.txt")
set (PROJECT_RELEASE "v1.5")
set (PROJECT_REVISION "0")
set (PROJECT_SOURCE_DIR "/home/filippos/programs/dramms-1.5.1-source")
set (PROJECT_SOVERSION "1.5")
set (PROJECT_SUBDIRS "/home/filippos/programs/dramms-1.5.1-source/src;/home/filippos/programs/dramms-1.5.1-source/data")
set (PROJECT_TESTING_DIR "/home/filippos/programs/dramms-1.5.1-source/test")
set (PROJECT_UNPARSED_ARGUMENTS "UPenn,;MGH,;BCH")
set (PROJECT_VENDOR "SBIA")
set (PROJECT_VERSION "1.5.1")
set (PROJECT_VERSION_AND_REVISION "v1.5")
set (PROJECT_VERSION_MAJOR "1")
set (PROJECT_VERSION_MINOR "5")
set (PROJECT_VERSION_PATCH "0")
set (PROJECT_VERSION_PERL "1.05_00")
set (PYTHON_LIBRARY_TARGET "pythonlib")
set (PYTHON_VERSION_MAJOR "0")
set (PYTHON_VERSION_MINOR "0")
set (PYTHON_VERSION_PATCH "0")
set (PYTHON_VERSION_STRING "0.0")
set (PerlLIbs_FIND_QUIETLY "")
set (PerlLIbs_FIND_QUIETLY_BACKUP "1")
set (PerlLIbs_FIND_REQUIRED "")
set (PerlLIbs_FIND_REQUIRED_BACKUP "")
set (PerlLibs_FIND_REQUIRED "FALSE")
set (PerlLibs_FOUND "TRUE")
set (Perl_FOUND "TRUE")
set (Project_BINARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build")
set (Project_SOURCE_DIR "/home/filippos/programs/dramms-1.5.1-source")
set (RUN_CONFIGURE "ON")
set (RUN_FROM_DART "1")
set (S "/home/filippos/programs/dramms-1.5.1-source/src/.dramms")
set (SET_OUTPUT_NAME_TO_TARGET_NAME "TRUE")
set (SOURCES "/home/filippos/programs/dramms-1.5.1-source/src/dramms.sh.in")
set (SUBDIR "/home/filippos/programs/dramms-1.5.1-source/src")
set (SUBVERSION_FOUND "TRUE")
set (Subversion_FOUND "TRUE")
set (Subversion_SVN_EXECUTABLE "/usr/bin/svn")
set (Subversion_SVN_FOUND "TRUE")
set (Subversion_VERSION_SVN "1.9.7")
set (TARGET_NAME ".dramms")
set (TARGET_UID "dramms")
set (TEMPLATE_VERSION "1.7")
set (TESTING_ARCHIVE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/lib")
set (TESTING_BASH_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/lib/bash")
set (TESTING_JYTHON_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/lib/jython")
set (TESTING_LIBEXEC_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/lib")
set (TESTING_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/lib")
set (TESTING_MATLAB_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/lib/matlab")
set (TESTING_OUTPUT_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/Temporary")
set (TESTING_PERL_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/lib/perl")
set (TESTING_PYTHON_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/lib/python")
set (TESTING_RUNTIME_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/Testing/bin")
set (TOPLEVEL_BINARY_ARCHIVE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib")
set (TOPLEVEL_BINARY_BASH_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/bash")
set (TOPLEVEL_BINARY_CODE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src")
set (TOPLEVEL_BINARY_CONFIG_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/config")
set (TOPLEVEL_BINARY_DATA_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/data")
set (TOPLEVEL_BINARY_DOC_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/doc")
set (TOPLEVEL_BINARY_EXAMPLE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/example")
set (TOPLEVEL_BINARY_INCLUDE_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/include")
set (TOPLEVEL_BINARY_JYTHON_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/jython")
set (TOPLEVEL_BINARY_LIBEXEC_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib")
set (TOPLEVEL_BINARY_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib")
set (TOPLEVEL_BINARY_MATLAB_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/matlab")
set (TOPLEVEL_BINARY_MODULES_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/modules")
set (TOPLEVEL_BINARY_PERL_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/perl")
set (TOPLEVEL_BINARY_PYTHON_LIBRARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/lib/python")
set (TOPLEVEL_BINARY_RUNTIME_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/bin")
set (TOPLEVEL_BINARY_TESTING_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/test")
set (TOPLEVEL_INSTALL_ARCHIVE_DIR "lib/dramms")
set (TOPLEVEL_INSTALL_BASH_LIBRARY_DIR "lib/dramms/bash")
set (TOPLEVEL_INSTALL_CONFIG_DIR "lib/cmake/dramms")
set (TOPLEVEL_INSTALL_DATA_DIR "share/dramms/data")
set (TOPLEVEL_INSTALL_DOC_DIR "doc/dramms")
set (TOPLEVEL_INSTALL_EXAMPLE_DIR "share/dramms/example")
set (TOPLEVEL_INSTALL_INCLUDE_DIR "include")
set (TOPLEVEL_INSTALL_JYTHON_LIBRARY_DIR "lib/dramms/jython")
set (TOPLEVEL_INSTALL_LIBEXEC_DIR "lib/dramms")
set (TOPLEVEL_INSTALL_LIBRARY_DIR "lib/dramms")
set (TOPLEVEL_INSTALL_MATLAB_LIBRARY_DIR "lib/dramms/matlab")
set (TOPLEVEL_INSTALL_PERL_LIBRARY_DIR "lib/dramms/perl")
set (TOPLEVEL_INSTALL_PYTHON_LIBRARY_DIR "lib/dramms/python")
set (TOPLEVEL_INSTALL_RUNTIME_DIR "bin")
set (TOPLEVEL_INSTALL_SHARE_DIR "share/dramms")
set (TOPLEVEL_PROJECT_AUTHORS "Yangming Ou;Aristeidis Sotiras;Andreas Schuh")
set (TOPLEVEL_PROJECT_AUTHORS_FILE "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/AUTHORS.txt")
set (TOPLEVEL_PROJECT_BINARY_DIR "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build")
set (TOPLEVEL_PROJECT_CONTACT "SBIA Group <sbia-software at uphs.upenn.edu>")
set (TOPLEVEL_PROJECT_COPYING_FILE "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/COPYING.txt")
set (TOPLEVEL_PROJECT_COPYRIGHT "2011--2013 University of Pennsylvania. 2014--2016 Massachusetts General Hospital, Harvard Medical School. 2016--, Boston Childrens Hosptial, Harvard Medical School.")
set (TOPLEVEL_PROJECT_DIVISION_LOGO "")
set (TOPLEVEL_PROJECT_DIVISION_NAME "")
set (TOPLEVEL_PROJECT_DIVISION_NAME_L "")
set (TOPLEVEL_PROJECT_DIVISION_NAME_U "")
set (TOPLEVEL_PROJECT_DIVISION_WEBSITE "")
set (TOPLEVEL_PROJECT_INSTALL_FILE "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/INSTALL.txt")
set (TOPLEVEL_PROJECT_LICENSE "See http://www.cbica.upenn.edu/sbia/software/license.html or COPYING file.")
set (TOPLEVEL_PROJECT_NAME "DRAMMS")
set (TOPLEVEL_PROJECT_NAMESPACE_CMAKE "dramms")
set (TOPLEVEL_PROJECT_NAME_L "dramms")
set (TOPLEVEL_PROJECT_NAME_RE "DRAMMS")
set (TOPLEVEL_PROJECT_NAME_RE_L "dramms")
set (TOPLEVEL_PROJECT_NAME_RE_U "DRAMMS")
set (TOPLEVEL_PROJECT_NAME_U "DRAMMS")
set (TOPLEVEL_PROJECT_PACKAGE_CONFIG_PREFIX "DRAMMS")
set (TOPLEVEL_PROJECT_PACKAGE_LOGO "")
set (TOPLEVEL_PROJECT_PACKAGE_NAME "DRAMMS")
set (TOPLEVEL_PROJECT_PACKAGE_NAME_L "dramms")
set (TOPLEVEL_PROJECT_PACKAGE_NAME_RE "DRAMMS")
set (TOPLEVEL_PROJECT_PACKAGE_NAME_RE_L "dramms")
set (TOPLEVEL_PROJECT_PACKAGE_NAME_RE_U "DRAMMS")
set (TOPLEVEL_PROJECT_PACKAGE_NAME_U "DRAMMS")
set (TOPLEVEL_PROJECT_PACKAGE_UID "SBIA-DRAMMS-1.5.1")
set (TOPLEVEL_PROJECT_PACKAGE_VENDOR "SBIA")
set (TOPLEVEL_PROJECT_PACKAGE_VENDOR_L "sbia")
set (TOPLEVEL_PROJECT_PACKAGE_VENDOR_U "SBIA")
set (TOPLEVEL_PROJECT_PACKAGE_WEBSITE "")
set (TOPLEVEL_PROJECT_PROVIDER_LOGO "")
set (TOPLEVEL_PROJECT_PROVIDER_NAME "")
set (TOPLEVEL_PROJECT_PROVIDER_NAME_L "")
set (TOPLEVEL_PROJECT_PROVIDER_NAME_U "")
set (TOPLEVEL_PROJECT_PROVIDER_WEBSITE "")
set (TOPLEVEL_PROJECT_README_FILE "/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/README.txt")
set (TOPLEVEL_PROJECT_SOURCE_DIR "/home/filippos/programs/dramms-1.5.1-source")
set (TOPLEVEL_PROJECT_VERSION "1.5.1")
set (TYPE "EXECUTABLE")
set (UNIX "1")
set (VER "")
set (ZLIB_FOUND "TRUE")
set (ZLIB_H "#define ZLIB_VERSION \"1.2.11\"")
set (ZLIB_INCLUDE_DIR "/usr/include")
set (ZLIB_INCLUDE_DIRS "/usr/include")
set (ZLIB_LIBRARIES "/usr/lib/x86_64-linux-gnu/libz.so")
set (ZLIB_LIBRARY "/usr/lib/x86_64-linux-gnu/libz.so")
set (ZLIB_LIBRARY_DEBUG "ZLIB_LIBRARY_DEBUG-NOTFOUND")
set (ZLIB_LIBRARY_RELEASE "/usr/lib/x86_64-linux-gnu/libz.so")
set (ZLIB_MAJOR_VERSION "1")
set (ZLIB_MINOR_VERSION "2")
set (ZLIB_PATCH_VERSION "11")
set (ZLIB_VERSION_MAJOR "1")
set (ZLIB_VERSION_MINOR "2")
set (ZLIB_VERSION_PATCH "11")
set (ZLIB_VERSION_STRING "1.2.11")
set (ZLIB_VERSION_TWEAK "")
set (basis_LIB_DEPENDS "general;basis.utilities_cxx;")
set (c "")
set (common_LIB_DEPENDS "general;niftiio;general;basis;")
set (f "")
set (file "")
set (l "")
set (mode "")
set (search "")
set (t "")
set (target "")
set (testtype "")
set (type "executable")
set (val "0")
