# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.8

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/filippos/programs/dramms-1.5.1-source

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/filippos/programs/dr/build/bundle/src/DRAMMS-build

# Include any dependencies generated for this target.
include src/tools/CMakeFiles/GenerateImageListFile.dir/depend.make

# Include the progress variables for this target.
include src/tools/CMakeFiles/GenerateImageListFile.dir/progress.make

# Include the compile flags for this target's objects.
include src/tools/CMakeFiles/GenerateImageListFile.dir/flags.make

src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o: src/tools/CMakeFiles/GenerateImageListFile.dir/flags.make
src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o: /home/filippos/programs/dramms-1.5.1-source/src/tools/GenerateImageListFile.cxx
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o"
	cd /home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/tools && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o -c /home/filippos/programs/dramms-1.5.1-source/src/tools/GenerateImageListFile.cxx

src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.i"
	cd /home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/tools && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/filippos/programs/dramms-1.5.1-source/src/tools/GenerateImageListFile.cxx > CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.i

src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.s"
	cd /home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/tools && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/filippos/programs/dramms-1.5.1-source/src/tools/GenerateImageListFile.cxx -o CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.s

src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o.requires:

.PHONY : src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o.requires

src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o.provides: src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o.requires
	$(MAKE) -f src/tools/CMakeFiles/GenerateImageListFile.dir/build.make src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o.provides.build
.PHONY : src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o.provides

src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o.provides.build: src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o


# Object files for target GenerateImageListFile
GenerateImageListFile_OBJECTS = \
"CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o"

# External object files for target GenerateImageListFile
GenerateImageListFile_EXTERNAL_OBJECTS =

lib/GenerateImageListFile: src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o
lib/GenerateImageListFile: src/tools/CMakeFiles/GenerateImageListFile.dir/build.make
lib/GenerateImageListFile: lib/libcommon.a
lib/GenerateImageListFile: lib/libbasis.a
lib/GenerateImageListFile: /home/filippos/programs/dr/lib/dramms/basis/libutilities.a
lib/GenerateImageListFile: /home/filippos/programs/dr/build/bundle/lib/libniftiio.a
lib/GenerateImageListFile: /home/filippos/programs/dr/build/bundle/lib/libznz.a
lib/GenerateImageListFile: /usr/lib/x86_64-linux-gnu/libz.so
lib/GenerateImageListFile: src/tools/CMakeFiles/GenerateImageListFile.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/filippos/programs/dr/build/bundle/src/DRAMMS-build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable ../../lib/GenerateImageListFile"
	cd /home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/tools && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/GenerateImageListFile.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/tools/CMakeFiles/GenerateImageListFile.dir/build: lib/GenerateImageListFile

.PHONY : src/tools/CMakeFiles/GenerateImageListFile.dir/build

src/tools/CMakeFiles/GenerateImageListFile.dir/requires: src/tools/CMakeFiles/GenerateImageListFile.dir/GenerateImageListFile.cxx.o.requires

.PHONY : src/tools/CMakeFiles/GenerateImageListFile.dir/requires

src/tools/CMakeFiles/GenerateImageListFile.dir/clean:
	cd /home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/tools && $(CMAKE_COMMAND) -P CMakeFiles/GenerateImageListFile.dir/cmake_clean.cmake
.PHONY : src/tools/CMakeFiles/GenerateImageListFile.dir/clean

src/tools/CMakeFiles/GenerateImageListFile.dir/depend:
	cd /home/filippos/programs/dr/build/bundle/src/DRAMMS-build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/filippos/programs/dramms-1.5.1-source /home/filippos/programs/dramms-1.5.1-source/src/tools /home/filippos/programs/dr/build/bundle/src/DRAMMS-build /home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/tools /home/filippos/programs/dr/build/bundle/src/DRAMMS-build/src/tools/CMakeFiles/GenerateImageListFile.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/tools/CMakeFiles/GenerateImageListFile.dir/depend

