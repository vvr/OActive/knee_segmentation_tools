# Install script for directory: /home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/filippos/programs/dr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/dramms/basis/cmake-modules" TYPE FILE FILES
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/BasisPack.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/BasisTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/BasisTest.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/BasisSettings.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/CommonTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/DirectoriesSettings.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/DocTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ExportTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ExternalData.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ExternalData_config.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ImportTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/InterpTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/InstallationTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/MatlabTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ProjectTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/RevisionTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/SlicerTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/SuperBuildTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/TargetTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/TopologicalSort.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/UtilitiesTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindAFNI.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindBASH.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindBLAS.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindBoostNumericBindings.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindGMock.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindGTest.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindITK.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindJythonInterp.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindLIBLINEAR.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindMATLAB.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindMatlabNiftiTools.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindMOSEK.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindNiftiCLib.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindOpenCV.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindPerl.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindPerlLibs.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindPythonInterp.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindPythonModules.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindSparseBayes.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindSphinx.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindSVMTorch.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/FindWeka.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/Doxyfile.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/Modules.dox"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/Utilities.dox"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/CxxUtilities.dox"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/JavaUtilities.dox"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/PythonUtilities.dox"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/PerlUtilities.dox"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/BashUtilities.dox"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/MatlabUtilities.dox"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/Config.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ModuleConfig.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/BasisConfigSettings.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ConfigVersion.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ConfigUse.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ModuleConfigUse.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/GenerateConfig.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/doxygen_header.html.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/doxygen_footer.html.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/doxygen_extra.css.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/sphinx_conf.py.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/sphinx_make.sh.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/buildtimestamp.cmd"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/CheckPublicHeaders.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/cmake_uninstall.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/configure_script.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ConfigureIncludeFiles.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/Directories.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/doxyfilter.bat.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ExecuteProcess.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/generate_matlab_executable.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/get_python_lib.py"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/glob.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ScriptConfig.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/PostprocessChangeLog.cmake"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/ProjectSettings.cmake.in"
    "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/updatefile.py"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/dramms/basis/cmake-modules/uninstall" TYPE FILE FILES "/home/filippos/programs/dr/build/bundle/src/BASIS/src/cmake/uninstall/CMakeLists.txt")
endif()

