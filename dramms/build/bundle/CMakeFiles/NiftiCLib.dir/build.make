# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.8

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/filippos/programs/dr/build/bundle

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/filippos/programs/dr/build/bundle

# Utility rule file for NiftiCLib.

# Include the progress variables for this target.
include CMakeFiles/NiftiCLib.dir/progress.make

CMakeFiles/NiftiCLib: CMakeFiles/NiftiCLib-complete


CMakeFiles/NiftiCLib-complete: src/NiftiCLib-stamp/NiftiCLib-install
CMakeFiles/NiftiCLib-complete: src/NiftiCLib-stamp/NiftiCLib-mkdir
CMakeFiles/NiftiCLib-complete: src/NiftiCLib-stamp/NiftiCLib-download
CMakeFiles/NiftiCLib-complete: src/NiftiCLib-stamp/NiftiCLib-update
CMakeFiles/NiftiCLib-complete: src/NiftiCLib-stamp/NiftiCLib-patch
CMakeFiles/NiftiCLib-complete: src/NiftiCLib-stamp/NiftiCLib-configure
CMakeFiles/NiftiCLib-complete: src/NiftiCLib-stamp/NiftiCLib-build
CMakeFiles/NiftiCLib-complete: src/NiftiCLib-stamp/NiftiCLib-install
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Completed 'NiftiCLib'"
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/CMakeFiles
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/CMakeFiles/NiftiCLib-complete
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/NiftiCLib-done

src/NiftiCLib-stamp/NiftiCLib-install: src/NiftiCLib-stamp/NiftiCLib-build
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Performing install step for 'NiftiCLib'"
	cd /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build && $(MAKE) install
	cd /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/NiftiCLib-install

src/NiftiCLib-stamp/NiftiCLib-mkdir:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Creating directories for 'NiftiCLib'"
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/src/NiftiCLib
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/tmp
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E make_directory /home/filippos/programs/dr/build/bundle/src
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/NiftiCLib-mkdir

src/NiftiCLib-stamp/NiftiCLib-download: src/NiftiCLib-stamp/NiftiCLib-urlinfo.txt
src/NiftiCLib-stamp/NiftiCLib-download: src/NiftiCLib-stamp/NiftiCLib-mkdir
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Performing download step (download, verify and extract) for 'NiftiCLib'"
	cd /home/filippos/programs/dr/build/bundle/src && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -P /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/download-NiftiCLib.cmake
	cd /home/filippos/programs/dr/build/bundle/src && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -P /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/verify-NiftiCLib.cmake
	cd /home/filippos/programs/dr/build/bundle/src && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -P /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/extract-NiftiCLib.cmake
	cd /home/filippos/programs/dr/build/bundle/src && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/NiftiCLib-download

src/NiftiCLib-stamp/NiftiCLib-update: src/NiftiCLib-stamp/NiftiCLib-download
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "No update step for 'NiftiCLib'"
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E echo_append
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/NiftiCLib-update

src/NiftiCLib-stamp/NiftiCLib-patch: src/NiftiCLib-stamp/NiftiCLib-download
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "No patch step for 'NiftiCLib'"
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E echo_append
	/home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/NiftiCLib-patch

src/NiftiCLib-stamp/NiftiCLib-configure: tmp/NiftiCLib-cfgcmd.txt
src/NiftiCLib-stamp/NiftiCLib-configure: src/NiftiCLib-stamp/NiftiCLib-update
src/NiftiCLib-stamp/NiftiCLib-configure: src/NiftiCLib-stamp/NiftiCLib-patch
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "Performing configure step for 'NiftiCLib'"
	cd /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -Wno-dev -C/home/filippos/programs/dr/build/bundle/tmp/NiftiCLib-cache-Release.cmake "-GUnix Makefiles" /home/filippos/programs/dr/build/bundle/src/NiftiCLib
	cd /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/NiftiCLib-configure

src/NiftiCLib-stamp/NiftiCLib-build: src/NiftiCLib-stamp/NiftiCLib-configure
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/filippos/programs/dr/build/bundle/CMakeFiles --progress-num=$(CMAKE_PROGRESS_8) "Performing build step for 'NiftiCLib'"
	cd /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build && $(MAKE)
	cd /home/filippos/programs/dr/build/bundle/src/NiftiCLib-build && /home/filippos/Support/cmake-3.8.2-Linux-x86_64/bin/cmake -E touch /home/filippos/programs/dr/build/bundle/src/NiftiCLib-stamp/NiftiCLib-build

NiftiCLib: CMakeFiles/NiftiCLib
NiftiCLib: CMakeFiles/NiftiCLib-complete
NiftiCLib: src/NiftiCLib-stamp/NiftiCLib-install
NiftiCLib: src/NiftiCLib-stamp/NiftiCLib-mkdir
NiftiCLib: src/NiftiCLib-stamp/NiftiCLib-download
NiftiCLib: src/NiftiCLib-stamp/NiftiCLib-update
NiftiCLib: src/NiftiCLib-stamp/NiftiCLib-patch
NiftiCLib: src/NiftiCLib-stamp/NiftiCLib-configure
NiftiCLib: src/NiftiCLib-stamp/NiftiCLib-build
NiftiCLib: CMakeFiles/NiftiCLib.dir/build.make

.PHONY : NiftiCLib

# Rule to build all files generated by this target.
CMakeFiles/NiftiCLib.dir/build: NiftiCLib

.PHONY : CMakeFiles/NiftiCLib.dir/build

CMakeFiles/NiftiCLib.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/NiftiCLib.dir/cmake_clean.cmake
.PHONY : CMakeFiles/NiftiCLib.dir/clean

CMakeFiles/NiftiCLib.dir/depend:
	cd /home/filippos/programs/dr/build/bundle && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/filippos/programs/dr/build/bundle /home/filippos/programs/dr/build/bundle /home/filippos/programs/dr/build/bundle /home/filippos/programs/dr/build/bundle /home/filippos/programs/dr/build/bundle/CMakeFiles/NiftiCLib.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/NiftiCLib.dir/depend

